package stepdefinition.homepage;

import commonfunctions.CommonFunctions;
import commonfunctions.CustomVerification;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.testng.asserts.SoftAssert;
import pages.HomePage;
import io.cucumber.datatable.DataTable;
import utility.UrlUtils;
import utility.Validations;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

public class HomePageDefs {

    CommonFunctions commonFunctions = new CommonFunctions();
    HomePage homePage = new HomePage();
    SoftAssert softAssert = new SoftAssert();
    config.ObjPropertyReader objPropertyReader= new config.ObjPropertyReader();
    String styleReportImageByClassName = objPropertyReader.readProperty("styleReportImageByClassName");
    private NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    private String baseURL;

    @Then("^I navigate to the home page to see the 'INSTANT OUTFIT' section with each product having product image, designer name, product description and price$")
    public void  verifyInstantOutfitContainer(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
                String url = rawTable.get(i).get(0);
                String expectedTitle = rawTable.get(i).get(1);
                if (navigateUsingURL.getrelativeURLJenkins() == null) {
                    commonFunctions.navigateTo(Constants.BASE_URL + url);
                } else {
                    String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
                    commonFunctions.navigateTo(relativeURLJenkins + url);
                }

                String actualTitle = homePage.grabInstantOutfitTitle();
                int actualNumberOfProducts = homePage.grabNumberOfProducts();
                List<String> instantOutfitProductDetails = homePage.grabInstantOutfitProductDetailsContainer();
                int actualNumOfProducts = Integer.parseInt((String.valueOf(actualNumberOfProducts)));
                softAssert.assertTrue(expectedTitle.contentEquals(actualTitle), "Actual title was not the wanted one: Expected: " + expectedTitle + "--Actual: " + actualTitle);
                softAssert.assertTrue(!instantOutfitProductDetails.isEmpty(),"Product details were empty. ");
                softAssert.assertTrue(actualNumOfProducts>=1,"Actual number of items was not the wanted one");

            }
            softAssert.assertAll();
        }
    }

    @Then("^I navigate to the home page to see the 'The Style Report' section with a scrollbar$")
    public void verifyStyleReportContainer(DataTable dataTable) throws IOException

    {
       // logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
               // logger.info("Row data: " + rawTable.get(i));
                String url = rawTable.get(i).get(0);
                if (navigateUsingURL.getrelativeURLJenkins() == null) {
                    commonFunctions.navigateTo(Constants.BASE_URL + url);
                } else {
                    String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
                    commonFunctions.navigateTo(relativeURLJenkins + url);
                }
                homePage.scrollScreen300Pixel();
                homePage.waitForPageToLoad();
                String contactUsImageStatus = homePage.grabTheStyleReportImage();
                String urlStatus=homePage.isLinkBroken(contactUsImageStatus);
                softAssert.assertTrue((urlStatus.trim()).equalsIgnoreCase("200") ,"Style report image internal URL is not working" + "URl status is" + urlStatus);
                softAssert.assertAll();
                String imageURL = "";
                if (imageURL.contentEquals(contactUsImageStatus)) {
                    String imageStatus = "";
                    try {
                        imageStatus = UrlUtils.getUrlStatusCode(imageURL);
                    } catch (IOException e) {
                        System.out.println("The style report image: - has issues with the url: " + imageURL);
                    }
                    Validations.verifyImagesStatusCodeInProductView(imageURL, imageStatus);
                }
                homePage.scrollTillLastElementIsVisible();
            }
        }
    }

    @Given("^the carousel contains three counters, three images, next and previous arrows from which can be clicked to change the current image$")
    public void verifyCarouselCounterImagesChange(DataTable dataTable)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        int expectedCarouselSize = 3;
        String previousUrl = "";
        if (navigateUsingURL.getrelativeURLJenkins() == null) {
            baseURL=Constants.BASE_URL;
        } else {
            baseURL = navigateUsingURL.getrelativeURLJenkins();
        }
        List<List<String>> rawTable = dataTable.asLists();

        for (int i = 1; i < rawTable.size(); i++)
        {
            //logger.info("Row data: " + rawTable.get(i));
            String tableUrl = rawTable.get(i).get(0);
            String performAction = rawTable.get(i).get(1);
            if (!previousUrl.contentEquals(tableUrl))
            {
                previousUrl = tableUrl;
                String newUrl = baseURL + tableUrl;
                newUrl = newUrl.replace("//", "/");
                commonFunctions.navigateTo(newUrl);
                int actualCarouselSize = homePage.grabCarouselCounterSize();
                softAssert.assertEquals(actualCarouselSize,expectedCarouselSize,"Carousel should contain "+expectedCarouselSize+" images");
            }
            if (performAction.contains("next"))
                homePage.clickOnNextArrowOnCarousel();
            else
                homePage.clickOnPreviousArrowOnCarousel();
        }
    }
}
