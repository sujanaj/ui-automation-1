package stepdefinition.minicart;

import cucumber.api.java.en.Then;
//import org.testng.asserts.SoftAssert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pages.MiniCartPage;

public class MiniCartDefs  {
	SoftAssert assertions = new SoftAssert();
	MiniCartPage  miniCartPage = new MiniCartPage();

	@Then("^the number next to the mini bag icon updates to '(.*)'")
	public void verifyTheMinibagIcon(String expectedminiBagIconItemNumber) {
		miniCartPage.minicartIsDisplayed();
		//  logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + expectedminiBagIconItemNumber);
		String actualminiBagIconItemNumber = miniCartPage.grabMinicartIconNumber();
		assertions.assertTrue(
				expectedminiBagIconItemNumber.contentEquals(actualminiBagIconItemNumber),
				"Minicart item number was not the one from example table " + " Expected: "
						+ expectedminiBagIconItemNumber + " Actual: " + actualminiBagIconItemNumber);
		assertions.assertAll();


	}


}
