package stepdefinition.checkout;

import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.asserts.SoftAssert;
import pages.CheckOutPage;
import pages.HeaderPage;
import pages.account.PaymentCardsPage;
import stepdefinition.productDetails.ProductDetailsDefs;

public class CheckOutDefs {
    CommonFunctions commonFunctions = new CommonFunctions();
    CheckOutPage checkOutPage = new CheckOutPage();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    SoftAssert assertion = new SoftAssert();
    private NavigateUsingURL navigateUsingURL = new NavigateUsingURL();

    final PaymentCardsPage paymentCardsPage = new PaymentCardsPage();
    final HeaderPage headerPage = new HeaderPage();
    final ProductDetailsDefs productDetailsDefs = new ProductDetailsDefs();

    String bottomPurchaseNowBtnLocatorByCSS = obj_Property.readProperty("bottomPurchaseNowBtnLocatorByCSS");
    String editShoppingBagFromReviewAndPay = obj_Property.readProperty("editShoppingBagFromReviewAndPayByClassName");

    @And("I check out")
    public void iCheckOut() throws Throwable {
        commonFunctions.waitForScriptsToLoad();
        navigateUsingURL.navigateToRelativeURL("/checkout");
        commonFunctions.waitForScriptsToLoad();
    }

    @And("^I select the saved '(.*)' credit card$")
    public void iSelectTheSavedCreditCard(String cardtype) throws Throwable {
        commonFunctions.waitForScriptsToLoad();
        checkOutPage.selectSavedCreditCard(cardtype,"");
    }

    @And("^I select the saved '(.*)' credit card '(.*)'$")
    public void iSelectTheSavedCreditCard(String cardtype, String digits) throws Throwable {
        commonFunctions.waitForScriptsToLoad();
        checkOutPage.selectSavedCreditCard(cardtype, digits);
    }

    @And("^I place the order$")
    public void iPlaceTheOrder() {
        commonFunctions.waitForScriptsToLoad();
        commonFunctions.clickElementByCSS(bottomPurchaseNowBtnLocatorByCSS);
        commonFunctions.waitForScriptsToLoad();
    }
    @Then("^I should see the order confirmation page$")
    public void iShouldSeeTheorderConfirmationSteps()  {
        String orderNumberLocatorByCSS = obj_Property.readProperty("orderNumberLocatorByCSS");

        String confirmationPopUpBoxById = obj_Property.readProperty("confirmationPopUpBoxById");
      //  commonFunctions.closePopUpBox(confirmationPopUpBoxById);
        boolean found = false;
        try {
            if(commonFunctions.isElementDisplayed(orderNumberLocatorByCSS)) {
                String orderNumber = commonFunctions.getTextByCssSelector(orderNumberLocatorByCSS);
                System.out.println("MY ORDER# :" + orderNumber);
                if (orderNumber.toLowerCase().contains(Constants.ORDER_PREFIX.toLowerCase())) {
                    found = true;
                }
                assertion.assertTrue(found,"Expected order prefix: " + Constants.ORDER_PREFIX + " Actual: " + orderNumber );
            }
        }catch (Exception e){
            assertion.assertTrue(checkOutPage.getErrorMessageOnPlaceOrder());
            assertion.fail("Order Placement Failed");
            assertion.assertAll();
        }
    }

    @And("^I select the '(.*)' billing address$")
    public void iSelectTheBillingAddress(String shipping_method) throws Throwable {
        String shippingMethodlistByCSS = obj_Property.readProperty("shippingMethodlistByCSS");
        String shippingMethodDeliveryPeriodByCSS = obj_Property.readProperty("shippingMethodDeliveryPeriodByCSS");
        checkOutPage.selectSShippingMethod(shipping_method,shippingMethodlistByCSS, shippingMethodDeliveryPeriodByCSS);

    }

    @When("^I select the 'Enter Card Details' radio button$")
    public void iSelectTheEnterCardDetailsRadioButton() {
        checkOutPage.clickEnterCardDetails();
    }


    @When("^I select '(.*)' from the Select Card Type Dropdown List$")
    public void iSelectVisaFromTheSelectCardTypeDropdownList(String cardType) {
        //cardDetailsSteps.clickEnterCardDetails();
        checkOutPage.selectCardType(cardType);
    }


    @When("^I enter '(.*)' in the Card Number field$")
    public void iEnterInTheCardNumberField(String cardNumber) {
        checkOutPage.inputCardNumber(cardNumber);
    }

    @When("^I enter '(.*)' in the Name On Card field$")
    public void iEnterTheNameOnCardField(String cardName) {
        checkOutPage.inputCardName(cardName);
    }


    @When("^I select '(.*)' and '(.*)' from the Expires End Dropdown Lists$")
    public void iSelectAndFromTheExpiresEndDropdownLists(String month, String year) {
        checkOutPage.inputExpireMonth(month);
        checkOutPage.inputExpireYear(year);
    }

    @When("^I enter '(.*)' in the Security Code field$")
    public void iEnterInTheSecurityCodeField(String securityCode) {
        checkOutPage.inputSecurityCode(securityCode);
    }

    @And("^I will get One Time Password pop-up enter One Time Password$")
    public void iWillGetOneTimePasswordPopUpEnterOneTimePassword() {
        checkOutPage.enterOTP();

    }

    @And("^I submit  One Time Password$")
    public void iSubmitOneTimePassword() {
        checkOutPage.submitOTP();
    }

    @And("^I unselect the checkbox to not save the card by default$")
    public void iUnselectTheCheckboxToNotSaveTheCardByDefault() {
        checkOutPage.unselectSaveCardByDefaultCheckbox();
    }

    @Then("^I should get the error message '(.*)' and order placement should get failed$")
    public void iShouldGetTheErrorMessageErrorAndOrderPlacementShouldGetFailed(String errorMessage)  {
        assertion.assertTrue(getErrorMessageOnPlaceOrder(),"Order placement Successfully");
        assertion.assertAll();
    }

    @And("^Secure SessionId should exist$")
    public void dSecureSessionIdShouldExist() {
        assertion.assertTrue(checkOutPage.isthreeDSecureSessionIdPresent(), "Failure: 3d-Secure SessionId is not present. --Actual: " + checkOutPage.isthreeDSecureSessionIdPresent() + "--Expected: TRUE");
        assertion.assertAll();
    }

    @When("^I select the 'Enter A New Billing Address' radio button$")
    public void iSelectTheEnterANewBillingAddressRadioButton() {
        String newBillingAddressByCSS = obj_Property.readProperty("newBillingAddressByCSS");
        commonFunctions.selectRadioButton(newBillingAddressByCSS);
    }

    @When("^I select the Billing Address 'Enter Full Address Manually' link if it is present$")
    public void iSelectTheBillingAddressEnterFullAddressManuallyLinkIfItIsPresent() {
        String enterFullAddressLocator = obj_Property.readProperty("enterFullAddressLocator");
        commonFunctions.clickElementByCSS(enterFullAddressLocator);
        commonFunctions.scrollScreen50Pixel();
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();

    }

    @When("^I select '(.*)' from the Billing Address Country Dropdown List$")
    public void iSelectTheBillingAddressCountryDropdownList(String country) {
        String enterFullAddressLocator = obj_Property.readProperty("enterFullAddressLocator");
        commonFunctions.selectFromDropdownByCSS(country,enterFullAddressLocator);

    }

    @When("^I enter '(.*)' in the Billing Address Line 1 field$")
    public void iEnterManetteStInTheBillingAddressLineField(String billingAddress) {
        String addressOneLocatorByCSS = obj_Property.readProperty("addressOneLocatorByCSS");
        commonFunctions.enterTextInputBox(addressOneLocatorByCSS,billingAddress);

    }
    @When("^I enter '(.*)' in the Billing Address Line 2 field$")
    public void iEnterTheBillingAddressLineTwoField(String billingAddress) {
        String addressTwoLocatorByCSS = obj_Property.readProperty("addressTwoLocatorByCSS");
        commonFunctions.enterTextInputBox(addressTwoLocatorByCSS,billingAddress);
    }

    @When("^I enter '(.*)' in the Billing Address Town/City field$")
    public void iEnterTownCityField(String city) {
        String townLocatorByCSS = obj_Property.readProperty("townLocatorByCSS");
        commonFunctions.enterTextInputBox(townLocatorByCSS,city);
    }
    @When("^I enter '(.*)' in the Billing Address Postcode/Zipcode field$")
    public void iEnterPostcodeZipcodeField(String zipCode) {
        String postcodeLocatorByCSS = obj_Property.readProperty("postcodeLocatorByCSS");
        commonFunctions.enterTextInputBox(postcodeLocatorByCSS,zipCode);
    }

    @When("^I select the 'Pay With Credit' radio button$")
    public void iSelectThePayWithCreditRadioButton() {
        String payWithCreditRadioLocatorByCSS = obj_Property.readProperty("payWithCreditRadioLocatorByCSS");
        commonFunctions.waitForPageToLoad();
        commonFunctions.scrollIntoViewByCss(payWithCreditRadioLocatorByCSS);
        commonFunctions.selectRadioButton(payWithCreditRadioLocatorByCSS);
        commonFunctions.waitForPageToLoad();
    }


    public boolean getErrorMessageOnPlaceOrder() {
        boolean isErrorExist = true;
        if(!checkOutPage.getErrorMessageOnPlaceOrder()){
            isErrorExist = false;
            iShouldSeeTheorderConfirmationSteps();
        }
        return  isErrorExist;
    }

    @And("^I have saved '(.*)' credit card '(.*)' or place an order with '(.*)' and '(.*)' and '(.*)' and '(.*)'$")
    public void iHaveSavedCard(String cardType, String digits, String cardNumber, String year, String securityCode, String challenge) throws Throwable
    {
        if(paymentCardsPage.isCardOnPage(cardType, digits)){
            return;
        }

        productDetailsDefs.addProductsToShoppingBagViaUrl();
        iCheckOut();
        iSelectTheBillingAddress("PREMIUM");
        iSelectTheEnterCardDetailsRadioButton();
        iSelectVisaFromTheSelectCardTypeDropdownList(cardType);
        iEnterInTheCardNumberField(cardNumber);
        iEnterTheNameOnCardField("ORDER TEST");
        iSelectAndFromTheExpiresEndDropdownLists("01", year);
        iEnterInTheSecurityCodeField(securityCode);
        iPlaceTheOrder();
        if(Boolean.valueOf(challenge))
        {
            iWillGetOneTimePasswordPopUpEnterOneTimePassword();
            iSubmitOneTimePassword();
        }
        iShouldSeeTheorderConfirmationSteps();

        headerPage.selectPaymentCardsOption();
        if(!paymentCardsPage.isCardOnPage(cardType, digits)){
            System.out.println(String.format("NO SAVED %s %s CARD FOUND",cardType, digits));
        }
    }

    @And("^I will get One Time Password pop-up for threeDSecureVone and enter One Time Password$")
    public void iWillGetOneTimePasswordPopUpForThreeDSecureVoneAndEnterOneTimePassword()  {
        checkOutPage.enterOTPFor3dSV1();
    }

    @And("^I submit  One Time Password for threeDSecureVone$")
    public void iSubmitOneTimePasswordForThreeDSecureVone()  {
        checkOutPage.submitOTPFor3dSV1();
    }

    @When("^I edit the order from Review and Pay page$")
    public void iEditOrderFromReviewAndPayPage(){
        commonFunctions.scrollIntoViewByCss(editShoppingBagFromReviewAndPay);
        commonFunctions.clickElementByCSS(editShoppingBagFromReviewAndPay);
    }
}
