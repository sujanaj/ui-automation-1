package stepdefinition.account;

import commonfunctions.CommonFunctions;
import cucumber.api.java.en.And;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.asserts.SoftAssert;
import pages.signin.RegisterSignInPage;
import pages.account.ContactDetailsPage;

import java.util.List;

public class ContactDetailsDefs {

    SoftAssert assertion = new SoftAssert();
    ContactDetailsPage contactDetailsPage = new ContactDetailsPage();
    RegisterSignInPage registerSignInPage = new RegisterSignInPage();
    CommonFunctions commonFunctions = new CommonFunctions();

    @And("^I change the Uni-Pass to (.*)$")
    public void iChangeTheUniPassToUni_Pass(String pccc) {
        assertion.assertTrue(contactDetailsPage.isPCCCDisplayed(),"Failure: Uni-Pass field should be visible");
        assertion.assertAll();
        contactDetailsPage.enterPCCC(pccc);
    }

    @And("^I save the Personal Details$")
    public void iSaveThePersonalDetails() {
        contactDetailsPage.clickDetailsFormSaveButton();
    }

    @And("^A success message should be displayed in (.*)$")
    public void verifySaveSuccessMessage(String language) {
        String expectedMessageEN = "Your profile has been updated";
        String expectedMessageKOR = "고객님의 개인 정보가 변경되었습니다. ";
        String actualMessage = contactDetailsPage.grabDetailsFormAlertMessage();
        if(language.equalsIgnoreCase("English")) {
            assertion.assertTrue(actualMessage.contains(expectedMessageEN), "Failure: Personal details save message not as expected. Expected: " + expectedMessageEN + " -- Actual: " + actualMessage);
        } else if (language.equalsIgnoreCase("한국어")) {
            assertion.assertTrue(actualMessage.contains(expectedMessageKOR), "Failure: Personal details save message not as expected. Expected: " + expectedMessageKOR + " -- Actual: " + actualMessage);
        }
        assertion.assertAll();
    }


    @And("^the Uni-Pass should contain (.*)$")
    public void theUniPassShouldContainUni_Pass(String pccc) {

        String actualLabel = contactDetailsPage.grabPCCC().trim();
        assertion.assertTrue(actualLabel.contains(pccc),"Failure: Personal details Uni-Pass not as expected. Expected: " + pccc + " -- Actual: " + actualLabel);
        assertion.assertAll();
    }

    @And("^Uni-Pass field should not be visible to customer$")
    public void uniPassFieldShouldNotBeVisibleToCustomer() {
        assertion.assertFalse(contactDetailsPage.isPCCCDisplayed(),"Failure: Uni-Pass shouldn't be visible");
        assertion.assertAll();
    }

    @When("^I change the First Name to (.*)$")
    public void changeFirstName(String firstName) {
        registerSignInPage.inputFirstName(firstName);

    }

    @Then("^the Title should contain (.*)$")
    public void verifyTitle(String expected) {
        String actualLabel = registerSignInPage.grabTitle().trim();
        assertion.assertTrue(actualLabel.contains(expected), "Failure: Personal details title not as expected. Expected: " + expected + " -- Actual: " + actualLabel);
        assertion.assertAll();
    }

    @Then("^the First Name should contain (.*)$")
    public void verifyFirstName(String expected) {
        String actualLabel = registerSignInPage.grabFirstName().trim();
        assertion.assertTrue(actualLabel.contains(expected),"Failure: Personal details First Name not as expected. Expected: " + expected + " -- Actual: " + actualLabel);
        assertion.assertAll();
    }

    @Then("^the Last Name should contain (.*)$")
    public void verifyLastName(String expected) {
        String actualLabel = registerSignInPage.grabLastName().trim();
        assertion.assertTrue(actualLabel.contains(expected),"Failure: Personal details Last Name not as expected. Expected: " + expected + " -- Actual: " + actualLabel);
        assertion.assertAll();
    }

    @Then("^the Phone Number should contain (.*)$")
    public void verifyPhoneNumber(String expected) {
        String actualLabel = registerSignInPage.grabPhone().trim();
        assertion.assertTrue(actualLabel.contains(expected),"Failure: Personal details Phone not as expected. Expected: " + expected + " -- Actual: " + actualLabel);
        assertion.assertAll();
    }

    @Then("^I should see the validation error message '(.*)' on save$")
    public void iShouldSeeAValidationErrorMessage(String message)  {
        List<String> validationMessages = contactDetailsPage.getErrorMessage();
        for (String validationMessage : validationMessages) {
            assertion.assertEquals(validationMessage, message, "Failure. The Validation Message is NOT the expected one");
        }
        assertion.assertAll();
    }

    @Then("^entering valid details in the 'CHANGE PASSWORD' form and clicking 'SAVE PASSWORD' triggers a success message '(.*)'$")
    public void enteringValidDetailsInTheCHANGEPASSWORDFormAndClickingSAVEPASSWORDTriggersASuccessMessageSuccess_message(String expectedMessage) {
        contactDetailsPage.setCurrentPassword();
        contactDetailsPage.setNewPassword();
        contactDetailsPage.setConfirmPassword();
        contactDetailsPage.clickPasswordFormSaveButton();

        assertion.assertTrue(contactDetailsPage.grabDetailsFormAlertMessage().contains(expectedMessage),"Failure: Personal details save message not as expected. Expected: " + expectedMessage + " -- Actual: " + contactDetailsPage.grabDetailsFormAlertMessage());
        assertion.assertAll();
    }

    @And("^I select birthday date and month$")
    public void iSelectBirthdayDateAndMonth()  {
        contactDetailsPage.selectBirthDayDate();
        commonFunctions.waitForPageToLoad();
        contactDetailsPage.selectBirthDayMonth();
        contactDetailsPage.clickBirthDayDetailsSubmitButton();
    }

    @Then("^I should save birthday details and trigger a success message '(.*)'$")
    public void iShouldSaveBirthdayDetailsAndTriggerASuccessMessageSuccess_message(String expectedMessage) {
        assertion.assertTrue(contactDetailsPage.grabDetailsFormAlertMessage().contains(expectedMessage),"Failure: BirthDay details save message not as expected. Expected: " + expectedMessage + " -- Actual: " + contactDetailsPage.grabDetailsFormAlertMessage());
        assertion.assertAll();
    }
}
