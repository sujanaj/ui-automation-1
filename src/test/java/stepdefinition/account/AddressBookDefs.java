package stepdefinition.account;

import commonfunctions.CommonFunctions;
import commonfunctions.CustomVerification;
import commonfunctions.NavigateUsingURL;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.testng.asserts.SoftAssert;
import pages.account.AddressBookPage;

import java.util.ArrayList;
import java.util.List;

public class AddressBookDefs {

    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
    AddressBookPage addressBookPage = new AddressBookPage();
    SoftAssert assertion = new SoftAssert();
    CommonFunctions commonFunctions= new CommonFunctions();
    private String enterAddressManuallyLink = obj_Property.readProperty("enterAddressManuallyLink");

    @And("^I navigate to the Address Book page$")
    public void iNavigateToTheAddressBookPage()
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
       // abstractSteps.navigateTo(ConfigUtils.getBaseUrl() + URL_PATH_ADDRESS_BOOK_PAGE);
        commonFunctions.waitForPageToLoad();
        navigateUsingURL.navigateToRelativeURL(obj_Property.readProperty("URL_ADDRESS_BOOK_PAGE"));
        commonFunctions.waitForPageToLoad();
    }

    @And("I click on 'ADD A NEW ADDRESS'")
    public void iClickOnAddANewAddress()
    {
         if(commonFunctions.isElementDisplayed(".save__address")){
                commonFunctions.selectRadioButton(enterAddressManuallyLink);
                addressBookPage.saveAddressDetails("ADDRESS LINE 1", "ADDRESS LINE 2", "CITY", "54321");
            }else
                addressBookPage.clickOnAddNewAddressButton();
    }

    @And("I should see the 'ADD ADDRESS' section")
    public void iShouldSeeTheAddAddressSection()
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        Boolean isDisplayed = addressBookPage.isAddAddressFormDisplayed();
        assertion.assertTrue(isDisplayed,"Failure. I should see the 'ADD ADDRESS' section. Actual result: \" + isDisplayed + \" -- Expected: true");
        assertion.assertAll();
    }

    @And("clicking 'CANCEL' should close the '(.*)' form")
    public void clickingCancelClosesAddAddressForm(String form)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + form);
        addressBookPage.clickOnCancelButton();
        Boolean isDisplayed = addressBookPage.isAddAddressFormDisplayed();
        assertion.assertFalse(isDisplayed,"Failure. Clicking 'CANCEL' should close the " + form
                + " form. Actual result: false. -- Expected: " + isDisplayed );
        assertion.assertAll();
    }

    @And("I click 'EDIT' underneath the last address in my Address Book")
    public void clickEditUnderLastAddress()
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        addressBookPage.clickEditUnderLastAddress();
    }

    @And("I use the 'EDIT ADDRESS' form to enter INVALID firstName or lastName")
    public void iUseTheEditAddressFormToEditAddressDetailsWithInvalidData(DataTable dataTable)
    {
       // logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() > 0)
        {
            for (int i = 1; i < rawTable.size(); i++)
            {
               // logger.info("Row data: " + rawTable.get(i));
                List<String> row = rawTable.get(i);
                String firstName = row.get(0);
                String lastName = row.get(1);

                addressBookPage.saveAddressNameAndSurname(firstName, lastName);
            }
        }
    }


    @Then("^I should see the validation error message '(.*)'$")
    public void iShouldSeeAValidationErrorMessage(String message)  {
        List<String> validationMessages = addressBookPage.getValidationMessage();
        for (String validationMessage : validationMessages) {
            assertion.assertEquals(validationMessage, message, "Failure. The Validation Message is NOT the expected one");
            assertion.assertAll();
        }
    }

    @And("^I use the 'EDIT ADDRESS' form to enter valid firstName or lastName$")
    public void iUseTheEDITADDRESSFormToEnterValidFirstNameOrLastName(DataTable dataTable)  {
        iUseTheEditAddressFormToEditAddressDetailsWithInvalidData(dataTable);
    }

    @And("^It should close the '(.*)' form$")
    public void itShouldCloseTheEDITADDRESSForm(String form)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + form);
        Boolean isDisplayed = addressBookPage.isAddAddressFormDisplayed();
        assertion.assertFalse(isDisplayed,"Failure. Clicking 'SAVE' should close the " + form
                + " form. Actual result: false. -- Expected: " + isDisplayed);
        assertion.assertAll();
    }

    @And("I delete the following address in my Address Book")
    public void iDeleteAddressFromMyAddressBook(DataTable dataTable)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
        {
            for (int i = 1; i < rawTable.size(); i++)
            {
                List<String> row = rawTable.get(i);
                String ad1 = row.get(0);
                String city = row.get(1);
                String county = row.get(2);
                String code = row.get(3);

                Boolean addressExists = addressBookPage.checkIfAddressExists(ad1, city, county, code);
                if (addressExists)
                {
                     addressBookPage.clickOnDeleteButtonForGivenAddress(ad1, city, county, code);
                     addressBookPage.clickOnDeleteOnConfirmationOverlay();
                 }
            }
        }
    }

    @And("I use the 'ADD ADDRESS' Postcode Lookup feature")
    public void iUseTheAddAddressPostcodeLookupFeature(DataTable dataTable)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
        {
            for (int i = 1; i < rawTable.size(); i++)
            {
               // logger.info("Row data: " + rawTable.get(i));
                List<String> row = rawTable.get(i);
                String number = row.get(0);
                String code = row.get(1);

                addressBookPage.enterAddress(code);
            }
        }
    }

    @And("I save the address in my Address Book")
    public void iSaveTheAddressInMyAddressBook()
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        addressBookPage.clickOnSaveAddress();
    }

    @And("the first address in my Address Book contains the expected address")
    public void theAddAddressFormShouldContainTheExpectedAddress(DataTable table)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        CustomVerification customVer = new CustomVerification();

        List<List<String>> rawTable = table.asLists();
        if (rawTable.size() >= 2)
        {
            for (int i = 1; i < rawTable.size(); i++)
            {
               // logger.info("Row data: " + rawTable.get(i));
                List<String> row = rawTable.get(i);
                String ad1 = row.get(0);
                String city = row.get(1);
                String county = row.get(2);
                String code = row.get(3);

                List<String> actualDetails = addressBookPage.getFirstAddressItemDetails();
                customVer.verifyTrue("Failure. Address 1 is: " + actualDetails.get(0) + " -- Expected: " + ad1,
                        actualDetails.get(0).trim().equalsIgnoreCase(ad1));
                customVer.verifyTrue("Failure. City: " + actualDetails.get(1) + " -- Expected: " + city,
                        actualDetails.get(1).trim().equalsIgnoreCase(city));
                customVer.verifyTrue("Failure. County is: " + actualDetails.get(2) + " -- Expected: " + county,
                        actualDetails.get(2).trim().equalsIgnoreCase(county));
                customVer.verifyTrue("Failure. Postcode is: " + actualDetails.get(3) + " -- Expected: " + code,
                        actualDetails.get(3).trim().contains(code));
            }
        }
        customVer.verifyNoErrors();
    }


    @And("I use the 'ADD ADDRESS' form to create a new address for '(.*)'")
    public void iUseTheAddAddressFormToCreateANewAddress(String country, DataTable table) {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        //Assert.assertTrue("Failure: Uni-Pass field should be visible", contactDetailsPage.isPCCCDisplayed());
        commonFunctions.waitForPageToLoad();
        List<List<String>> rawTable = table.asLists();
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
                //logger.info("Row data: " + rawTable.get(i));
                List<String> row = rawTable.get(i);
                String ad1 = row.get(0);
                String ad2 = row.get(1);
                String city = row.get(2);
                String code = row.get(3);
                String pccc = row.get(4);
                if (country.equalsIgnoreCase("Korea Republic of")) {
                    addressBookPage.saveAddressDetailsWithPCCC(ad1, ad2, city, code, pccc);

                } else {
                    addressBookPage.saveAddressDetails(ad1, ad2, city, code);
                }
            }
        }
    }

    @And("the '(.*)' form on Address Book page disappears")
    public void theAddAddressFormDisappears(String form)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + form);
        commonFunctions.waitForPageToLoad();
        Boolean isDisplayed = addressBookPage.isAddAddressFormDisplayed();
        assertion.assertFalse(isDisplayed,"Failure. The " + form + " form disappears. Actual result: false. -- Expected: " + isDisplayed);
        assertion.assertAll();
    }

    @And("the first line of the first address in my Address Book contains '(.*)'")
    public void theFirstAddressContainsAddress1(String address)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + address);
        String actualAddress = addressBookPage.getFirstAddressLine1();
         address = addressBookPage.getExpectedFirstAddressLine1(address);
         assertion.assertTrue(actualAddress.equalsIgnoreCase(address),"Failure. The first line of the first address in my Address Book contains: " + actualAddress
                + " -- Expected: " + address);
        assertion.assertAll();
    }

    @And("the fifth line of the first address in my Address Book contains '(.*)'")
    public void theFirstAddressContainsAddress1InLine5(String address)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + address);
        String actualAddress = addressBookPage.getFirstAddressLine1ForJPN();
        assertion.assertTrue(actualAddress.equalsIgnoreCase(address),"Failure. The fifth line of the first address in my Address Book contains: " + actualAddress
                + " -- Expected: " + address);
        assertion.assertAll();
    }

    @And("I click Delete under the first address in my Address Book")
    public void iClickDeleteFirstAddress()
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        addressBookPage.clickOnDeleteForFirstAddress();
    }

    @And("the 'DELETE ADDRESS' confirmation overlay appears")
    public void theDeleteAddressConfirmationOverlayAppears()
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        Boolean isDisplayed = addressBookPage.isDeleteConfirmationOverlayDisplayed();
        assertion.assertTrue(isDisplayed,"Failure. The 'DELETE ADDRESS' confirmation overlay appears. Actual result: " + isDisplayed
                + " -- Expected: true");
        assertion.assertAll();
    }

    @And("I click 'DELETE' on the 'DELETE ADDRESS' confirmation overlay")
    public void iClickDeleteOnTheConfirmationOverlay()
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        addressBookPage.clickOnDeleteOnConfirmationOverlay();
    }

    @And("the first line of the first address in my Address Book should not contain '(.*)'")
    public void theFirstAddressShouldNotContainAddress1(String address)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + address);
        String actualAddress = addressBookPage.getFirstAddressLine1();
        assertion.assertFalse(actualAddress.equalsIgnoreCase(address),"Failure. The first line of the first address in my Address Book should not contain: "
                + actualAddress + " -- Expected: ");
        assertion.assertAll();
    }

    @And("the fifth line of the first address in my Address Book should not contain '(.*)'")
    public void theFirstAddressShouldNotContainAddress1InLine5(String address)
    {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + address);
        String actualAddress = addressBookPage.getFirstAddressLine1ForJPN();
        assertion.assertFalse(actualAddress.equalsIgnoreCase(address),"Failure. The fifth line of the first address in my Address Book should not contain: "
                + actualAddress + " -- Expected: ");
        assertion.assertAll();
    }

    @And("^All address form fields are available for '(.*)'$")
    public void allAddressFormFieldsAreAvailable(String language) {
        List<String> expectedAddressFormFieldsAttribute = new ArrayList<String>();
        if (language.equalsIgnoreCase("日本語")) {
            expectedAddressFormFieldsAttribute.add("address.title");
            expectedAddressFormFieldsAttribute.add("address.surname");
            expectedAddressFormFieldsAttribute.add("address.firstName");
            expectedAddressFormFieldsAttribute.add("address.country");
            expectedAddressFormFieldsAttribute.add("address.postcode");
            expectedAddressFormFieldsAttribute.add("address.townCity");
            expectedAddressFormFieldsAttribute.add("address.line2");
            expectedAddressFormFieldsAttribute.add("address.line1");
            expectedAddressFormFieldsAttribute.add("address.line3");
            expectedAddressFormFieldsAttribute.add("address.phone");
        } else {
            expectedAddressFormFieldsAttribute.add("address.title");
            expectedAddressFormFieldsAttribute.add("address.firstName");
            expectedAddressFormFieldsAttribute.add("address.surname");
            expectedAddressFormFieldsAttribute.add("address.country");
            expectedAddressFormFieldsAttribute.add("address.line1");
            expectedAddressFormFieldsAttribute.add("address.line2");
            expectedAddressFormFieldsAttribute.add("address.line3");
            expectedAddressFormFieldsAttribute.add("address.townCity");
            expectedAddressFormFieldsAttribute.add("address.countyState");
            expectedAddressFormFieldsAttribute.add("address.postcode");
            expectedAddressFormFieldsAttribute.add("address.phone");
        }
        commonFunctions.waitForPageToLoad();
        assertion.assertTrue(expectedAddressFormFieldsAttribute.equals(addressBookPage.validateAddressFormFields()), "Failure: Address fields are not in expected sequence");
        assertion.assertAll();
    }

    @When("^I enter address details in 'ADD ADDRESS' form to create a new address$")
    public void iEnterAddressDetailsInADDADDRESSFormToCreateANewAddress(DataTable table)  {
        List<List<String>> rawTable = table.asLists();
        if (rawTable.size() >= 2)
        {
            for (int i = 1; i < rawTable.size(); i++)
            {
                List<String> row = rawTable.get(i);
                String country = row.get(0);
                String ad1 = row.get(1);
                String ad2 = row.get(2);
                String city = row.get(3);
                String state = row.get(4);
                String code = row.get(5);

                addressBookPage.populateAddressDetails(country, ad1, ad2, city, state, code);
            }
        }
    }

    @And("^I click in 'SAVE ADDRESS'$")
    public void iClickInSAVEADDRESS()  {
        addressBookPage.clickOnSaveAddress();
    }

    @And("^the third line of the first address in my Address Book contains '(.*)'$")
    public void theThirdLineOfTheFirstAddressInMyAddressBookContainsAddress_line_(String address){
        String actualAddress = addressBookPage.getFirstAddressLine3();
        assertion.assertTrue(actualAddress.equalsIgnoreCase(address),"Failure. The third line of the first address in my Address Book contains: " + actualAddress
                + " -- Expected: " + address);
        assertion.assertAll();
    }

    @And("^I click Edit underneath the '(.*)' address$")
    public void iClickEditUnderneathTheElSalvadorAddress(String country) {
        addressBookPage.clickEditWhereCountryIs(country);
    }

    @When("^I use the 'EDIT ADDRESS' form to enter valid address details for '(.*)'$")
    public void iUseTheEDITADDRESSFormToEnterValidAddressDetails(String country, DataTable table) {
        iUseTheAddAddressFormToCreateANewAddress(country,table);
    }

    @And("^my '(.*)' address contains the updated address details$")
    public void myElSalvadorAddressContainsTheUpdatedAddressDetails(String country, DataTable table)  {
        CustomVerification customVer = new CustomVerification();

        List<List<String>> rawTable = table.asLists();
        if (rawTable.size() >= 2)
        {
            for (int i = 1; i < rawTable.size(); i++)
            {
                List<String> row = rawTable.get(i);
                String ad1 = row.get(0);
                String ad2 = row.get(1);
                String code = row.get(2);
                String city = row.get(3);

                List<String> actualDetails = addressBookPage.getAddressDetails(country);
                customVer.verifyTrue("Failure. Address 1 is: " + actualDetails.get(0) + " -- Expected: " + ad1,
                        actualDetails.get(0).equals(ad1));
                customVer.verifyTrue("Failure. Address 2 is: " + actualDetails.get(1) + " -- Expected: " + ad2,
                        actualDetails.get(1).equals(ad2));
                customVer.verifyTrue("Failure. Postcode is: " + actualDetails.get(2) + " -- Expected: " + ad1,
                        actualDetails.get(2).equals(code));
                customVer.verifyTrue("Failure. City is: " + actualDetails.get(3) + " -- Expected: " + ad1,
                        actualDetails.get(3).equals(city));
            }
        }
        customVer.verifyNoErrors();
    }

    @And("^the 'SAVE ADDRESS' button text is changed to '(.*)'$")
    public void theSAVEADDRESSButtonTextIsChangedToAddress_saved(String text) {
        String actualText = addressBookPage.getSaveAddressButtonText();
        assertion.assertTrue(actualText.equals(text),"Failure. Clicking 'SAVE ADDRESS' on the Shipping Address form changes the button text to: "
                + actualText + " -- Expected: " + text);
        assertion.assertAll();
    }
}
