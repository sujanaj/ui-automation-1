package stepdefinition.account;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.testng.asserts.SoftAssert;
import pages.account.ManagePreferencePage;

public class ManagePreferenceDefs {
    SoftAssert assertion = new SoftAssert();
    ManagePreferencePage managePreferencePage = new ManagePreferencePage();

    @Then("^Account should be created with Other Preferences Options as selected$")
    public void accountShouldBeCreatedWithOtherPreferencesOptionsAsSelected() {
        assertion.assertTrue(managePreferencePage.isPreferencesSelected(),"Failure: Preferences are not selected.");
        assertion.assertAll();
    }

    @Then("^Account should be created with Other Preferences Options as deselected$")
    public void accountShouldBeCreatedWithOtherPreferencesOptionsAsDeselected() {
        assertion.assertTrue(managePreferencePage.isPreferencesDeselected(), "Failure: Preferences are selected.");
        assertion.assertAll();
    }

    @When("^I select all the Manage Preferences consent displayed$")
    public void iSelectAllTheManagePreferencesConsentDisplayed() {
        managePreferencePage.selectPreferences();
    }

    @And("^I click on Save Other Preferences button on Manage Preferences page$")
    public void iClickOnSaveOtherPreferencesButton()  {
        managePreferencePage.savePreferences();
    }


    @Then("^I should able to save the Other Preferences Options as selected$")
    public void iShouldAbleToSaveTheOtherPreferencesOptionsAsSelectedSuccessfully() {
        accountShouldBeCreatedWithOtherPreferencesOptionsAsSelected();
    }

    @When("^I deselect all the Manage Preferences consent displayed$")
    public void iDeselectAllTheManagePreferencesConsentDisplayed() {
        managePreferencePage.deselectPreferences();

    }

    @Then("^I should able to save the Other Preferences Options as deselected$")
    public void iShouldAbleToSaveTheOtherPreferencesOptionsAsDeselectedSuccessfully()  {
        accountShouldBeCreatedWithOtherPreferencesOptionsAsDeselected();
    }
}
