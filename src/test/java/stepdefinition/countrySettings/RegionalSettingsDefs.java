package stepdefinition.countrySettings;


import baseclass.BaseClass;
import baseclass.WebDriverFactory;
import commonfunctions.ProductDetailsFromJson;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import config.Constants;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.asserts.SoftAssert;
import pages.HeaderSettingsPage;
import io.cucumber.datatable.DataTable;

import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RegionalSettingsDefs extends BaseClass {

    CommonFunctions commonFunctions = new CommonFunctions();
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    HeaderSettingsPage headerSettingsPage = new HeaderSettingsPage();


    SoftAssert softAssert = new SoftAssert();
    public String scenarioName;
    public String countryName;
    WebDriver driver;

    @Before
    public synchronized void before(Scenario scenario) {

        if (WebDriverFactory.browser.equalsIgnoreCase("Edge")) {
            getWebDriver().quit();
            System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
            // WebDriver driver = new EdgeDriver();
            driver = new EdgeDriver();
            setWebDriver(driver);
            getWebDriver().manage().window().maximize();

        }
        scenarioName = scenario.getName();
        try {
            FileWriter writer = new FileWriter("scenarioName.txt", false);
            writer.write(scenarioName);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


//    @Before
//    public synchronized void before(Scenario scenario) {
//
////        if(WebDriverFactory.browser.equalsIgnoreCase("Firefox")){
////            getWebDriver().quit();
////            System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver");
////            WebDriver driver= new FirefoxDriver();
////            setWebDriver(driver);
////        }
//        scenarioName= scenario.getName();
//        try {
//            FileWriter writer = new FileWriter("scenarioName.txt", false);
//            writer.write(scenarioName);
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    @Given("^I am on the website with '(.*)' set as delivery country$")
    public void iAmOnTheWebsiteWithDeliveryCountry(String country) {
        //  logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + " " + country);
        if (Constants.COUNTRY_ISOCODE_MAP.containsKey(country)) {
            navigateUsingURL.iAmOnTheWebsiteWithDeliveryCountry(Constants.COUNTRY_ISOCODE_MAP.get(country));
            commonFunctions.skipPopups();
        } else {
            navigateUsingURL.iAmOnTheWebsiteWithDeliveryCountry(null);
        }
        commonFunctions.skipPopups();
    }

    @When("^I set my country '(.*)'$")
    public void iSetMyCountry(String country) {
        iSetMyCountryAs(country);
    }

    @And("^I set my country as '(.*)'$")
    public void iSetMyCountryAs(String country) {
        String cssLocatorShippingCountry = obj_Property.readProperty("countryDropDownSettingsPageById");
        String cssLocatorSaveSettings = obj_Property.readProperty("saveSettingsButtonSettingsPageByClassName");
        String shippingCountry = obj_Property.readProperty("shippingCountrySettingsHeaderById");
        commonFunctions.clickElementByCSS(shippingCountry);
        commonFunctions.selectFromDropdownByCSS(cssLocatorShippingCountry, country);
        commonFunctions.waitForPageToLoad();
        commonFunctions.scrollScreen300Pixel();
        commonFunctions.clickElementByCSS(cssLocatorSaveSettings);
        commonFunctions.waitForPageToLoad();
        commonFunctions.refreshPage();
    }

    @When("^I am on the website with '(.*)' '(.*)' '(.*)'$")
    public void iSetMyCountryLanguageCurreny(String country, String language, String currency) {
        countryName = country;
        try {
            FileWriter writer = new FileWriter("countryName.txt", false);
            writer.write(countryName);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        iAmOnTheWebsiteWithDeliveryCountry(null);

        //this commented code will be removed after successful dry run

//        String cssLocatorShippingCountry = obj_Property.readProperty("countryDropDownSettingsPageById");
//        String cssLocatorSaveSettings = obj_Property.readProperty("saveSettingsButtonSettingsPageByClassName");
//        String shippingCountry = obj_Property.readProperty("shippingCountrySettingsHeaderByCSS");
//        String languageDropDownByCSS = obj_Property.readProperty("languageDropDownSettingsPageBydId");
//
//        commonFunctions.clickElementByCSS(shippingCountry);
//        commonFunctions.selectFromDropdownByCSS(cssLocatorShippingCountry, country);
//        commonFunctions.waitForPageToLoad();
//        if (Constants.COUNTRY.containsKey(country)) {
//            if (language.isEmpty()) {
//
//            } else {
//                commonFunctions.selectFromDropdownByCSS(languageDropDownByCSS, language);
//            }
//        }
//        if (currency.isEmpty()) {
//
//        } else {
//            String currencyByCSS = obj_Property.readProperty("currencyDropDownSettingsPageById");
//            commonFunctions.selectFromDropdownByCSS(currencyByCSS, currency);
//        }
//
//        commonFunctions.waitForPageToLoad();
//        commonFunctions.scrollScreen300Pixel();
//        commonFunctions.waitForPageToLoad();
//        commonFunctions.clickElementByCSS(cssLocatorSaveSettings);
//        commonFunctions.waitForPageToLoad();

        // this change has been made to fix the country, language switch issue

        if (country.equalsIgnoreCase("United Kingdom")) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("");

        } else if (country.equalsIgnoreCase("United States")) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/us/");
        } else if (country.equalsIgnoreCase("Australia")) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/au/");
        } else if (country.equalsIgnoreCase("Hong Kong")) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/intl/");
        } else if (country.equalsIgnoreCase("France") && (language.equalsIgnoreCase("Français"))) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/fr");
        } else if (country.equalsIgnoreCase("France") && (language.equalsIgnoreCase("English")) || (country.equalsIgnoreCase("France") && (language.isEmpty()))) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/en-fr");
        } else if (country.equalsIgnoreCase("Japan") && (language.equalsIgnoreCase("日本語"))) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/jp");
        } else if (country.equalsIgnoreCase("Japan") && (language.equalsIgnoreCase("English")) || (country.equalsIgnoreCase("Japan") && (language.isEmpty()))) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/en-jp");
        } else if (country.equalsIgnoreCase("Korea Republic of") && (language.equalsIgnoreCase("한국어"))) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/kr");
        } else if (country.equalsIgnoreCase("Korea Republic of") && (language.equalsIgnoreCase("English")) || (country.equalsIgnoreCase("Korea Republic of") && (language.isEmpty()))) {
            navigateUsingURL.changeLanguageAndCountryUsingURL("/en-kr");
        }
        commonFunctions.refreshPage();
        commonFunctions.waitForPageToLoad();

        String currencyOptionsListLocator = obj_Property.readProperty("currencyListSettingsHeaderByID");
        if (currency.isEmpty()) {

        } else if (currency.equalsIgnoreCase("US Dollar ($)")) {
            currency = "USD $";
            commonFunctions.selectFromDropdownByCSS(currencyOptionsListLocator, currency);
        } else if (currency.equalsIgnoreCase("Euro (€)")) {
            currency = "EUR €";
            commonFunctions.selectFromDropdownByCSS(currencyOptionsListLocator, currency);
        } else if (currency.equalsIgnoreCase("Japanese Yen (¥)")) {
            currency = "JPY ¥";
            commonFunctions.selectFromDropdownByCSS(currencyOptionsListLocator, currency);
        } else if (currency.equalsIgnoreCase("British Pound (£)")) {
            currency = "GBP £";
            commonFunctions.selectFromDropdownByCSS(currencyOptionsListLocator, currency);
        } else if (currency.equalsIgnoreCase("Australian Dollar ($)")) {
            currency = "AUD $";
            commonFunctions.selectFromDropdownByCSS(currencyOptionsListLocator, currency);
        } else if (currency.equalsIgnoreCase("Hong Kong Dollar ($)")) {
            currency = "HKD $";
            commonFunctions.selectFromDropdownByCSS(currencyOptionsListLocator, currency);
        }
        commonFunctions.waitForPageToLoad();
    }


    @Given("^I am on the website$")
    public void iAmOnTheWebsite() {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        navigateUsingURL.iAmOnTheWebsiteWithDeliveryCountry(null);
    }


    @When("^I navigate to '(.*)'$")
    public void iNavigateToUrl(String url) {
        if (url.equalsIgnoreCase("designers_page")) {
            url = Constants.DESIGNERS_LINK;
        }
        navigateUsingURL.navigateToRelativeURL(url);
    }


    @Then("^I should see selected currency as '(.*)' on PDP page$")
    public void productPriceCurrencyOnPDPPage(String pdpCurrency) {
        String expectedCurrency = Normalizer.normalize(pdpCurrency.trim(), Normalizer.Form.NFKC);
        String actualCurrency = headerSettingsPage.getProductPriceCurrency();
        actualCurrency = Normalizer.normalize(actualCurrency, Normalizer.Form.NFKC);
        Assert.assertTrue(actualCurrency.equalsIgnoreCase(expectedCurrency));
//        softAssert.assertTrue(actualCurrency.equalsIgnoreCase(expectedCurrency),"Failure. I should be able see selected currency as   "+expectedCurrency+"   on PDP page but actual currency is  "
//                + actualCurrency);
//        softAssert.assertAll();

    }

    @Then("^I should only be able to switch to the following currencies using the Homepage Currency Dropdown List$")
    public void iSwitchToGivenCurrencyUsingTheHomepageDropdown(DataTable dataTable) {
        //  logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        List<String> expectedCurrencyList = new ArrayList<String>();
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
                String currency = rawTable.get(i).get(0);
                expectedCurrencyList.add(currency);
            }
        }
        List<String> actualCurrencyList = headerSettingsPage.checkCurrencyList();
        List<String> actualPureCurrencyList = new ArrayList<String>();
        for (String actualCurrency : actualCurrencyList) {
            String pureCurrency = actualCurrency.trim().split(" ")[0];
            actualPureCurrencyList.add(pureCurrency.trim());
        }

        Collections.sort(actualPureCurrencyList);
        Collections.sort(expectedCurrencyList);

        softAssert.assertTrue(actualPureCurrencyList.equals(expectedCurrencyList), "Failure. I should be able to switch to the following currencies using the Homepage Currency Dropdown List. Actual currency list: "
                + actualPureCurrencyList + " -- Expected: " + expectedCurrencyList);
        softAssert.assertAll();

    }

    @And("^refresh the page$")
    public void refresh() {
        commonFunctions.refreshPage();
    }

    @Then("^I am redirected to the Sign In page$")
    public void iAmRedirectedToTheSignInPage() {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        commonFunctions.waitForPageToLoad();
        String urlLocation = commonFunctions.getCurrentURL();
        Assert.assertTrue("Failed: Expected to be on the sign in page. Actual: " + urlLocation,
                urlLocation.contains("login"));
    }

}
