package stepdefinition.wishlist;
import commonfunctions.CommonFunctions;
import commonfunctions.CustomVerification;
import commonfunctions.NavigateUsingURL;
import commonfunctions.ProductDetailsFromJson;
import config.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;
import pages.HeaderSettingsPage;
import pages.HeaderPage;
import pages.MiniCartPage;
import pages.PreHomePage;
import pages.WishlistPage;
import java.util.ArrayList;
import java.util.List;


public class WishlistDefs {
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    CommonFunctions commonFunctions = new CommonFunctions();
    HeaderSettingsPage headerSettingsPage= new HeaderSettingsPage();
    SoftAssert softAssert = new SoftAssert();
    private static String firstItemID;
    WishlistPage wishlistPage = new WishlistPage();
    ProductDetailsFromJson productDetailsFromJson = new ProductDetailsFromJson();
    private NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    String wishlistButtonLocatorByCSS = obj_Property.readProperty("wishlistButtonLocatorByCSS");
    String wishlistAddedToWishlistByCSS = obj_Property.readProperty("wishlistAddedToWishlistByCSS");
    String wishlistMiniRollOverByCSS = obj_Property.readProperty("wishlistMiniRollOverByCSS");
    String wishlistButtonByCSS = obj_Property.readProperty("wishlistButtonByCSS");
    String shareSelectedItemsBtnLocatorByCSS = obj_Property.readProperty("shareSelectedItemsBtnLocatorByCSS");
    String wishlistProductCheckboxesLocatorByCSS = obj_Property.readProperty("wishlistProductCheckboxesLocatorByCSS");
    String manageSelectedDropDownByCSS = obj_Property.readProperty("manageSelectedDropDownByCSS");
    String wishlistRecepientNameByCSS=obj_Property.readProperty("wishlistRecepientNameByCSS");
    String wishlistRecepientEmailByCSS=obj_Property.readProperty("wishlistRecepientEmailByCSS");
    String wishlistMessageByCSS=obj_Property.readProperty("wishlistMessageByCSS");
    String wishlisShareTopButtonByCSS=obj_Property.readProperty("wishlisShareTopButtonByCSS");
    String sentWishlistOverlayByCSS=obj_Property.readProperty("sentWishlistOverlayByCSS");
    String sentWishlistOverlayLocator=obj_Property.readProperty("sentWishlistOverlayLocator");
    String wishlistOverlayCloseButtonLocator=obj_Property.readProperty("wishlistOverlayCloseButtonLocator");
   // String shareSelectedItemsBtnLocatorByCSS=obj_Property.readProperty("shareSelectedItemsBtnLocatorByCSS");
   String wishlistTopCancelButtonByCSS=obj_Property.readProperty("wishlistTopCancelButtonByCSS");
  //  String wishlistProductCheckboxesLocatorByCSS=obj_Property.readProperty("wishlistProductCheckboxesLocatorByCSS");
  String wishlistEditButtonByCSS=obj_Property.readProperty("wishlistEditButtonByCSS");
   // String wishlistProductCheckboxesLocatorByCSS=obj_Property.readProperty("wishlistProductCheckboxesLocatorByCSS");
    String wishlistFormNameAttribute = obj_Property.readProperty("wishlistFormNameAttribute");
    String wishlistFormTitleByCSS = obj_Property.readProperty("wishlistFormTitleByCSS");
    String wishlistErrorMessageByCSS = obj_Property.readProperty("wishlistErrorMessageByCSS");

    HeaderPage headerPage = new HeaderPage();
    private static int noOfItems =0;
    CustomVerification customVerification = new CustomVerification();
    PreHomePage preHomePage = new PreHomePage();
    MiniCartPage miniCartPage = new MiniCartPage();
    String wishlistSelectAllCheckbox = obj_Property.readProperty("wishlistSelectAllCheckboxByCSS");
    String wishlistProductsCheckbox = obj_Property.readProperty("wishlistProductsCheckboxByCSS");
    String wishlistContinueShoppingBtn = obj_Property.readProperty("wishlistContinueShoppingBtnByID");
    String wishlistDeleteOverlayCancelBtn = obj_Property.readProperty("wishlistDeleteOverlayCancelBtnByCSS");
    String deleteOverlayMsgonWishlist = obj_Property.readProperty("deleteOverlayMsgonWishlist");
    String pageTitleFromAccountsLeftMenu = obj_Property.readProperty("pageTitleFromAccountsLeftMenuByCSS");
    String wishlistDeleteOverlayClose = obj_Property.readProperty("wishlistDeleteOverlayCloseByCSS");
    String wishlistDeleteOverlayContainer = obj_Property.readProperty("wishlistDeleteOverlayContainerByClassName");
    String wishlistLoadMoreBtn = obj_Property.readProperty("wishlistLoadMoreBtnByID");
    String wishlistDeleteOverlayDeleteBtn = obj_Property.readProperty("wishlistDeleteOverlayDeleteBtnByCSS");
    String wishlistLinkOnShoppingBag = obj_Property.readProperty("wishlistLinkShoppingBagByCSS");


    @When("^I click on the '(.*)' link on the Shopping Bag$")
    public void iClickOnTheAddToWishlistLinkOnTheShoppingBag(String link) {
        commonFunctions.waitForPageToLoad();
       WebElement we = wishlistPage.getWebElementByJQuery(wishlistLinkOnShoppingBag,link);
       commonFunctions.clickOnElement(we);
       commonFunctions.waitForPageToLoad();
    }
    @Then("^the link text changes to '(.*)'$")
    public void verifyWishlistLinkChanges(String expectedLinkText) {
        String wishlistAddedToWishlistByCSS = obj_Property.readProperty("wishlistAddedToWishlistByCSS");
        commonFunctions.getTextFromElementInList(wishlistAddedToWishlistByCSS,expectedLinkText);
    }

    @Then("^the rollover mini Wishlist list appears$")
    public void verifyMiniWishlistRolloverAppears() {

        Boolean isDisplayed = commonFunctions.isElementDisplayed(wishlistMiniRollOverByCSS);
        softAssert.assertFalse(isDisplayed,"Failure. The 'Mini Wishlist' shoud be displayed. Actual result: false -- Expected: " + isDisplayed);
        softAssert.assertAll();
    }

    @And("^I clean my shopping bag$")
    public void iCleanMyShoppingBag() {
        String currentURL = commonFunctions.getCurrentURL();
        navigateUsingURL.navigateToRelativeURL("/at/clearshoppingbag");
        commonFunctions.navigateTo(currentURL);
        commonFunctions.refreshPage();
    }
    @Then("^I remove items from my wishlist$")
    public void iRemoveItemsFromMyWishlist() {
        commonFunctions.navigateToWishlistPage();
        commonFunctions.clickElementByCSS(wishlistSelectAllCheckbox);
        wishlistPage.selectDeleteOptnFromDd();
        commonFunctions.waitForPageToLoad();
       commonFunctions.clickElementByCSS(wishlistDeleteOverlayDeleteBtn);
    }

    @When("^I navigate to the Wishlist page$")
    public void iNavigateToThewishlistSteps() {

        commonFunctions.waitForElementByCssLocator(wishlistButtonByCSS);
        commonFunctions.clickElementByCSS(wishlistButtonByCSS);
    }

    @And("sharing the first product in my wishlist displays the product details in the Share Wishlist email preview")
    public void theEmailPreviewOnShareWishlistDisplaysCorrectProductDetails() {

        List<String> wFirstProdDetails = wishlistPage.getFirstProductRelevantDetails();
        wishlistPage.selectWishlistFirstProduct();

        commonFunctions.scrollToElement(shareSelectedItemsBtnLocatorByCSS);
        commonFunctions.clickElementByCSS(shareSelectedItemsBtnLocatorByCSS);

        List<String> swFirstProdModel = wishlistPage.getProductRelevantDetailsOnShareWishlistPage();
        softAssert.assertTrue(wFirstProdDetails.equals(swFirstProdModel),
                "Failure. The product details in the Share Wishlist email preview are: " + swFirstProdModel
                        + " -- The product details on the Wishlist page are: " + wFirstProdDetails);
        softAssert.assertAll();
    }
    @And("^I tick the first product from my Wishlist$")
    public void iTickTheFirstProductFromMyWishlist() {

        wishlistPage.selectWishlistFirstProduct();
        firstItemID = wishlistPage.getFirstItemID(wishlistProductCheckboxesLocatorByCSS);
    }
    @When("^I select '(.*)' from the drop down list$")
    public void iSelectWishlistActionFromTheDropDownList(String textValue) {
        commonFunctions.selectFromDropdownByText(manageSelectedDropDownByCSS,textValue);
       // commonFunctions.selectFromDropdownByCSS(manageSelectedDropDownByCSS,textValue);
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
    }

    @And("I enter the following credentials in the Share Wishlist page")
    public void iEnterCredentialsInshareWishlistSteps() {


        commonFunctions.enterTextInputBox(wishlistRecepientNameByCSS, Constants.WISHLIST_NAME);
        commonFunctions.enterTextInputBox(wishlistRecepientEmailByCSS, Constants.WISHLIST_EMAIL);
        commonFunctions.enterTextInputBox(wishlistMessageByCSS, Constants.WISHLIST_TEXTMESSAGE);

    }
    @And("I click on the 'SEND' button on the top of the Share Wishlist page")
    public void iClickOnTopSendButtonOnShareWishlist() {

        commonFunctions.clickElementByCSS(wishlisShareTopButtonByCSS);
    }

    @And("the Wishlist Sent overlay appears with title '(.*)' and message '(.*)'")
    public void verifyWishlistSentOverlayAppearsWithMessage(String title, String msg) {

        String actualTitle= commonFunctions.getTextByCssSelector(sentWishlistOverlayByCSS);
        String actualMsg= commonFunctions.getTextByCssSelector(sentWishlistOverlayLocator);
        softAssert.assertTrue(actualTitle.equals(title),
                "The actual title of send wishlist overlay is: " + actualTitle
                        + " But the expected title of send wishlist overlay is: " + title);
        softAssert.assertTrue(actualMsg.equals(msg),
                "The actual msg of send wishlist overlay is: " + actualMsg
                        + " But the expected title of send wishlist overlay is: " + msg);
        softAssert.assertAll();
    }

    @And("I click anywhere on the page outside of the 'WISHLIST SENT' overlay")
    public void iClickAnywhereOutsideOfWishlistSentOverlay() {

        commonFunctions.clickElementByCSS(wishlistOverlayCloseButtonLocator);
    }

    @And("after a few seconds I am navigated back to my Wishlist page")
    public void afterAFewSecondsIAmNavigatedBackToWishlist() {
        String currentUrl = commonFunctions.getCurrentURL();
        softAssert.assertFalse(currentUrl.contains("account/wishlist/message"),
                "Failure. I am not navigated back to my Wishlist page.");
        softAssert.assertTrue(currentUrl.contains("account/wishlist"),"Failure. I am navigated back to my Wishlist page: " + currentUrl);
        softAssert.assertAll();

    }

    @When("^I click on the 'SHARE SELECTED ITEMS' button at the bottom of the Wishlist page$")
    public void iClickOnTheShareSelectedItemsButton() {

        commonFunctions.scrollToElement(shareSelectedItemsBtnLocatorByCSS);
       // commonFunctions.waitForElementByCssLocator(shareSelectedItemsBtnLocatorByCSS);
        commonFunctions.waitForPageToLoad();
        commonFunctions.clickElementByCSS(shareSelectedItemsBtnLocatorByCSS);
    }
    @And("I click on the 'CANCEL' button at the top of the Share Your Wishlist page")
    public void iClickOnCancelAtTopOfshareWishlistSteps() {

        commonFunctions.waitForPageToLoad();
        commonFunctions.scrollScreen300Pixel();
        commonFunctions.waitForPageToLoad();
//        commonFunctions.waitForElementByCssLocator(wishlistTopCancelButtonByCSS);
        commonFunctions.clickElementByCSS(wishlistTopCancelButtonByCSS);
    }

    @And("I am navigated back to the Wishlist page")
    public void iAmNavigatedToThewishlistSteps() {
        String currentUrl = commonFunctions.getCurrentURL();
        Assert.assertFalse("Failure. I am not navigated back to my Wishlist page.",
                currentUrl.contains("account/wishlist/message"));
        Assert.assertTrue("Failure. I am navigated back to my Wishlist page: " + currentUrl,
                currentUrl.contains("account/wishlist"));

    }
    @And("the first product that I ticked is no longer ticked in my Wishlist")
    public void theFirstProductInWishlistIsUnselected() {

        Boolean isSelected = wishlistPage.isFirstCheckboxSelected(wishlistProductCheckboxesLocatorByCSS);
        Assert.assertFalse(
                "Failure. The first product should not be selected. Actual result: false. Expected: " + isSelected,
                isSelected);
    }

    @And("I click on the 'EDIT' button at the top of the Share Your Wishlist page")
    public void iClickOnEditAtTopOfshareWishlistSteps() {

       commonFunctions.scrollScreen300Pixel();
       commonFunctions.scrollToElement(wishlistEditButtonByCSS);
       commonFunctions.waitForElementByCssLocator(wishlistEditButtonByCSS);
       commonFunctions.waitForPageToLoad();
       commonFunctions.clickElementByCSS(wishlistEditButtonByCSS);

    }

    @And("the first product that I ticked is still ticked in my Wishlist")
    public void theFirstProductInWishlistIsSelected() {
//        String wishlistProductCheckboxesLocatorByCSS=obj_Property.readProperty("wishlistProductCheckboxesLocatorByCSS");
//        Boolean isSelected = wishlistPage.isFirstCheckboxSelected(wishlistProductCheckboxesLocatorByCSS);
        commonFunctions.waitForPageToLoad();
//        commonFunctions.waitForElementByCssLocator(wishlistProductCheckboxesLocatorByCSS);
        //  String checkbox = wishlistPage.isCheckboxSelected();
        //  System.out.println(checkbox);
        Boolean isSelected = wishlistPage.isFirstCheckboxSelected(wishlistProductCheckboxesLocatorByCSS);
        Assert.assertTrue(
                "Failure. The first product should be selected. Actual result: " + isSelected + " -- Expected: true",
                isSelected);
    }
    @When("^I click on the 'Wishlist' link in the header$")
    public void iClickOnTheWishlistLinkInTheHeader() {
        headerPage.clickWishlistButton();
    }

    @When("^I click on the link '(.*)' for the first product in my Wishlist$")
    public void iClickOnTheRemoveLinkForTheFirstProduct(String link) {
        commonFunctions.waitForPageToLoad();
        Boolean isCorrect = wishlistPage.isRemoveLinkCorrect(link);
        softAssert.assertTrue(isCorrect,"Failure. The remove link is correct. Actual result: " + isCorrect + " -- Expected: true");
        softAssert.assertAll();
        noOfItems = wishlistPage.getFirstNoOfItem();
        for(int i=0;i<=noOfItems-1;i++) {
            wishlistPage.clickOnFirstRemoveLink();
        }


    }

    @Then("^the first product is removed from my Wishlist$")
    public void theFirstProductIsRemovedFromMyWishlist() {
        int	NoOfItemAferDelete = wishlistPage.getFirstNoOfItem();
        String NoOfItemAferDele = Integer.toString(NoOfItemAferDelete);
        String NoOfItemBeforeDelete = Integer.toString(noOfItems);
        softAssert.assertNotEquals("Failure. The first item is not removed. Actual result: "
                        + NoOfItemBeforeDelete.equals(NoOfItemAferDele) + " -- Expected: false",
                NoOfItemBeforeDelete.equals(NoOfItemAferDele));
        softAssert.assertAll();
    }

    @Then("^every link on the Wishlist left side should take me to the correct page$")
    public void everyLinkOnTheWishlistLeftSideShouldTakeMeToTheCorrectPage(DataTable linksTable) {
        List<List<String>> rawTable = linksTable.asLists();
        List<String> actualTitleList = new ArrayList<String>();
        String actualTitle="";
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
                List<String> row = rawTable.get(i);
                String link = row.get(0);
                String url = row.get(1);
                String expectedPageTitle = row.get(2);

                wishlistPage.clickOnLeftSideMenuLinks(link);
                String actualUrl = commonFunctions.getCurrentURL();
                actualTitle = commonFunctions.retrieveText(pageTitleFromAccountsLeftMenu);
                if(actualTitle.toLowerCase().contains("order history")){
                    actualTitle="ORDER HISTORY";
                }

                actualTitleList.add(actualTitle);

                customVerification.verifyTrue("Failure. Every link from accounts left menu should take me to the correct page. Actual page URL is: "
                        + actualUrl + " -- Expected: " + url, actualUrl.contains(url));

                actualTitle = actualTitle.trim();
                customVerification.verifyTrue("Failure. Every link from accounts left menu should take me to the correct page. Actual page title: "+ actualTitle + " -- Expected: " + expectedPageTitle,
                            actualTitle.equalsIgnoreCase(expectedPageTitle.trim()));
            }
        }
        customVerification.verifyNoErrors();
    }

    @And("^clicking on the Wishlist 'CONTINUE SHOPPING' button should take me to the PreHome Page$")
    public void clickingOnTheWishlistContinueShoppingButtonShouldTakeMeToTheHomePage() {
        commonFunctions.clickElementByCSS(wishlistContinueShoppingBtn);
        Boolean isPreHomepage = preHomePage.checkIfIAmOnPreHomepage();

        softAssert.assertTrue(isPreHomepage,"Failure. Clicking on the Wishlist 'CONTINUE SHOPPING' button should take me to the Home Page. Actual result: "
                        + isPreHomepage + " -- Expected: true");
        softAssert.assertAll();
    }

    @Then("^the Wishlist page should have the correct column names '(.*)' '(.*)' '(.*)' '(.*)' '(.*)'$")
    public void thewishlistStepsShouldHaveTheCorrectColumnNames(String first, String second, String third,
                                                                String fourth, String fifth) {
        fifth = fifth.replace("DisponibilitE", "Disponibilit\u00E9");
        List<String> expectedColumnsList = new ArrayList<String>();
        expectedColumnsList.add(first);
        expectedColumnsList.add(second);
        expectedColumnsList.add(third);
        expectedColumnsList.add(fourth);
        expectedColumnsList.add(fifth);

        List<String> actualColumnsList = wishlistPage.getColumnNames();

        softAssert.assertTrue(actualColumnsList.equals(expectedColumnsList),"Failure. The Wishlist page should have the correct column names. Actual columns: "
                        + actualColumnsList + " -- Expected: " + expectedColumnsList);
        softAssert.assertAll();
    }


    @Then("^I add items to my wishlist$")
    public void iAddItemsToMyWishlist(DataTable itemsTable) {
        String currentURL = commonFunctions.getCurrentURL();
        List<List<String>> rawTable = itemsTable.asLists();
        String item = "";
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
                List<String> row = rawTable.get(i);

                if(productDetailsFromJson.getInStockProductSKUCode().isEmpty()){
                     item = row.get(0);
                }else
                    item = productDetailsFromJson.getInStockProductSKUCode().get(i-1);

                if (navigateUsingURL.getrelativeURLJenkins() == null) {
                    commonFunctions.navigateTo(Constants.BASE_URL + "/at/wishlist/add/" + item);
                } else {
                    String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
                    commonFunctions.navigateTo(relativeURLJenkins + "/at/wishlist/add/" + item);
                }
                commonFunctions.navigateTo(currentURL);
            }
        }
    }

    @When("^I click on 'Select/Clear All' link on the Wishlist page$")
    public void iClikcOnSelectOrClearAllLinkOnThewishlistSteps() {
        commonFunctions.clickElementByCSS(wishlistSelectAllCheckbox);
    }

    @Then("^the 'Select/Clear All' link text changes to '(.*)'$")
    public void theSelectOrClearAllLinkTextChanges(String linkText) {
        commonFunctions.waitForElementTextToChangeTo(wishlistSelectAllCheckbox,linkText);
    }

    @And("^all the visible products listed in my wishlist are selected$")
    public void allTheVisibleProductsListedInMyWishlistAreSelected() {
        Boolean isSelected = wishlistPage.areAllCheckboxesSelected();
        softAssert.assertTrue(isSelected,"Failure. All the visible products listed in my wishlist are selected. Actual result: "
                + isSelected + " -- Expected: true");
        softAssert.assertAll();
    }

    @And("^the 'manage selected' drop down list is enabled$")
    public void theManageSelectedDropDownListIsEnabled() {
        Boolean isEnabled = wishlistPage.isManageSelectedDropDownEnabled();
        softAssert.assertTrue(isEnabled,"Failure. The 'manage selected' drop down list is enabled. Actual result: " + isEnabled
                + " -- Expected: true");
        softAssert.assertAll();
    }

    @Then("^all the products listed in my wishlist are unselected$")
    public void allTheProductsListedInMyWishlistAreUnselected() {
        Boolean isSelected = wishlistPage.areAllCheckboxesSelected();
        softAssert.assertFalse(isSelected,"Failure. All the visible products listed in my wishlist are unselected. Actual result: "
                + isSelected + " -- Expected: true");
        softAssert.assertAll();
    }

    @Then("^the 'manage selected' drop down list is disabled$")
    public void theManageSelectedDropDownListIsDisabled() {
        Boolean isEnabled = wishlistPage.isManageSelectedDropDownEnabled();
        softAssert.assertFalse(isEnabled,"Failure. The 'manage selected' drop down list is disabled. Actual result: " + isEnabled
                + " -- Expected: true");
        softAssert.assertAll();
    }

    @And("^a 'DELETE ITEMS' overlay appears containing the message '(.*)'$")
    public void aDeleteItemsOverlayAppearsContainingMessage(String msg) {
        Boolean isDisplayed = wishlistPage.isDeleteOverlayDisplayed();
        softAssert.assertTrue(isDisplayed,"Failure. The overlay is not displayed.");
        wishlistPage.retrieveText(deleteOverlayMsgonWishlist).replace("\n","");
        String actualMsg = commonFunctions.retrieveText(deleteOverlayMsgonWishlist).replace("\n","");
        softAssert.assertTrue(actualMsg.equals(msg),"Failure. The overlay contains the message: " + actualMsg + " -- Expected: " + msg);
        softAssert.assertAll();
    }

    @And("^I click 'CANCEL' on the 'DELETE ITEMS' overlay$")
    public void iClickCancelOnTheDeleteItemsOverlay() {
        commonFunctions.clickElementByCSS(wishlistDeleteOverlayCancelBtn);
    }

    @And("^the 'DELETE ITEMS' overlay is closed$")
    public void theDeleteItemsOverlayIsClosed() {
        Boolean isDisplayed = wishlistPage.isDeleteOverlayDisplayed();
        softAssert.assertFalse(isDisplayed,"Failure. The delete overlay on wishlist is not closed.");
        softAssert.assertAll();
    }

    @And("^I click on the 'X' close icon in the right hand corner of the 'DELETE ITEMS' overlay$")
    public void iClickOnTheCloseIcon() {
        commonFunctions.clickElementByCSS(wishlistDeleteOverlayClose);
    }

    @And("^I click anywhere on the page outside of the 'DELETE ITEMS' overlay$")
    public void iClickAnywhereOutsideOfTheDeleteItemsOverlay() {
        wishlistPage.selectRadioButton(wishlistDeleteOverlayContainer);
    }

    @And("^the product has not been removed from my Wishlist$")
    public void theProductHasNotBeenRemovedFromMyWishlist() {
        List<String> itemsIDList = wishlistPage.getWishlistItemIdList();
        Assert.assertTrue("Failure. The product has not been removed from my Wishlist. Actual result: "
                + itemsIDList.contains(firstItemID) + " -- Expected: true", itemsIDList.contains(firstItemID));
    }

    @Then("^'(.*)' items are listed on the Wishlist page$")
    public void aNumberOfItemsAreListedOnThewishlistSteps(String number) {
        wishlistPage.checkNumberOfEntries(Integer.parseInt(number));
    }

    @When("^I click on the 'LOAD MORE...' button on the Wishlist page$")
    public void iClikcOnTheLoadMoreButtonOnThewishlistSteps() {
        commonFunctions.clickElementByCSS(wishlistLoadMoreBtn);
        wishlistPage.waitForChangesToBeApplied();
    }

    @And("^the last '(.*)' on the Wishlist page are not selected$")
    public void theLastNumberOfItemsOnThewishlistStepsAreNotSelected(String number) {
        int num = Integer.parseInt(number);
        Boolean areSelected = wishlistPage.areTheLastXCheckboxesSelected(num);
        Assert.assertFalse(
                "Failure. The last " + number + "are not selected. Actual result: false -- Expected: " + areSelected,
                areSelected);
    }


    @And("^clicking on the 'ADD TO BAG' button for the first product causes the minibag rollover to appear with '(.*)' text$")
    public void iClickOnAddToBagButton(String addToBagText) {
        CustomVerification verification = new CustomVerification();

        wishlistPage.clickOnFirstAddToBagButton(addToBagText);
        Boolean isDisplayed = miniCartPage.minicartIsDisplayed();
        verification.verifyTrue("Failure. The mini bag rollover list appears. Actual result: " + isDisplayed + " -- Expected: true",
                    isDisplayed);
        verification.verifyNoErrors();

    }

    @Then("^number of wishlist items must be more than '(.*)' after LOAD MORE$")
    public void must_be_more_than_after_LOAD_MORE(int wishlistItemsbeforeLoadMore) {
        List<WebElement> wishlistItemsAfterLoadMore = commonFunctions.getWebDriver().findElements(By.cssSelector(wishlistProductsCheckbox));
            boolean isMore = wishlistItemsAfterLoadMore.size() > wishlistItemsbeforeLoadMore;
            softAssert.assertTrue(isMore,"Wishlist LOAD MORE is not working");
    }
    @And("I should see placeholder text '(.*)' , '(.*)' and '(.*)' in the fields in the form")
    public void iShouldSeePlaceholderTextInRecepientForm(String name, String email, String msg) {

        String actualName = commonFunctions.getAttribute(wishlistRecepientNameByCSS,wishlistFormNameAttribute);
        String actualEmail = commonFunctions.getAttribute(wishlistRecepientEmailByCSS,wishlistFormNameAttribute);
        String actualMsg = commonFunctions.getAttribute(wishlistMessageByCSS,wishlistFormNameAttribute);
        String countryLabel = headerSettingsPage.grabCountryLabel();
        if((countryLabel!="日本")) {
            softAssert.assertEquals(actualName, name);
            softAssert.assertEquals(actualEmail, email);
            softAssert.assertEquals(actualMsg, msg);
        } else{
            softAssert.assertEquals(actualEmail, email);
            softAssert.assertEquals(actualMsg, msg);
        }
    }

    @And("^the Share Your Wishlist page should contain the text '(.*)'$")
    public void theShareYourWishlistStepsShouldContainTheCorrectFormTitle(String title) {

        String actualTitle = commonFunctions.getTextByCssSelector(wishlistFormTitleByCSS);

        softAssert.assertTrue(actualTitle.toLowerCase().equals(title.toLowerCase()),"Failure. The Share Your Wishlist page should contain the title: " + title + " -- Actual: "
                + actualTitle);
        softAssert.assertAll();
    }
    @Then("^the error message '(.*)' is displayed in red text$")
    public void theErrorMessageIsDisplayedInRedText(String errorMsg) {

       String wishlistErrorMsg= commonFunctions.getTextByCssSelector(wishlistErrorMessageByCSS);
        softAssert.assertTrue(wishlistErrorMsg.toLowerCase().equals(errorMsg.toLowerCase()),"Failure. The Share Your Wishlist page should contain the title: " + errorMsg + " -- Actual: "
                + wishlistErrorMsg);
        softAssert.assertAll();
    }
    @And("I click 'SEND' button on the Share Wishlist form")
    public void iClickOnSendButtonOnShareWishlist() {
        commonFunctions.clickElementByCSS(wishlisShareTopButtonByCSS);

    }
    @And("I should see error messages '(.*)' , '(.*)' and '(.*)' above the fields in the form")
    public void iShouldSeeErrorMessagesAboveFieldsInShareWishlistForm(String name, String email, String msg) {

//        String actualName = shareWishlistSteps.getRecepientNameError();
//        String actualEmail = shareWishlistSteps.getRecepientEmailError();
//        String actualMsg = shareWishlistSteps.getRecepientMessageError();
//
//        CustomVerification ver = new CustomVerification();
//        ver.verifyTrue("Failure. I should see name error: " + name + " --Actual: " + actualName,
//                actualName.equals(name));
//        ver.verifyTrue("Failure. I should see email error: " + email + " --Actual: " + actualEmail,
//                actualEmail.equals(email));
//        ver.verifyTrue("Failure. I should see message error: " + msg + " --Actual: " + actualMsg,
//                actualMsg.equals(msg));
    }
}
