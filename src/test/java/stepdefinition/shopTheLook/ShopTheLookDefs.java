package stepdefinition.shopTheLook;

import commonfunctions.CommonFunctions;
import commonfunctions.ProductDetailsFromJson;
import config.Constants;
import config.ObjPropertyReader;
import cucumber.api.java.en.Given;
import pages.ShopTheLookListPage;

public class ShopTheLookDefs {

    ShopTheLookListPage shopTheLookListPage = new ShopTheLookListPage();
    ProductDetailsFromJson productDetailsFromJson = new ProductDetailsFromJson();
    ObjPropertyReader objPropertyReader = new ObjPropertyReader();
    String shopTheLookSizeDD = objPropertyReader.readProperty("shopTheLookSizeDDByCSS");
    String product = "";
    String productSize = "";

    @Given("^I select size '(.*)' from the Shop The Look size drop down$")
    public void selectSizeFromTheShopTheLookSizeDropDown(String size)
    {
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            product = Constants.INSTOCK;
            productSize = size;
        }else{
            product = productDetailsFromJson.getInStockProductIdWithAvailableVarients().get(0);
            productSize = productDetailsFromJson.retrieveAvailableSizes().get(0);
        }

         //productSize= productSize.substring(0,2)+" "+productSize.substring(2);
        shopTheLookListPage.selectSizeOnShopTheLook(shopTheLookSizeDD.replace("productID",product), productSize);
    }

}
