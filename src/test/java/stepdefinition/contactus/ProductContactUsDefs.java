package stepdefinition.contactus;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import commonfunctions.ProductDetailsFromJson;
import config.GetInstockProductsDetail;
import org.junit.Assert;

import commonfunctions.CommonFunctions;
import commonfunctions.CustomVerification;
//import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.testng.asserts.SoftAssert;
import pages.ContactUsErrorModel;
import pages.ProductContactUsPage;
import utility.UrlUtils;
import utility.Validations;

public class ProductContactUsDefs {
	CommonFunctions commonFunctions = new CommonFunctions();
	ProductContactUsPage productContactUsPage = new ProductContactUsPage();
	UrlUtils urlUtils = new UrlUtils();
	config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
	GetInstockProductsDetail getInstockProductsDetail = new GetInstockProductsDetail();
	//String[] product;
	ProductDetailsFromJson productDetailsFromJson = new ProductDetailsFromJson();
	ArrayList<String> product;
	SoftAssert softAssertions = new SoftAssert();


	@When("I click on the Contact us link")
	public void clickOnContactUsLink() {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
		String productContactUsLinkByCSS=obj_Property.readProperty("productContactUsLinkByCSS");
		commonFunctions.clickElementByCSS(productContactUsLinkByCSS);

	}

	@And("the fields on the Contact us form should contain the correct placeholder text")
	public void contactUsPlaceholderText(DataTable dataTable) {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
		CustomVerification customVerification = new CustomVerification();
		List<List<String>> rawTable = dataTable.asLists();
		if (rawTable.size() >= 2)
			for (int i = 1; i < rawTable.size(); i++) {
				//logger.info("Row data: " + rawTable.get(i));
				String select = rawTable.get(i).get(0);
				String firstNamePlaceholderText = rawTable.get(i).get(1);
				String surnamePlaceholderText = rawTable.get(i).get(2);
				String emailAddressPlaceholderText = rawTable.get(i).get(3);
				String phoneNumberPlaceholderText = rawTable.get(i).get(4);
				String countryPlaceholderText = rawTable.get(i).get(5);
				String enquiryPlaceholderText = rawTable.get(i).get(6);
				String subjectPlaceholderText = rawTable.get(i).get(7);
				String messagePlaceholderText = rawTable.get(i).get(8);
				String sizeSelectorPlaceholderText = rawTable.get(i).get(9);
				String franceSelect = rawTable.get(i).get(10);
				String productContactMeRadioButtonByCSS=obj_Property.readProperty("productContactMeRadioButtonByCSS");
				if (countryPlaceholderText.contentEquals("France")) {
					commonFunctions.clickOnElementInList(productContactMeRadioButtonByCSS,franceSelect);
				} else {
					commonFunctions.clickOnElementInList(productContactMeRadioButtonByCSS,select);
				}
				List<String> expectedTableFormText = new ArrayList<String>();
				expectedTableFormText.add(firstNamePlaceholderText);
				expectedTableFormText.add(surnamePlaceholderText);
				expectedTableFormText.add(emailAddressPlaceholderText);
				expectedTableFormText.add(phoneNumberPlaceholderText);
				expectedTableFormText.add(countryPlaceholderText);
				expectedTableFormText.add(enquiryPlaceholderText);
				expectedTableFormText.add(subjectPlaceholderText);
				expectedTableFormText.add(messagePlaceholderText);
				expectedTableFormText.add(sizeSelectorPlaceholderText);

				List<String> actualTableFormText = productContactUsPage.grabContactUsTextLabelsForm();

				customVerification.verifyTrue(
						"Contact us form text labels were not the same with the ones in the table: " + "-Expected: "
								+ expectedTableFormText.toString() + " -Actual: " + actualTableFormText.toString(),
								expectedTableFormText.toString().equalsIgnoreCase(actualTableFormText.toString()));
			}
		customVerification.verifyNoErrors();
	}

	@Then("^clicking on CLOSE should close the PDP Contact us overlay$")
	public void clickOnCloseContactUsLink() {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
		String productCloseContactUsLink=obj_Property.readProperty("productCloseContactUsLink");
		commonFunctions.clickElementByCSS(productCloseContactUsLink);
	}

	@Then("I click on SUBMIT on the Contact us form")
	public void clickOnSubmitContactUsForm() {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
		String productCloseContactUsSubmitButton=obj_Property.readProperty("productCloseContactUsSubmitButton");
		commonFunctions.clickElementByCSS(productCloseContactUsSubmitButton);
	}

	@And("it should trigger the following messages")
	public void verifyErrorMessages(DataTable dataTable) {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
		CustomVerification customVerification = new CustomVerification();
		List<List<String>> rawTable = dataTable.asLists();
		if (rawTable.size() >= 2)
			for (int i = 1; i < rawTable.size(); i++) {
				//logger.info("Row data: " + rawTable.get(i));
				String emailAddressIncorrect = rawTable.get(i).get(0);
				String select = rawTable.get(i).get(1);
				String firstNameError = rawTable.get(i).get(2);
				String surNameError = rawTable.get(i).get(3);
				String emailAddressError = rawTable.get(i).get(4);
				String phoneNumberError = rawTable.get(i).get(5);
				String subjectError = rawTable.get(i).get(6);
				String messageError = rawTable.get(i).get(7);
				String sizeError = rawTable.get(i).get(8);
				String countryPlaceholderText = rawTable.get(i).get(9);
				String franceSelect = rawTable.get(i).get(10);
				String productContactMeRadioButtonByCSS=obj_Property.readProperty("productContactMeRadioButtonByCSS");
				String productContactUsemailInput=obj_Property.readProperty("productContactUsemailInput");
				String productCloseContactUsSubmitButton=obj_Property.readProperty("productCloseContactUsSubmitButton");
				if (countryPlaceholderText.contentEquals("France")) {
					commonFunctions.clickOnElementInList(productContactMeRadioButtonByCSS,franceSelect);
				} else {
					commonFunctions.clickOnElementInList(productContactMeRadioButtonByCSS,select);
				}
				if (!emailAddressIncorrect.isEmpty()) {
					commonFunctions.enterTextInputBox(productContactUsemailInput,emailAddressIncorrect);
				}
				commonFunctions.clickElementByCSS(productCloseContactUsSubmitButton);

				ContactUsErrorModel actualErrorFormText = productContactUsPage.grabContactUsErrorText();

				customVerification.verifyTrue("Contact us form FIRST NAME error not as expected. Expected: " + firstNameError + " -- Actual: " + actualErrorFormText.getFirstName(), actualErrorFormText.getFirstName().contains(firstNameError));
				customVerification.verifyTrue("Contact us form SURNAME error not as expected. Expected: " + surNameError + " -- Actual: " + actualErrorFormText.getSurName(), actualErrorFormText.getSurName().contains(surNameError));
				if (select.contains("email")) {
					customVerification.verifyTrue("Contact us form EMAIL error not as expected. Expected: " + emailAddressError + " -- Actual: " + actualErrorFormText.getEmail(), actualErrorFormText.getEmail().contains(emailAddressError));
				}
				if (select.contains("phone")) {
					customVerification.verifyTrue("Contact us form PHONE error not as expected. Expected: " + phoneNumberError + " -- Actual: " + actualErrorFormText.getPhone(), actualErrorFormText.getPhone().contains(phoneNumberError));
				}
				customVerification.verifyTrue("Contact us form SUBJECT error not as expected. Expected: " + subjectError + " -- Actual: " + actualErrorFormText.getSubject(), actualErrorFormText.getSubject().contains(subjectError));
				customVerification.verifyTrue("Contact us form MESSAGE error not as expected. Expected: " + messageError + " -- Actual: " + actualErrorFormText.getMessage(), actualErrorFormText.getMessage().contains(messageError));
				customVerification.verifyTrue("Contact us form SIZE error not as expected. Expected: " + sizeError + " -- Actual: " + actualErrorFormText.getSize(), actualErrorFormText.getSize().contains(sizeError));
			}
		customVerification.verifyNoErrors();
	}

	@And("opening and submitting the Contact us form with valid details should display a success message")
	public void verifySuccessMessageAfterSubmitContactUsForm(DataTable dataTable) throws IOException {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
		CustomVerification customVerification = new CustomVerification();
		List<List<String>> rawTable = dataTable.asLists();
		if (rawTable.size() >= 2)
			for (int i = 1; i < rawTable.size(); i++) {
				//logger.info("Row data: " + rawTable.get(i));
				String select = rawTable.get(i).get(0);
				String firstName = rawTable.get(i).get(1);
				String surName = rawTable.get(i).get(2);
				String emailAddress = rawTable.get(i).get(3);
				String phoneNumber = rawTable.get(i).get(4);
				String typeOfEnquiry = rawTable.get(i).get(5);
				String subject = rawTable.get(i).get(6);
				String message = rawTable.get(i).get(7);
				String size = rawTable.get(i).get(8);
				String expectedSuccessMessage = rawTable.get(i).get(9);
				String franceSuccessMessage = rawTable.get(i).get(10);
				String franceSelect = rawTable.get(i).get(11);
				String countryPlaceholderText = rawTable.get(i).get(12);
				String franceTypeOfEnquiry = rawTable.get(i).get(13);

				String productContactUsLinkByCSS=obj_Property.readProperty("productContactUsLinkByCSS");
				String productContactMeRadioButtonByCSS=obj_Property.readProperty("productContactMeRadioButtonByCSS");
				String productContactUsSurnameInput=obj_Property.readProperty("productContactUsSurnameInput");
				String productContactusFisrtNameInputByCSS=obj_Property.readProperty("productContactusFisrtNameInputByCSS");
				String productContactUsemailInput=obj_Property.readProperty("productContactUsemailInput");
				String productContactUsphoneInput=obj_Property.readProperty("productContactUsphoneInput");
				String productContactUsSubjectInput=obj_Property.readProperty("productContactUsSubjectInput");
				String productContactUsTypeOfEnquiry=obj_Property.readProperty("productContactUsTypeOfEnquiry");
				String productContactUsMessageInput=obj_Property.readProperty("productContactUsMessageInput");
				String productContactUsSizeSelect=obj_Property.readProperty("productContactUsSizeSelect");
				String productCloseContactUsSubmitButton=obj_Property.readProperty("productCloseContactUsSubmitButton");
				String productContactUsSuccessMsg=obj_Property.readProperty("productContactUsSuccessMsg");
				String actualSuccessMessage = commonFunctions.getTextByCssSelector(productContactUsSuccessMsg);
				String productCloseContactUsLink=obj_Property.readProperty("productCloseContactUsLink");

				commonFunctions.scrollScreen300Pixel();
				commonFunctions.clickWebElementByJSE(commonFunctions.getWebElement(productContactUsLinkByCSS));
				commonFunctions.waitForPageToLoad();
				if (countryPlaceholderText.contentEquals("France")) {
					commonFunctions.clickOnElementInList(productContactMeRadioButtonByCSS,franceSelect);

				} else {
					commonFunctions.clickOnElementInList(productContactMeRadioButtonByCSS,select);
				}
				commonFunctions.enterTextInputBox(productContactusFisrtNameInputByCSS,firstName);
				commonFunctions.enterTextInputBox(productContactUsSurnameInput,surName);
				commonFunctions.enterTextInputBox(productContactUsemailInput,emailAddress);
				commonFunctions.enterTextInputBox(productContactUsphoneInput,phoneNumber);
				if (countryPlaceholderText.contentEquals("France")) {
					commonFunctions.selectFromDropdownByCSS(productContactUsTypeOfEnquiry,franceTypeOfEnquiry);
				} else {
					commonFunctions.selectFromDropdownByCSS(productContactUsTypeOfEnquiry,typeOfEnquiry);
				}
				commonFunctions.enterTextInputBox(productContactUsSubjectInput,subject);

				commonFunctions.enterTextInputBox(productContactUsMessageInput,message);
				//if ("false" == getInstockProductsDetail.getProductIdAndSize() || (null == getInstockProductsDetail.getProductIdAndSize())){
				if(productDetailsFromJson.retrieveAvailableProductDetails().isEmpty()) {
					commonFunctions.selectFromDropdownByCSS(productContactUsSizeSelect,size);
				}else{
					//product=productDetailsFromJson.getInStockProductIdWithAvailableVarients();
					size = ProductDetailsFromJson.availableProductDetails.get("size").trim();
					commonFunctions.selectFromDropdownByCSS(productContactUsSizeSelect,size );
				}
				commonFunctions.clickElementByCSS(productCloseContactUsSubmitButton);
				//actualSuccessMessage = actualSuccessMessage.replaceAll("[\n]"+"\\s{2,}"," ").trim();
				actualSuccessMessage = actualSuccessMessage.replaceAll("[\n]"," ").trim();


				if (countryPlaceholderText.contentEquals("France")) {
			customVerification.verifyTrue("The Successful form submission message was not correctly displayed --Expected:" + franceSuccessMessage + " --Actual: " + actualSuccessMessage, franceSuccessMessage.toLowerCase().replaceAll(" ","").equalsIgnoreCase(actualSuccessMessage.toLowerCase().replaceAll(" ","")));
			commonFunctions.clickElementByCSS(productCloseContactUsLink);

		} else {
			customVerification.verifyTrue("The Successful form submission message was not correctly displayed --Expected:" + expectedSuccessMessage + " --Actual: " + actualSuccessMessage, expectedSuccessMessage.toLowerCase().replaceAll(" ","").equalsIgnoreCase(actualSuccessMessage.toLowerCase().replaceAll(" ","")));
			commonFunctions.clickElementByCSS(productCloseContactUsLink);
		}
	}
		customVerification.verifyNoErrors();
	}
	@Then("The product image on the Contact us overlay should be displayed")
	public void verifyProductImageOnContactUs() {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
		String productContactUsImageByCSS=obj_Property.readProperty("productContactUsImageByCSS");
		String contactUsImageStatus = commonFunctions.getSrcAttribute(productContactUsImageByCSS);
		String imageURL = "";
		if (imageURL.contentEquals(contactUsImageStatus)) {
			String imageStatus = "";
			try {
				imageStatus = urlUtils.getUrlStatusCode(imageURL);
			} catch (IOException e) {
			}
			Validations.verifyImagesStatusCodeInProductView(imageURL, imageStatus);
		}
	}

	@Then("The '(.*)' should be displayed on the Contact us overlay")
	public void verifyDesignerNameOnContactUs(String expectedContactUsDesignerName) {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + expectedContactUsDesignerName);
		String expectedDesignerName = "";
		String productContactUsDesignerNameByCSS=obj_Property.readProperty("productContactUsDesignerNameByCSS");
		String actualDesignerName = commonFunctions.getTextByCssSelector(productContactUsDesignerNameByCSS);

		if(!ProductDetailsFromJson.availableProductDetails.isEmpty()){
				expectedDesignerName = ProductDetailsFromJson.availableProductDetails.get("designerName");
		}else
			expectedDesignerName = expectedContactUsDesignerName;

		softAssertions.assertTrue(expectedDesignerName.contentEquals(actualDesignerName),
				"PDP Designer name was not the same with the one from table" + " Expected: " + expectedDesignerName + " Actual: " + actualDesignerName);
		softAssertions.assertAll();

	}
	@Then("Product Description '(.*)' should be displayed on the Contact us overlay")
	public void verifyProductDescriptionOnContactUs(String expectedProductDescription) {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + expectedProductDescription);
		String productContactUsProdDescriptionByCSS=obj_Property.readProperty("productContactUsProdDescriptionByCSS");
		String actualProductDescription = commonFunctions.getTextByCssSelector(productContactUsProdDescriptionByCSS);
		String expectedDescription = "";
		if(!ProductDetailsFromJson.availableProductDetails.isEmpty())
			expectedDescription = ProductDetailsFromJson.availableProductDetails.get("description");
		else
			expectedDescription = expectedProductDescription;

		softAssertions.assertTrue(expectedDescription.contentEquals(actualProductDescription),
				"PDP product description was not the same with the one from table" + " Expected: " + expectedDescription + " Actual: " + actualProductDescription);
		softAssertions.assertAll();
	}
	@Then("The price displayed on the Contact us overlay is '(.*)'")
	public void verifyProductPriceOnContactUs(String expectedContactUsPrice) {
		//logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + expectedContactUsPrice);
		String expectedPrice = "";
		String productContactUsProdPriceByCSS=obj_Property.readProperty("productContactUsProdPriceByCSS");
		String actualProductPrice = commonFunctions.getTextByCssSelector(productContactUsProdPriceByCSS);
		if(!ProductDetailsFromJson.availableProductDetails.isEmpty()){
			expectedPrice = ProductDetailsFromJson.availableProductDetails.get("price").trim();
		}else
			expectedPrice = expectedContactUsPrice.trim();

			softAssertions.assertTrue(actualProductPrice.trim().contains(expectedPrice),
					"Contact Us Price was not the same with table price" + " Expected: " + expectedPrice + " Actual: " + actualProductPrice);
			softAssertions.assertAll();
	}
	@Then("The text '(.*)' followed by '(.*)' should be displayed on the Contact us overlay")
	public void verifyProductCodeOnContactUs(String expectedCodeTextLabel, String expectedProductCode) {
		//	logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + expectedCodeTextLabel);
		String productContactUsProdCodeLableByCSS=obj_Property.readProperty("productContactUsProdCodeLableByCSS");
		String actualProductCodeTextLabel = commonFunctions.getTextByCssSelector(productContactUsProdCodeLableByCSS);

		if (expectedCodeTextLabel.contentEquals("Code produit:")) {
			softAssertions.assertTrue(expectedCodeTextLabel.contains(actualProductCodeTextLabel.replace(actualProductCodeTextLabel, "Code produit:")),
					"ContactUs 'Code produit' text was not the same with table code" + " Expected: " + expectedCodeTextLabel + " Actual: " + actualProductCodeTextLabel.replace(actualProductCodeTextLabel, "Code produit:"));
		} else if(expectedCodeTextLabel.contentEquals("상품 번호:")){
			softAssertions.assertTrue(expectedCodeTextLabel.contains(actualProductCodeTextLabel.replace(actualProductCodeTextLabel, "상품 번호:")),
					"ContactUs '상품 번호:' text was not the same with table code" + " Expected: " + expectedCodeTextLabel + " Actual: " + actualProductCodeTextLabel.replace(actualProductCodeTextLabel, "상품 번호:"));
		}else if(expectedCodeTextLabel.contentEquals("商品番号:")){
			softAssertions.assertTrue(expectedCodeTextLabel.contains(actualProductCodeTextLabel.replace(actualProductCodeTextLabel, "商品番号:")),
					"ContactUs '商品番号:' text was not the same with table code" + " Expected: " + expectedCodeTextLabel + " Actual: " + actualProductCodeTextLabel.replace(actualProductCodeTextLabel, "商品番号:"));
		}

		else {
			expectedCodeTextLabel = Normalizer.normalize(expectedCodeTextLabel.replaceAll(" ",""), Normalizer.Form.NFKC);
			actualProductCodeTextLabel = Normalizer.normalize(actualProductCodeTextLabel.replaceAll("[0-9]","").replaceAll(" ",""),Normalizer.Form.NFKC);
			softAssertions.assertTrue(expectedCodeTextLabel.contains(actualProductCodeTextLabel),"ContactUs 'Product Code' text was not the same with table code" + " Expected: " + expectedCodeTextLabel + " Actual: " + actualProductCodeTextLabel.replace(actualProductCodeTextLabel, "Product Code:"));
			String productContactUsProdCodeByCSS=obj_Property.readProperty("productContactUsProdCodeByCSS");
			String actualProductCode = commonFunctions.getTextByCssSelector(productContactUsProdCodeByCSS).replaceAll("[^0-9]", "");
			if(ProductDetailsFromJson.availableProductDetails.isEmpty()){
				softAssertions.assertTrue(expectedProductCode.contentEquals(actualProductCode),
						"Contact Us Code was not the same with table code" + " Expected: " + expectedProductCode + " Actual: " + actualProductCode);
			}else{
				softAssertions.assertTrue(ProductDetailsFromJson.availableProductDetails.get("productId").contentEquals(actualProductCode),
						"Contact Us Code was not the same with table code" + " Expected: " + expectedProductCode + " Actual: " + actualProductCode);
			}
		}
		softAssertions.assertAll();
	}

	@Then("Size '(.*)' should be preselected in the Contact us overlay size dropdown list")
	public void verifyProductSizeOnContactUs(String size) {
		//	logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + expectedSize);
		String expectedSize = "";
		String productContactUsGetTxtSizeSelect=obj_Property.readProperty("productContactUsGetTxtSizeSelect");
		String actualsize = commonFunctions.getTextByCssSelector(productContactUsGetTxtSizeSelect);
		if(ProductDetailsFromJson.availableProductDetails.isEmpty()){
			expectedSize = size;
		}else
			expectedSize = ProductDetailsFromJson.availableProductDetails.get("size");

		softAssertions.assertTrue(expectedSize.contentEquals(actualsize.replaceAll(" ","").replaceAll("\n|\t|\\s","")),
				"Contact Us preselected Size was not the same with table size" + " Expected: " + expectedSize + " Actual: " + actualsize);
		softAssertions.assertAll();
	}

	@Then("I should see phone number '(.*)' on the Contact us overlay")
	public void verifyPhoneNumberOnContactUs(String expectedPhoneNumber) {
		String PDPContactUsPhoneNumberByCSS=obj_Property.readProperty("PDPContactUsPhoneNumberByCSS");
		String actualPhoneNumber = commonFunctions.getTextByCssSelector(PDPContactUsPhoneNumberByCSS).replaceAll("[\n]"," ").replaceAll(("\\s+")," ");
		actualPhoneNumber =Normalizer.normalize(actualPhoneNumber,Normalizer.Form.NFKC);
		expectedPhoneNumber = Normalizer.normalize(expectedPhoneNumber,Normalizer.Form.NFKC);
		softAssertions.assertTrue(expectedPhoneNumber.trim().contains(actualPhoneNumber.trim()),
				"Contact Us Phone number was not the same with table phone number" + " Expected: " + expectedPhoneNumber + " Actual: " + actualPhoneNumber);
		softAssertions.assertAll();
	}

	@Then("I should see landline number '(.*)' on the Contact us overlay")
	public void verifyLandlineNumberOnContactUs(String expectedContactLandlineNumber) {
		String PDPContactUsLandLineNumberByCSS=obj_Property.readProperty("PDPContactUsLandLineNumberByCSS");
		String actualLandlineNumber = commonFunctions.getTextByCssSelector(PDPContactUsLandLineNumberByCSS).replaceAll("[\n]"," ").replaceAll(("\\s+")," ");
		softAssertions.assertTrue(expectedContactLandlineNumber.trim().contains(actualLandlineNumber.trim()),
				"Contact Us Price was not the same with table price" + " Expected: " + expectedContactLandlineNumber + " Actual: " + actualLandlineNumber);
		softAssertions.assertAll();
	}

	@Then("I should see Contact us email address '(.*)' option")
	public void verifyProductEmailAddressSupportOnContactUs(String expectedContactUsEmailAddress) {
		String PDPContactUSEmailAddressByCSS=obj_Property.readProperty("PDPContactUSEmailAddressByCSS");
		String actualEmailAddress = commonFunctions.getTextByCssSelector(PDPContactUSEmailAddressByCSS);
		softAssertions.assertTrue(expectedContactUsEmailAddress.trim().contains(actualEmailAddress.replaceAll("[\n]"," ").replaceAll(("\\s+")," ").trim()),
				"Contact Us Price was not the same with table price" + " Expected: " + expectedContactUsEmailAddress + " Actual: " + actualEmailAddress);
		softAssertions.assertAll();
	}
}
