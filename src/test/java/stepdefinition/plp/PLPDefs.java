package stepdefinition.plp;

import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import config.ObjPropertyReader;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

public class PLPDefs extends CommonFunctions {
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    SoftAssert softAssert = new SoftAssert();
    ObjPropertyReader obj_Property = new ObjPropertyReader();
    String plpBreadcrumbByCSS = obj_Property.readProperty("plpBreadcrumbByCSS");
    String plpPageTitleByCSS = obj_Property.readProperty("plpPageTitleByCSS");
    String plpDesignerNameforProduct = obj_Property.readProperty("plpDesignerNameforProductByCSS");
    String plpProductDetails = obj_Property.readProperty("plpProductDetailsByClassName");
    String plpProductPrice = obj_Property.readProperty("plpProductPriceByClassName");
    String plpNextArrow = obj_Property.readProperty("plpNextArrowByCSS");
    String plpPreviousArrow = obj_Property.readProperty("plpPreviousArrowByCSS");
    String plpFirstProduct = obj_Property.readProperty("plpFirstProductByCSS");
    String plpSizeGuideOnFirstProduct = obj_Property.readProperty("plpSizeGuideOnFirstProductByClassName");
    String plpImagesOfFirstProduct = obj_Property.readProperty("plpImagesOfFirstProductByCSS");
    String plpImageResults = obj_Property.readProperty("plpImageResultsByClassName");
    String plpBackToTopButton = obj_Property.readProperty("plpBackToTopButtonByClassName");

    @When("^I go to '(.*)' and '(.*)'$")
    public void goToBreadcrumb(String gender, String url) {
        navigateUsingURL.navigateToRelativeURL(gender+url);
    }

    @Then("^I should see breadcrumb links are clickable and navigating to the correct page '(.*)'$")
    public void verifyBreadcrumb(String url) {
        waitForPageToLoad();
        String breadcrumbText = retrieveText(plpBreadcrumbByCSS);
        String[] breadcrumbLinksList = breadcrumbText.replaceAll("[\n\t]", "").replaceAll("\\s+", " ").trim().split(" ");
        for (int i = breadcrumbLinksList.length - 1; i >= 0; i--) {
            waitForPageToLoad();
            waitForPageToLoad();
            WebElement el = getWebDriver().findElement(By.linkText(breadcrumbLinksList[i].replaceAll("[\n\t]", "").replaceAll("\\s+", "").trim()));
            clickWebElementByJSE(el);
            if (i == 0) {
                String plpPageURL = getWebDriver().getCurrentUrl();
                softAssert.assertEquals(!plpPageURL.contains(url), "Page URL is correct");
            }
            if (i > 0) {
                String plpPageTitle = getWebDriver().findElement(By.cssSelector(plpPageTitleByCSS)).getText();
                if (plpPageTitle.equalsIgnoreCase(breadcrumbLinksList[i].replaceAll("[\n\t]", "").trim())) {
                    softAssert.assertTrue(true
                            ,
                            "Breadcrumb page titile is " + " Expected: "
                                    + breadcrumbLinksList[i] + " Actual: " + plpPageTitle);
                }

            }
            softAssert.assertAll();
        }
    }

    @Then("I should see designer Name, product description and price for products on PLP")
    public void iShouldSeeDesignerNameProductDescriptionAndPriceForProductsOnPLP() {
        String designerName = "", prodDescription="",price="";
        designerName = retrieveText(plpDesignerNameforProduct);
        prodDescription = retrieveText(plpProductDetails);
        price = retrieveText(plpProductPrice);
        softAssert.assertTrue((!designerName.isEmpty() && !prodDescription.isEmpty() && !price.isEmpty()),"Failure...!!! DesignerName, " +
                "product details and price are empty");
        softAssert.assertAll();
    }

    @When("^I click on '(.*)' arrow on first product$")
    public void iClickOnNextArrowOnFirstProduct(String arrow){
        if(arrow.equalsIgnoreCase("Next"))
            clickOnElement(getWebElement(plpNextArrow));
        else
            clickOnElement(getWebElement(plpPreviousArrow));
    }

    @Then("^Image is scrolled on clicking '(.*)'$")
    public void imageIsScrolledOnClickingArrow(String arrow){
        boolean hasImageChanged = false;
        if(arrow.equalsIgnoreCase("Next")){
            hasImageChanged = getWebElement(plpPreviousArrow).getAttribute("aria-disabled").equals("false");
        }else{
            hasImageChanged = getWebElement(plpPreviousArrow).getAttribute("aria-disabled").equals("true");
        }

        softAssert.assertTrue(hasImageChanged,"Failure...!!! Image has not changed on clicking "+arrow+" on PLP Images");
        softAssert.assertAll();
    }

    @And("^I mousehover on image on PLP$")
    public void iMousehoverOnImageOnPLP(){
        fireEventByCSS(plpFirstProduct,"mouseover");
    }

    @Then("I see the size guide on product image")
    public void iSeeTheSizeGuidOnProductImage() {
        boolean isSizeGuideDisplayed = getWebElement(plpSizeGuideOnFirstProduct).getAttribute("style").contains("block");
        softAssert.assertTrue(isSizeGuideDisplayed,"Failure...!!! Cannot see size guide on product on PLP when mousehovered");
        softAssert.assertAll();
    }

    @Then("^I should not see 'Previous' slider on first image$")
    public void iShouldNotSeePreviousOnFirstImage(){
        boolean isPrevDisplayed = getWebElement(plpPreviousArrow).getAttribute("class").contains("swiper-button-disabled");
        softAssert.assertTrue(isPrevDisplayed,"Failure...!!! Can see Previous Slider on first image on a product on PLP");
        softAssert.assertAll();
    }

    @Then("^I should not see 'Next' slider on last image$")
    public void iShouldNotSeeNextSliderOnLastImage(){
        boolean isNextDisplayed = getWebElement(plpNextArrow).getAttribute("class").contains("swiper-button-disabled");
        softAssert.assertTrue(isNextDisplayed,"Failure...!!! Can see Next slider on last image on a product on PLP");
        softAssert.assertAll();
    }

    @And("^I scroll to the last image of first product$")
    public void iScrollTotheLastImageOnFirstProduct(){
        int numberOfImages = waitForListElementByCssLocator(plpImagesOfFirstProduct).size();
        for(int i = 0; i<numberOfImages-1; i++){
            clickOnElement(getWebElement(plpNextArrow));
        }
    }

    @Then("^I should see the number of results more than 0$")
    public void iShouldSeeResultsMorethanZero(){
        int numberOfResultsonPLP = Integer.parseInt(retrieveText(plpImageResults).split(" ")[0].replaceAll(",",""));
        softAssert.assertTrue(numberOfResultsonPLP>0 , "Problem...!!! NO Images are loaded as the results is 0");
        softAssert.assertAll();
    }

    @And("^I select '(.*)' in the left hand facet from category list$")
    public void iSelectCategoryinTheLeftHandFacetFromCategoryList(String category){
        waitForPageToLoad();
        clickUsingLinkText(category);
        waitForPageToLoad();
    }

    @Then("^I see the title of the page is the selected '(.*)'$")
    public void iSeeTheTitleOfThePageiSSelectedCategory(String category){
        waitForPageToLoad();
        String actualTitle = retrieveText(".plp__title");
        softAssert.assertTrue(actualTitle.equalsIgnoreCase(category),"Failure..!! The title of the PLP page is " +
                "not the selected category, Actual Category is: "+actualTitle+" Expected Category is: "+category);
        softAssert.assertAll();
    }

    @Then("^I should see '(.*)' in the breadcrumbs$")
    public void iShouldSeeSubcategoryInTheBreadcrumbs(String subcategory){
        String breadcrumbText = retrieveText(plpBreadcrumbByCSS).toLowerCase();
        softAssert.assertTrue(breadcrumbText.contains(subcategory.toLowerCase()),"Failure...!!!" +
                "Selected sub-category"+subcategory+" is not present in the breadcrumbs on PLP");
        softAssert.assertAll();
    }

    @When("I scroll down to a point on the page")
    public void iScrollDownToAPointOnThePage() {
        scrollScreen500Pixel();
        scrollScreen500Pixel();
    }

    @Then("I should see Back To Top button on right hand side of the page")
    public void iShouldSeeBackToTopbuttononrRightHandSideOfThePage() {
        WebElement backToTopButton = getWebElement(plpBackToTopButton);
        boolean bttIsDisplayed = backToTopButton.getLocation().x > 0 || backToTopButton.getLocation().y > 0;
        softAssert.assertTrue(bttIsDisplayed,"I cannot see Back To top button on right hand side of the PLP");
        softAssert.assertAll();
    }

    @Then("I click on the Back To top button")
    public void iClickOnTheBackToTopButton() {
        selectRadioButton(plpBackToTopButton);
        waitForPageToLoad();
    }

    @Then("I should go to the top of the page")
    public void iShouldGoToTheTopOfThePage(){
        boolean bttIsDisplayed = !getWebElement(plpBackToTopButton).isDisplayed();
        softAssert.assertTrue(bttIsDisplayed,"Page is not scrolled up when clicked on right side Back to top button on PLP");
        softAssert.assertAll();
    }

}
