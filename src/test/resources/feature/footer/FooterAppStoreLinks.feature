#Author: Himaja
@regression @footer @footerAppStoreLinks @teamContent
Feature: Navigation to App store from the footer
  In order to navigate to App stores from their current page
  A visitor to the website
  Should be able to scroll down to the bottom of the page to click on the icons or the links that redirect to iOS/Andriod app stores

  ############### SMOKE TEST, Please donot remove ########
  @smoke
  Scenario Outline: Validation of navigation to App stores from the footer

    Given I am on the website with '<country>' '<language>' ''
    Then clicking the links for App store should open and switch my view to a new tab containing the correct page url.
      | link                     | url_parameters                 | fr_link                         |
      | Download our iOS app     | /app/matchesfashion-com-modern | Téléchargez notre appli iOS     |
      | Download our Android app | ?id=com.matchesfashion.android | Téléchargez notre appli Android |
      | apple                    | /app/matchesfashion-com-modern | Téléchargez notre appli iOS     |
      | google                   | ?id=com.matchesfashion.android | Téléchargez notre appli Android |

    Examples:
      | country           | language |
      | United Kingdom    |          |
#      | Korea Republic of | 한국어      |
#      | France            | Français |
#      | Japan             | 日本語      |





  ###############################################

  @regression
  Scenario Outline: Validation of navigation to App stores from the footer

    Given I am on the website with '<country>' '<language>' ''
    Then clicking the links for App store should open and switch my view to a new tab containing the correct page url.
      | link                     | url_parameters                 | fr_link                         |
      | Download our iOS app     | /app/matchesfashion-com-modern | Téléchargez notre appli iOS     |
      | Download our Android app | ?id=com.matchesfashion.android | Téléchargez notre appli Android |
      | apple                    | /app/matchesfashion-com-modern | Téléchargez notre appli iOS     |
      | google                   | ?id=com.matchesfashion.android | Téléchargez notre appli Android |

    Examples:
      | country           | language |
      | United Kingdom    |          |
      | Korea Republic of | 한국어      |
      | France            | Français |
      | Japan             | 日本語      |
