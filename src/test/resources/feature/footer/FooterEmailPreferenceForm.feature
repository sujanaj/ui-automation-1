#Author: Himaja
@regression
Feature: Email preference form validation on the footer
  In order to allow existing customer or new customer to opt in for email updates
  A visitor to the website
  Should be able to scroll down to the bottom of the page to sign up or sign in for email updates


  ################################SMOKE TEST ##################
  @smoke @testrun
  Scenario Outline: Successful redirection to email registration page for a non-logged in existing customer on Email preferences sign up
 
    Given I am on the website with '<country>' '<language>' ''
    When I use the footer Email Preferences form to sign up for emails using '<email_address>'
    Then I should be redirected to Email registration page containing the pagetitle '<page_title>' and subheader '<sub_header>'

    Examples:
      | country           | language | email_address        | page_title                          | sub_header       |
      | United Kingdom    |          | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
#      | United States     |          | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
#      | Korea Republic of | English  | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
#      | Korea Republic of | 한국어      | mfautotest@gmail.com | 이메일 등록하기                            | 기존 회원            |
#      | France            | English  | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
#      | France            | Français | mfautotest@gmail.com | S’ABONNER AUX ACTUALITÉS PAR E-MAIL | DÉJÀ CLIENT      |
#      | Japan             | English  | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
#      | Japan             | 日本語      | mfautotest@gmail.com | ニュースレター配信登録                         | アカウントをお持ちのお客様    |




  #################################


  @regression
  Scenario Outline: (1) Validation of the placeholder text on the email address field of Email Preferences form

    Given I am on the website with '<country>' '<language>' '<currency>'
    And the email address field of Email Preferences form should contain the correct placeholder text '<placeholder_text>'
    Then clicking 'SIGN UP' on the Email Preferences form should trigger an error message
      | email_address | error_message   |
      | a.com         | <error_message> |

    Examples:
      | country           | language | currency | placeholder_text          | error_message               |
      | United Kingdom    |          |          | Sign up to our emails     | The Email field is invalid  |
      | United States     |          |          | Sign up to our emails     | The Email field is invalid  |
      | Korea Republic of | English  |          | Sign up to our emails     | The Email field is invalid  |
      | Korea Republic of | 한국어      |          | 이메일 등록하기                  | 이메일을 입력해 주세요.               |
      | France            | English  |          | Sign up to our emails     | The Email field is invalid  |
      | France            | Français |          | Offres et actualités mode | L'e-mail saisi est invalide |
      | Japan             | English  |          | Sign up to our emails     | The Email field is invalid  |
      | Japan             | 日本語      |          | メールアドレスを入力                | メールアドレスが正しくありません            |


  @regression
  Scenario Outline: (2) Successful redirection to email registration page for a non-logged in existing customer on Email preferences sign up

    Given I am on the website with '<country>' '<language>' ''
    When I use the footer Email Preferences form to sign up for emails using '<email_address>'
    Then I should be redirected to Email registration page containing the pagetitle '<page_title>' and subheader '<sub_header>'

    Examples:
      | country           | language | email_address        | page_title                          | sub_header       |
      | United Kingdom    |          | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
      | United States     |          | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
      | Korea Republic of | English  | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
      | Korea Republic of | 한국어      | mfautotest@gmail.com | 이메일 등록하기                            | 기존 회원            |
      | France            | English  | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
      | France            | Français | mfautotest@gmail.com | S’ABONNER AUX ACTUALITÉS PAR E-MAIL | DÉJÀ CLIENT      |
      | Japan             | English  | mfautotest@gmail.com | REGISTER FOR EMAIL UPDATES          | CURRENT CUSTOMER |
      | Japan             | 日本語      | mfautotest@gmail.com | ニュースレター配信登録                         | アカウントをお持ちのお客様    |

  @regression
  Scenario Outline: (3) Successful redirection to email registration page for a new customer on Email preference sign up

    When I am on the website with '<country>' '<language>' ''
    When I use the footer Email Preferences form to sign up for emails using '<email_address>'
    Then I should be redirected to Email registration page containing the pagetitle '<page_title>' and subheader '<sub_header>'

    Examples:
      | country           | language | email_address                  | page_title                          | sub_header          |
      | United Kingdom    |          | validemailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       |
      | United States     |          | validemailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       |
      | Korea Republic of | English  | validemailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       |
      | Korea Republic of | 한국어      | validemailaddress@fakemail.com | 이메일 등록하기                            | 정보 입력               |
      | France            | English  | validemailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       |
      | France            | Français | validemailaddress@fakemail.com | S’ABONNER AUX ACTUALITÉS PAR E-MAIL | SAISIR INFORMATIONS |
      | Japan             | English  | validemailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       |
      | Japan             | 日本語      | validemailaddress@fakemail.com | ニュースレター配信登録                         | 詳細を入力する             |

  @regression
  Scenario Outline: (4) Successful redirection to manage preferences page for logged in customer on Email preference sign up

    Given I am on the website with '<country>' '<language>' ''
    And I navigate to '/login'
    And I log in with the following credentials
    When I use the footer Email Preferences form to sign up for emails using '<email_address>'
    Then I should be redirected to Manage Preferences page containing the pagetitle '<page_title>'

    Examples:
      | country           | language | email_address        | page_title         |
      | United Kingdom    |          | mfautotest@gmail.com | MANAGE PREFERENCES |
      | Australia         |          | mfautotest@gmail.com | MANAGE PREFERENCES |
      | Korea Republic of | English  | mfautotest@gmail.com | MANAGE PREFERENCES |
      | Korea Republic of | 한국어      | mfautotest@gmail.com | 마케팅 수신 설정          |
      | France            | English  | mfautotest@gmail.com | MANAGE PREFERENCES |
      | France            | Français | mfautotest@gmail.com | MES PRÉFÉRENCES    |
      | Japan             | English  | mfautotest@gmail.com | MANAGE PREFERENCES |
      | Japan             | 日本語      | mfautotest@gmail.com | ニュースレター配信設定        |

  @regression
  Scenario Outline: (5) Validation of email preference link on the footer page for logged in customer

    Given I am on the website with '<country>' '<language>' ''
    And I navigate to '/login'
    And I log in with the following credentials
    When I click on the 'Email preferences' link
    Then I should be redirected to Manage Preferences page containing the pagetitle '<page_title>'

    Examples:
      | country           | language | page_title         |
      | United Kingdom    |          | MANAGE PREFERENCES |
      | United States     |          | MANAGE PREFERENCES |
      | Korea Republic of | English  | MANAGE PREFERENCES |
      | Korea Republic of | 한국어      | 마케팅 수신 설정          |
      | France            | English  | MANAGE PREFERENCES |
      | France            | Français | MES PRÉFÉRENCES    |
      | Japan             | English  | MANAGE PREFERENCES |
      | Japan             | 日本語      | ニュースレター配信設定        |

  @regression
  Scenario Outline: (6) Validation of email preference link on the footer page for nonlogged in customer

    Given I am on the website with '<country>' '<language>' ''
    When I click on the 'Email preferences' link
    Then I am redirected to the Sign In page

    Examples:
      | country           | language |
      | United Kingdom    |          |
      | United States     |          |
      | Korea Republic of |          |
      | Korea Republic of | 한국어      |
      | France            | Français |
      | France            |          |
      | Japan             |          |
      | Japan             | 日本語      |



  Scenario Outline: (7) Non-latin characters should not be allowed on email registration page for a new customer on Email preference sign up

    Given I am on the website with '<country>' '<language>' ''
    When I use the footer Email Preferences form to sign up for emails using '<email_address>'
    And I should be redirected to Email registration page containing the pagetitle '<page_title>' and subheader '<sub_header>'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<phone_number>' in the Phone Number field
    And I click Create An Account
    Then I should see the '<error_message>' validation Error Message

    Examples:
      | country           | language | email_address                           | page_title                          | sub_header          | title | first_name | last_name | password             | phone_number | error_message                             |
      | United Kingdom    |          | latinCharacteremailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       | Mr    | 테스트        | 테스트       | 242Eyvg8pJwpWcBxE7wz | 5522244788   | Please use Latin characters only.         |
      | France            | English  | latinCharacteremailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       | Mr    | 테스트        | 테스트       | 242Eyvg8pJwpWcBxE7wz | 5522244788   | Please use Latin characters only.         |
      | France            | Français | latinCharacteremailaddress@fakemail.com | S’ABONNER AUX ACTUALITÉS PAR E-MAIL | SAISIR INFORMATIONS | M.    | 테스트        | 테스트       | 242Eyvg8pJwpWcBxE7wz | 5522244788   | Merci de n'utiliser que l'alphabet latin. |
      | Korea Republic of | English  | latinCharacteremailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       | Mr    | 테스트        | 테스트       | 242Eyvg8pJwpWcBxE7wz | 5522244788   | Please use Latin characters only.         |
      | Korea Republic of | 한국어      | latinCharacteremailaddress@fakemail.com | 이메일 등록하기                            | 정보 입력               | Mr    | 테스트        | 테스트       | 242Eyvg8pJwpWcBxE7wz | 5522244788   | 영문만 입력 가능합니다.                             |
      | Japan             |          | latinCharacteremailaddress@fakemail.com | REGISTER FOR EMAIL UPDATES          | ENTER DETAILS       | Mr    | 테스트        | 테스트       | 242Eyvg8pJwpWcBxE7wz | 5522244788   | Please use Latin characters only.         |
      | Japan             | 日本語      | latinCharacteremailaddress@fakemail.com | ニュースレター配信登録                         | 詳細を入力する             | Mr    | 테스트        | 테스트       | 242Eyvg8pJwpWcBxE7wz | 5522244788   | 半角英数字で入力してください                            |

