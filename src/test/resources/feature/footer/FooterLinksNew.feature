#Author: Himaja D
@regression @footer @footerLinks @teamContent
Feature: Footer links validation on the website
  In order to provide more information and navigation options from the footer of their current page
  A visitor to the website
  Should be able to scroll down to the bottom of the page to access various links and navigate within the site

  @smoke
  Scenario Outline: 1. Smoke - Validation of Footer links under matchesfashion.com
    Given I am on the website with '<country>' '<language>' ''
    Then footer links should take me to the right page '<link>'
      | <page_title1> |
      | <page_title2> |
      | <page_title3> |
      | <page_title4> |

    Examples:
      | country           | language | link |page_title1       | page_title2           | page_title3 | page_title4      |
      | United Kingdom    | English  | MATCHESFASHION |ABOUT US          | SUSTAINABILITY        | CAREERS     | ABOUT AFFILIATES |

  @smoke
  Scenario Outline: 2. Smoke -  Validation of footer links under Help
    Given I am on the website with '<country>' '<language>' '<currency>'
    Then footer links should take me to the right page '<link>'
      | <page_title1> |
      | <page_title2> |
      | <page_title3> |
      | <page_title4> |

    Examples:
      | country           | language | currency|link  | page_title1    | page_title2                | page_title3 | page_title4 |
      | United Kingdom    | English  | |Help  | CONTACT US     | FREQUENTLY ASKED QUESTIONS | DELIVERY    | RETURNS     |

    @smoke
  Scenario Outline: 3. Smoke - Validation Services on footer
    Given I am on the website with '<country>' '<language>' '<currency>'
    Then footer links should take me to the right page '<link>'
      | <page_title1> |
      | <page_title2> |
      | <page_title3> |

    Examples:
      | country           | language | currency |link     | page_title1 | page_title2      | page_title3   |
      | United Kingdom    | English  | |Services | MYSTYLIST   | PRIVATE SHOPPING | YOUR REWARDS  |

  @smoke
  Scenario Outline: 4. Smoke - Validation of Giftcard links in footer

  # Gift card landing page need data set up, currently there is a 503 error page

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then clicking the gift cards links should take me to the right page with sections '<section_1>' and '<section_2>'

    Examples:
      | country           | language | currency|section_1          | section_2                  |
      | United Kingdom    |          | |SHOP GIFT CARDS | ACTIVATE YOUR GIFT CARD |

  Scenario Outline: 5. Smoke - Validation of Stores details on footer

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then footer links should take me to the right page '<link>'
      | <store_1>        |
      | <store_2>        |
      | <store_3>        |
      | <carlosplaceURL> |

    Examples:
      | country           | language | currency |link           | store_1    | store_2      | store_3   | carlosplaceURL |
      | United Kingdom    |          | |Visit Us       | MARYLEBONE | NOTTING HILL | WIMBLEDON | 5carlosplace   |


  ##############################################


  @regression
  Scenario Outline: 1. Validation of Footer links under matchesfashion.com

# Please note on the test environment, the links  for 'Careers' and 'FAQS' redirect to 503 service not available page due to data not available. Please refer the live site while implementing for Careers and FAQs.

    Given I am on the website with '<country>' '<language>' ''
    Then footer links should take me to the right page '<link>'
      | <page_title1> |
      | <page_title2> |
      | <page_title3> |
      | <page_title4> |


    Examples:
      | country           | language | link |page_title1       | page_title2           | page_title3 | page_title4      |
      | United Kingdom    | English  | MATCHESFASHION |ABOUT US          | SUSTAINABILITY        | CAREERS     | ABOUT AFFILIATES |
      | France            | Français | MATCHESFASHION.COM | QUI SOMMES-NOUS ? | DÉVELOPPEMENT DURABLE | CAREERS | NOS AFFILIÉS        |
      | Korea Republic of | 한국어      | MATCHESFASHION.COM | 브랜드 소개         | 지속가능성                 | CAREERS     | 제휴 프로그램         |
      | Japan             | 日本語      |   MATCHESFASHION.COM      |会社案内              | SUSTAINABILITY        | CAREERS     | アフィリエイト          |


  @regression
  Scenario Outline: 2. Validation of footer links under Help
    Given I am on the website with '<country>' '<language>' '<currency>'
    Then footer links should take me to the right page '<link>'
      | <page_title1> |
      | <page_title2> |
      | <page_title3> |
      | <page_title4> |

    Examples:
      | country           | language | currency|link  | page_title1    | page_title2                | page_title3 | page_title4 |
      | United Kingdom    | English  | |Help  | CONTACT US     | FREQUENTLY ASKED QUESTIONS | DELIVERY    | RETURNS     |
      | France    | English  | Help  | |CONTACT US     | FREQUENTLY ASKED QUESTIONS | DELIVERY    | RETURNS     |
      | France            | Français | |Aide  | CONTACTEZ-NOUS | FREQUENTLY ASKED QUESTIONS | LIVRAISON   | RETOURS     |
      |  Korea Republic of   | English  || Help  | CONTACT US     | FREQUENTLY ASKED QUESTIONS | DELIVERY    | RETURNS     |
      | Korea Republic of | 한국어      | |고객 지원 | 문의하기           | 자주 묻는 질문                   | 배송 정보       | 반품 정보       |
      |  Japan   | English  || Help  | CONTACT US     | FREQUENTLY ASKED QUESTIONS | DELIVERY    | RETURNS     |
      | Japan             | 日本語      | |ヘルプ   | お問い合わせ         | よくあるご質問                    | 配送について      | 返品について      |

  @regression
  Scenario Outline: 3. Validation Services on footer
    Given I am on the website with '<country>' '<language>' '<currency>'
    Then footer links should take me to the right page '<link>'
      | <page_title1> |
      | <page_title2> |
      | <page_title3> |

    Examples:
      | country           | language | currency |link     | page_title1 | page_title2      | page_title3   |
      | United Kingdom    | English  | |Services | MYSTYLIST   | PRIVATE SHOPPING | YOUR REWARDS  |
      | France            | Français | |Services | MYSTYLIST   | SHOPPING PRIVÉ   | VOS AVANTAGES |
      | Korea Republic of | 한국어      | |프리미엄 서비스 | 마이스타일리스트    | 프라이빗 쇼핑          | 리워드 소개        |
      | Japan             | 日本語      | |サービス     | MYSTYLIST   | プライベートショッピング     | お客様の特典        |

  @regression
  Scenario Outline: 4. Validation of Giftcard links in footer

  # Gift card landing page need data set up, currently there is a 503 error page

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then clicking the gift cards links should take me to the right page with sections '<section_1>' and '<section_2>'

    Examples:
      | country           | language | currency|section_1          | section_2                  |
      | United Kingdom    |          | |SHOP GIFT CARDS | ACTIVATE YOUR GIFT CARD |
      | France            | Français | |COMMANDEZ VOS CARTES CADEAUX     | ACTIVEZ VOTRE CARTE CADEAU|
      | Korea Republic of | 한국어      | |기프트 카드 구매하기 | 기프트 카드 활성화하기 |
      | Japan    | 日本語      | |ギフトカードの購入       | ギフトカードの有効化         |

  @regression
  Scenario Outline: 5. Validation of Stores details on footer

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then footer links should take me to the right page '<link>'
      | <store_1>        |
      | <store_2>        |
      | <store_3>        |
      | <carlosplaceURL> |

    Examples:
      | country           | language | currency |link           | store_1    | store_2      | store_3   | carlosplaceURL |
      | United Kingdom    |          | |Visit Us       | MARYLEBONE | NOTTING HILL | WIMBLEDON | 5carlosplace   |
      | Korea Republic of | 한국어      || 스토어            | 말리본        | 노팅힐          | 윔블던       | 5carlosplace   |
      | France            | Français | |Retrouvez-nous | MARYLEBONE | NOTTING HILL | WIMBLEDON | 5carlosplace   |
      | Japan             | 日本語      || 店舗情報           | MARYLEBONE | NOTTING HILL | WIMBLEDON | 5carlosplace   |
