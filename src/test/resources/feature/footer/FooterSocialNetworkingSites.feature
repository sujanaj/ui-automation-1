#Author: Arun Krishnan
@regression @footer @footerSocialNetworkingSites @teamContent
Feature: Social networking sites validation on the footer
  In order to navigate to social networking sites from the their current page
  A visitor to the website
  Should be able to scroll down to the bottom of the page to access various icons to navigate to the social networking sites

  ##### SMOKE Test - Please donot remove ############
  @smoke
  Scenario Outline: Validation of social networking sites on the footer of the page

    Given I am on the website
    And I set my country as '<country>'
    Then on clicking on the icons for 'social networking sites' should open and switch my view to a new tab containing the correct page url.
      | icon        | url                             |
      | facebook    | facebook.com/MATCHESFASHION     |
      | twitter     | twitter.com/matchesfashion      |
      | pinterest   | pinterest.co.uk/matchesfashion  |
      | instagram   | instagram.com/matchesfashion    |
      | youtube     | youtube.com/user/MATCHESFASHION |

    Examples:
      | country        |
      | United Kingdom |
#      | Korea Republic of|
#      | France         |
#      | Japan          |



  ######################



  @done @detailed
  Scenario Outline: Validation of social networking sites on the footer of the page

    Given I am on the website
    And I set my country as '<country>'
    Then on clicking on the icons for 'social networking sites' should open and switch my view to a new tab containing the correct page url.
      | icon        | url                             |
      | facebook    | facebook.com/MATCHESFASHION     |
      | twitter     | twitter.com/matchesfashion      |
      | pinterest   | pinterest.co.uk/matchesfashion  |
      | instagram   | instagram.com/matchesfashion    |
      | youtube     | youtube.com/user/MATCHESFASHION |

    Examples:
      | country        |
      | United Kingdom |
      | Korea Republic of|
      | France         |
      | Japan          |

