#Author: Dilbag
@regression @order @placeOrder @teamSecure
Feature: Place order
  In order to generate revenue
  The users of the website and impersonation
  Should be able to place orders

#	The following scenarios are intended to cover all the variables associated with placing an order, which may have some outcome on the order itself.
#	In some cases, the variables have been captured in the tests via data tables, and via examples tables as part of scenario outlines. In other cases,
#	due to the changing behaviour of the website, it has been necessary to create a different scenario entirely to cover the variables. An example of
#	this is the shipping address, which can be selected from a saved list, or entered as a new address by the user. As entering a new address requires
#	that the user fills out a form, whereas no form is required to use a sved address, it was necessary to capture this variable in separate scenarios.
#
#	The following is a table of the scenarios captured below and the variables they cover. It may be easier to refer to this table than to read through each
#	scenario individually.
#
#	   | scenario | site    | order_lines | customer_type | vouchers | promotions | ship_address | card_status | payment_method | bill_address | no_orders |
#      | 1        | website | single-line | existing      | no       | no         | saved        | saved       | card           | saved        | 8         |
#      | 2        | website | multi-line  | existing      | no       | no         | saved        | new         | card           | saved        | 4         |
#      | 3        | website | single-line | existing      | no       | no         | saved        | new         | card           | new          | 4         |
#      | 4        | website | single-line | existing      | no       | no         | saved        | N/A         | credit         | N/A          | 4         |
#      | 5        | website | single-line | existing      | no       | no         | saved        | saved       | part-credit    | saved        | 4         |
#      | 6        | website | single-line | existing      | no       | no         | new          | saved       | card           | saved        | 4         |
#      | 7        | website | single-line | guest         | no       | no         | new          | new         | card           | new          | 4         |
#      | 8        | website | multi-line  | staff         | no       | yes        | saved        | saved       | card           | saved        | 1         |
#      | 9        | website | multi-line  | existing      | yes      | yes        | saved        | saved       | part-credit    | saved        | 6         |
#      | 10	 	  | website | multi-line  | new			  | no       | no         | new          | new         | card           | new          | 5         |
#
#   When running tests locally, un-commenting the "I sign out" step in applicable scenarios is recommended. This is so that the session doesn't carry over
#   (something that only happens locally)

  @smoke
  Scenario Outline: (3) Website, single-line, existing customer, saved shipping address, saved credit card, saved billing address.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1      | address_line_2      | town_city          | postcode_zip | pccc          |
      | DO NOT DELETE LINE1 | DO NOT DELETE LINE2 | DO NOT DELETE Town | 12345        | P111111111111 |
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
   # And I select the saved '<card_type>' credit card
   # And I select the '<billing_address>' billing address
    And I place the order
    Then I should see the order confirmation page

    Examples:
      | country           | language | currency          | card_type        | shipping_method   |card_number      | card_name  | expiry_month | expiry_year | security_code |
#      | United Kingdom    |          | British Pound (£) | Visa             | PREMIUM           |
#       | United Kingdom    |          | British Pound (£) | American Express | NEXT BUSINESS DAY |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
      | United Kingdom    |          | British Pound (£) | Visa       | EXPRESS           |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
      | France            | Français | Euro (€)          | Visa             | EXPRESS |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |


  @smoke
  Scenario Outline: (1) Delete an address on my address book page.
    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    When I delete the following address in my Address Book
      | address_line_1      | town_city           | county_state | postcode_zip |
      | DO NOT DELETE LINE1 |  DO NOT DELETE Town |              | UB6 8BB        |


    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |

  @smoke
  Scenario Outline: (2) Create a new address on my address book page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    And I click on 'ADD A NEW ADDRESS'
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1      | address_line_2      | town_city          | postcode_zip | pccc          |
      | DO NOT DELETE LINE1 | DO NOT DELETE LINE2 | DO NOT DELETE Town | UB6 8BB      | P111111111111 |
    Then the 'ADD ADDRESS' form on Address Book page disappears
    And the first line of the first address in my Address Book contains '<address_line_1>'

    Examples:
      | country           | language | currency | address_line_1 |
      | United Kingdom    |          |          | DO NOT DELETE LINE1|

  @smoke
  Scenario Outline: (3) Website, single-line, existing customer, saved shipping address, saved credit card, saved billing address.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1      | address_line_2      | town_city          | postcode_zip | pccc          |
      | DO NOT DELETE LINE1 | DO NOT DELETE LINE2 | DO NOT DELETE Town | 12345        | P111111111111 |
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
   # And I select the saved '<card_type>' credit card
   # And I select the '<billing_address>' billing address
    And I place the order
    Then I should see the order confirmation page

    Examples:
      | country           | language | currency          | card_type        | shipping_method   |card_number      | card_name  | expiry_month | expiry_year | security_code |
#      | United Kingdom    |          | British Pound (£) | Visa             | PREMIUM           |
#       | United Kingdom    |          | British Pound (£) | American Express | NEXT BUSINESS DAY |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
       | United Kingdom    |          | British Pound (£) | Visa       | EXPRESS           |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
#      | United States     |          | US Dollar ($)     | Visa             | PREMIUM           |
#      | United States     |          | US Dollar ($)     | American Express | NEXT BUSINESS DAY |
#      | United States     |          | US Dollar ($)     | Mastercard       | EXPRESS           |
      | France            | Français | Euro (€)          | Visa             | EXPRESS |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
#      | France            | Français | Euro (€)          | American Express | NEXT BUSINESS DAY |
#      | France            | Français | Euro (€)          | Mastercard       | EXPRESS           |
#      | France            | English  | Euro (€)          | Visa             | NEXT BUSINESS DAY |
#      | France            | English  | Euro (€)          | American Express | EXPRESS           |
#      | France            | English  | Euro (€)          | Mastercard       | EXPRESS           |
       | Korea Republic of | 한국어      | Euro (€)          | Visa             | EXPRESS           |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
#       | Korea Republic of | 한국어      | Euro (€)          | Mastercard       | EXPRESS           |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
#       | Korea Republic of | 한국어      | Euro (€)          | American Express | EXPRESS           |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
#      | Korea Republic of | English  | Euro (€)          | Visa             | EXPRESS           |
#      | Korea Republic of | English  | Euro (€)          | American Express | EXPRESS           |
#      | Korea Republic of | English  | Euro (€)          | Mastercard       | EXPRESS           |
      | Japan             | 日本語      | Japanese Yen (¥)  | Visa             | EXPRESS           |4444333322221111 | ORDER TEST | 01           | 2026        | 123           |
#      | Japan             | 日本語      | Japanese Yen (¥)  | Mastercard       | EXPRESS           |
#      | Japan             | 日本語      | Japanese Yen (¥)  | American Express | EXPRESS           |
#      | Japan             | English  | Japanese Yen (¥)  | Visa             | EXPRESS           |
#      | Japan             | English  | Japanese Yen (¥)  | Mastercard       | EXPRESS           |
#      | Japan             | English  | Japanese Yen (¥)  | American Express | EXPRESS           |


  @smoke
  Scenario Outline: (4) Delete an address on my address book page.
    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    When I delete the following address in my Address Book
      | address_line_1      | town_city           | county_state | postcode_zip |
      | DO NOT DELETE LINE1 |  DO NOT DELETE Town |              | 12345        |


    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |


  @smoke
  Scenario Outline: (5) Deleting a payment card causes it to be removed from the Payment Cards page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Payment Cards page
    When I click Delete under the payment card where the Card Owner is '<card_owner>'
    And I click Delete in the overlay confirmation window
    Then the payment card where the Card Owner is '<card_owner>' no longer exists on the page

    Examples:

      | country           | language | currency | card_owner              |
      | United Kingdom    |          |          | ORDER TEST  |
#      | France            | Français |          | DELETE PAYMENT CARD FRA |
#      | Korea Republic of | 한국어     |          | DELETE PAYMENT CARD KOR |
#      | Japan             | 日本語    |          | DELETE PAYMENT CARD JPN |

 ##place order using guest user
  @smoke
  Scenario Outline: (5) Place an order using Guest User

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    And I check out
    When I login as a guest user '<first_name>'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<phone_number>' in the Phone Number field
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    Then I should see the order confirmation page


    Examples:
      | country           | language | currency | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | title | first_name | last_name | phone_number | address_saved |
      | United Kingdom    |          | EUR €    | Visa             | 4000000000001000 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
      | France            | Français | EUR €    | Mastercard       | 5200000000001005 | ORDER TEST | 01           | 2022        | 123           | M.    | GuestUser  | Test      | 0765432198   | ADRESSE ENREGISTRÉE |
      | Korea Republic of | 한국어      | USD $   |Visa | 4000000000001000  | ORDER TEST | 01           | 2022        | 123          | Mr    | GuestUser  | Test      | 0765432198   | 주소 저장 완료 |


  Scenario Outline:	Website, single-line, existing customer, saved shipping address, new credit card, new billing address.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
    | address_line_1      | address_line_2      | town_city          | postcode_zip | pccc          |
    | DO NOT DELETE LINE1 | DO NOT DELETE LINE2 | DO NOT DELETE Town | UB68B      | P111111111111 |
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I select the 'Enter A New Billing Address' radio button
  #  And I select the Billing Address 'Enter Full Address Manually' link if it is present
    And I select '<country>' from the Billing Address Country Dropdown List
    And I enter '<address_line_1>' in the Billing Address Line 1 field
    And I enter '<address_line_2>' in the Billing Address Line 2 field
    And I enter '<town_city>' in the Billing Address Town/City field
    And I enter '<postcode_zip>' in the Billing Address Postcode/Zipcode field
    And Uni-Pass field should not be visible to customer
    And I place the order
    Then I should see the order confirmation page


    Examples:
      | country         | language|currency | email             | password | shipping_method | packaging_option | card_type  | card_number      | card_name  | expiry_month | expiry_year | security_code | address_line_1 | address_line_2      | town_city | county_state | postcode_zip | TestId |
      | Korea Republic of  |         |         | tse_test@test.com | PasSw0rd | EXPRESS         | ECO PACKAGING    | Visa       | 4111111111111111 | DELETE PAYMENT CARD UK | 12           | 19          | 123           | DO NOT DELETE LINE1      | 26 Melbourne St     | Sydney    |              | 12312       | S03E03 |
     # | France          |         |         | tse_test@test.com | PasSw0rd | EXPRESS         | ECO PACKAGING    | Mastercard | 5555555555554444 | ORDER TEST | 12           | 19          | 123           | Black Dog      | 26 Rue des Lombards | Paris     |              | 75004        | S03E04 |


  Scenario Outline: (3) Website, single-line, existing customer, saved shipping address, credit payment.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    And I select the 'Pay With Credit' radio button
    And I place the order
    Then I should see the order confirmation page


    Examples:
      | country        |language| currency | email             | password | shipping_method | packaging_option  | TestId |
      | United Kingdom |        |GBP      | tse_test@test.com | PasSw0rd | EXPRESS         | MARBLED PACKAGING | S04E01 |
#      | France         |        | EUR      | tse_test@test.com | PasSw0rd | EXPRESS         | ECO PACKAGING     | S04E04 |


  Scenario Outline: (11) Delete an address on my address book page.
    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    When I delete the following address in my Address Book
      | address_line_1      | town_city           | county_state | postcode_zip |
      | DO NOT DELETE LINE1 |  DO NOT DELETE Town |              | 12345        |


    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |



  Scenario Outline: Deleting a payment card causes it to be removed from the Payment Cards page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Payment Cards page
    When I click Delete under the payment card where the Card Owner is '<card_owner>'
    And I click Delete in the overlay confirmation window
    Then the payment card where the Card Owner is '<card_owner>' no longer exists on the page

    Examples:

      | country           | language | currency | card_owner              |
      | United Kingdom    |          |          | DELETE PAYMENT CARD UK  |
#      | France            | Français |          | DELETE PAYMENT CARD FRA |
#      | Korea Republic of | 한국어     |          | DELETE PAYMENT CARD KOR |
#      | Japan             | 日本語    |          | DELETE PAYMENT CARD JPN |