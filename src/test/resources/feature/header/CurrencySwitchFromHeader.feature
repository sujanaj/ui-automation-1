#Author @Himaja
@regression @header @currencySwitch @teamContent
Feature: Currency switch from the Pre Homepage
  In order to browse the site and pay in different currencies
  A customer using the website
  Should be able to select their preferred currency

 ########### SMOKE Test- Please donot remove #############
 # @smoke
    Scenario Outline: UK and US sites should allow GBP, USD and EUR to be selected

      Given I am on the website with '<country>' set as delivery country
      Then I should only be able to switch to the following currencies using the Homepage Currency Dropdown List
        | currency |
        | GBP      |
        | USD      |
        | EUR      |

      Examples:
        | country           |
        | United Kingdom    |
  #      | United States     |
  #      | France            |
  #      | Korea Republic of |

    @smoke
    Scenario Outline: JP site should allow GBP, USD, EUR and JPY to be selected

      Given I am on the website with '<country>' '<language>' '<currency>'
      Then I should only be able to switch to the following currencies using the Homepage Currency Dropdown List
        | currency |
        | JPY      |
        | GBP      |
        | USD      |
        | EUR      |
      Examples:
        | country   |language|currency|
        | Japan     |        |        |


     @smoke
    Scenario Outline: Switching currency from header causes currency to be updated on PDPs  GBP, USD, EUR, HKD

      Given I am on the website with '<country>' '<language>' '<currency>'
      And I go to 'instock' product page
      Then I should see selected currency as '<pdp_price_symbol>' on PDP page

      Examples:
        | country        | language | currency             | pdp_price_symbol |
        | United Kingdom |          | British Pound (£)    |  £                |
  #      | United States  |          | US Dollar ($)        |  $                |
  #      | France         |          | Euro (€)             |  €                |
  #      | Hong Kong      |          | Hong Kong Dollar ($) | $                |
  #      | Japan          |          | Japanese Yen (¥)     |  ¥                |

    ############################

  @regression
  Scenario Outline: UK and US sites should allow GBP, USD and EUR to be selected

    Given I am on the website with '<country>' set as delivery country
    Then I should only be able to switch to the following currencies using the Homepage Currency Dropdown List
      | currency |
      | GBP      |
      | USD      |
      | EUR      |

    Examples:
      | country           |
      | United Kingdom    |
      | United States     |
      | France            |
      | Korea Republic of |

  @regression
  Scenario Outline: JP site should allow GBP, USD, EUR and JPY to be selected

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then I should only be able to switch to the following currencies using the Homepage Currency Dropdown List
      | currency |
      | JPY      |
      | GBP      |
      | USD      |
      | EUR      |
  Examples:
  | country   |language|currency|
  | Japan     |        |        |
  @regression
  Scenario Outline: International site should allow GBP, USD, EUR and HKD to be selected

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then I should only be able to switch to the following currencies using the Homepage Currency Dropdown List
      | currency |
      | GBP      |
      | USD      |
      | EUR      |
      | HKD      |

    Examples:
      | country   |language|currency|
      | Hong Kong |        |        |

  @regression
  Scenario Outline: AU site should allow GBP, USD, EUR and AUD to be selected

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then I should only be able to switch to the following currencies using the Homepage Currency Dropdown List
      | currency |
      | GBP      |
      | USD      |
      | EUR      |
      | AUD      |
  Examples:
      | country   |language|currency|
      | Australia|        |        |


  @regression
  Scenario Outline: Switching currency from header causes currency to be updated on PDPs - GBP, USD, EUR, HKD

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    Then I should see selected currency as '<pdp_price_symbol>' on PDP page

    Examples:
      | country        | language | currency             | pdp_price_symbol |
      | United Kingdom |          | British Pound (£)    |  £                |
      | United States  |          | US Dollar ($)        |  $                |
      | France         |          | Euro (€)             |  €                |
      | Hong Kong      |          | Hong Kong Dollar ($) | $                |
      | Japan          |          | Japanese Yen (¥)     |  ¥                |

  @regression
  Scenario Outline: Switching currency from header causes currency to be updated on PDPs - AUD

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    Then I should see selected currency as '<pdp_price_symbol>' on PDP page
    Examples:
   |country   |language| currency              |  pdp_price_symbol |
   |Australia   |      |Australian Dollar ($)  |  $                |



