#Author: Mona Agrawal
@regression @account @addressBook @teamSecure
Feature: Address Book page
  In order to add, delete and update addresses
  A customer logged into the website
  Should be able to use the Address Book page
############# SMOKE Scenario: Please do not remove ######################

  @smoke
  Scenario Outline: (2) Create a new address on my address book page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    And I click on 'ADD A NEW ADDRESS'
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1      | address_line_2      | town_city          | postcode_zip | pccc          |
      | DO NOT DELETE LINE1 | DO NOT DELETE LINE2 | DO NOT DELETE Town | 12345        | P111111111111 |
    Then the 'ADD ADDRESS' form on Address Book page disappears
    And the first line of the first address in my Address Book contains '<address_line_1>'

    Examples:
      | country           | language | currency | address_line_1 |
      | United Kingdom    |          |          | DO NOT DELETE LINE1|



#  @smoke
#  Scenario Outline: (7) Saving and deleting a new address should cause my address book to be updated.
#
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    And I navigate to '/login'
#    And I log in with the following credentials
#    And I navigate to the Address Book page
#    And I click on 'ADD A NEW ADDRESS'
#    And All address form fields are available for '<language>'
#    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
#      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
#      | 番地・部屋番号   | 市区町村・町域   | 都道府県   | 1234         |               |
#  #  Then the 'ADD ADDRESS' form on Address Book page disappears
#    And the fifth line of the first address in my Address Book contains '<address_line_1>'
#    When I click Delete under the first address in my Address Book
#    Then the 'DELETE ADDRESS' confirmation overlay appears
#    When I click 'DELETE' on the 'DELETE ADDRESS' confirmation overlay
#    Then the fifth line of the first address in my Address Book should not contain '<address_line_1>'
#
#    Examples:
#      | country | language | currency | address_line_1 |
#      | Japan   | 日本語    |          | 番地・部屋番号   |
##      | Japan   | English  |          | 番地・部屋番号   |



   @smoke
  Scenario Outline: (11) Delete an address on my address book page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    When I delete the following address in my Address Book
      | address_line_1      | town_city           | county_state | postcode_zip |
      | DO NOT DELETE LINE1 |  DO NOT DELETE Town |              | 12345        |


    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |



  #################################################

#
  @done @detailed @bug @EK-732 @dev4
  Scenario: (1) Clicking 'ADD A NEW ADDRESS' in the Address Book page causes the page to auto-scroll to the top.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    When I click on 'ADD A NEW ADDRESS'
    Then I should see the 'ADD ADDRESS' section
    And clicking 'CANCEL' should close the 'ADD ADDRESS' form


    @done @essential
  Scenario Outline: (2) Create a new address on my address book page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    And I click on 'ADD A NEW ADDRESS'
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1      | address_line_2      | town_city          | postcode_zip | pccc          |
      | DO NOT DELETE LINE1 | DO NOT DELETE LINE2 | DO NOT DELETE Town | 12345        | P111111111111 |
    Then the 'ADD ADDRESS' form on Address Book page disappears
    And the first line of the first address in my Address Book contains '<address_line_1>'

    Examples:
      | country           | language | currency | address_line_1 |
      | United Kingdom    |          |          | DO NOT DELETE LINE1|




  @done @essential @latinChars @failedTSEReg474
  Scenario Outline: (3) Name and Surname should not accept non-latin characters

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    When I click 'EDIT' underneath the last address in my Address Book
    When I use the 'EDIT ADDRESS' form to enter INVALID firstName or lastName
      | firtName | lastName |
      | 테스트      | 테스트      |
    Then I should see the validation error message '<error>'

    Examples:
      | country           | language | currency | error                                     |
      | United Kingdom    |          |          | Please use Latin characters only.         |
      | United States     |          |          | Please use Latin characters only.         |
      | Australia         |          |          | Please use Latin characters only.         |
      | France            | Français |          | Merci de n'utiliser que l'alphabet latin. |
      | France            | English  |          | Please use Latin characters only.         |
      | Korea Republic of | 한국어      |          | 영문만 입력 가능합니다.                             |
      | Korea Republic of | English  |          | Please use Latin characters only.         |
      | Japan             | 日本語      |          | 半角英数字で入力してください                            |
      | Japan             | English  |          | Please use Latin characters only.         |
#
  @done @essential @latinChars @failedTSEReg474
  Scenario Outline: (4) Name and Surname should only accept latin characters

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    When I click 'EDIT' underneath the last address in my Address Book
    And I use the 'EDIT ADDRESS' form to enter valid firstName or lastName
      | firtName            | lastName       |
      | testùàûâæçéèêëïôœÿÿ | testáéíñóúü¿¡ÿ |
    Then It should close the 'EDIT ADDRESS' form
    And I click 'EDIT' underneath the last address in my Address Book
    And I use the 'EDIT ADDRESS' form to enter valid firstName or lastName
      | firtName | lastName |
      | Test     | Test     |

    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |
      | United States     |          |          |
      | Australia         |          |          |
      | France            | Français |          |
      | France            | English  |          |
      | Korea Republic of | 한국어      |          |
      | Korea Republic of | English  |          |
      | Japan             | 日本語      |          |
      | Japan             | English  |          |


  @done @detailed @mytag @essential
  Scenario Outline: (5) Using Postcode/Zip Search details where only one address is found causes the address form to be automatically populated.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    And I delete the following address in my Address Book
      | address_line_1                              | town_city | county_state | postcode_zip |
      | Prime Minister & First Lord Of The Treasury | London    |              | SW1A 2AA     |
    And I click on 'ADD A NEW ADDRESS'
    And Uni-Pass field should not be visible to customer
    When I use the 'ADD ADDRESS' Postcode Lookup feature
      | house_name_number | postcode_zip |
      | 10                | SW1A 2AA     |
    And I save the address in my Address Book
    Then the first address in my Address Book contains the expected address
      | address_line_1                              | town_city | county_state | postcode_zip |
      | Prime Minister & First Lord Of The Treasury | London    |              | SW1A 2AA     |

    Examples:
      | country        |language | currency |
      | United Kingdom |         |          |




  @done @essential
  Scenario Outline: (6) Saving and deleting a new address with uni-pass should cause my address book to be updated.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    And I click on 'ADD A NEW ADDRESS'
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    Then the 'ADD ADDRESS' form on Address Book page disappears
    And the first line of the first address in my Address Book contains '<address_line_1>'
    When I click Delete under the first address in my Address Book
    Then the 'DELETE ADDRESS' confirmation overlay appears
    When I click 'DELETE' on the 'DELETE ADDRESS' confirmation overlay
    Then the first line of the first address in my Address Book should not contain '<address_line_1>'

    Examples:
      | country           | language | currency | address_line_1 |
      | United Kingdom    |          |          | 1 Test Street  |
      | France            | Français |          | 1 Test Street  |
      | France            | English  |          | 1 Test Street  |
      | Korea Republic of | 한국어     |          | 1 Test Street  |
      | Korea Republic of | English  |          | 1 Test Street  |



  @done @essential
  Scenario Outline: (7) Saving and deleting a new address should cause my address book to be updated.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    And I click on 'ADD A NEW ADDRESS'
    And All address form fields are available for '<language>'
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 番地・部屋番号   | 市区町村・町域   | 都道府県   | 1234         |               |
    Then the 'ADD ADDRESS' form on Address Book page disappears
    And the fifth line of the first address in my Address Book contains '<address_line_1>'
    When I click Delete under the first address in my Address Book
    Then the 'DELETE ADDRESS' confirmation overlay appears
    When I click 'DELETE' on the 'DELETE ADDRESS' confirmation overlay
    Then the fifth line of the first address in my Address Book should not contain '<address_line_1>'

    Examples:
      | country | language | currency | address_line_1 |
      | Japan   | 日本語    |          | 番地・部屋番号   |
      | Japan   | English  |          | 番地・部屋番号   |



  @done @essential @failedTSEReg474
  Scenario Outline: (8) Adding and Editing a USA address should display a state select.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    And I click on 'ADD A NEW ADDRESS'
    When I enter address details in 'ADD ADDRESS' form to create a new address
      | country       | address_line_1 | address_line_2 | town_city  | state        | postcode_zip |
      | Uruguay       | 1 Test Rua     | Test Road      | Montevideo | Pampa Gaucho | 1234         |
      | United States | 1 Test Street  | Test Road      | Testown    | California   | 1234         |
    And I click in 'SAVE ADDRESS'
    Then the 'ADD ADDRESS' form on Address Book page disappears
    And the third line of the first address in my Address Book contains '<address_line_3>'
    When I click Delete under the first address in my Address Book
    Then the 'DELETE ADDRESS' confirmation overlay appears
    When I click 'DELETE' on the 'DELETE ADDRESS' confirmation overlay
    Then the first line of the first address in my Address Book should not contain '<address_line_1>'

    Examples:
      | address_line_3 | address_line_1 |
      | CA             | 1 Test Street  |


  @done @essential @failedTSEReg474
  Scenario Outline: (9) Editing an address should cause my address book to be updated.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    And I click Edit underneath the '<address_Country>' address
    When I use the 'EDIT ADDRESS' form to enter valid address details for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | testing 123    | testing 456    | testing   | 7890         | P111111111111 |
    Then the 'EDIT ADDRESS' form on Address Book page disappears
    And my '<address_Country>' address contains the updated address details
      | address_line_1 | address_line_2 | postcode_zip | town_city |
      | testing 123    | testing 456    | 7890         | testing   |
    When I click Edit underneath the '<address_Country>' address
    And I use the 'EDIT ADDRESS' form to enter valid address details for '<country>'
      | address_line_1                 | address_line_2 | town_city    | postcode_zip |pccc          |
      | Reserved For Edit Address Test | Do Not Delete  | San Salvador | 11101      |P111111111111 |
    Then the 'EDIT ADDRESS' form on Address Book page disappears
    And my '<address_Country>' address contains the updated address details
      | address_line_1                 | address_line_2 | postcode_zip | town_city    |
      | Reserved For Edit Address Test | Do Not Delete  | 11101      | San Salvador |

    Examples:
      | country           | language | currency | address_Country |
      | United Kingdom    |          |          | El Salvador     |
      | France            | Français |          | El Salvador     |
      | France            | English  |          | El Salvador     |
      | Korea Republic of | 한국어      |          | 대한민국           |
      | Korea Republic of | English  |          | Korea Republic of |
      | Japan             | 日本語      |          | エルサルバドル         |
      | Japan             | English  |          | El Salvador     |


  @done @essential
  Scenario Outline: (10) PCCC updated on My Details page should get pre-populated on address form while adding new address

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the My Details page
    When I change the Uni-Pass to <Uni_Pass>
    And I save the Personal Details
    And A success message should be displayed in '<language>'
    And I navigate to the Address Book page
    And I click on 'ADD A NEW ADDRESS'
    Then the Uni-Pass should contain <Uni_Pass>

    Examples:
      | country           | language | currency | title | first_name | last_name | phone_number  | Uni_Pass      |
      | Korea Republic of | English  |          | Ms    | testing in | progress  | 020 7022 0828 | P555555555555 |
      | Korea Republic of | 한국어     |          | Ms    | testing in | progress  | 020 7022 0828 | P666666666666 |



     @done @essential
  Scenario Outline: (11) Delete an address on my address book page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Address Book page
    When I delete the following address in my Address Book
      | address_line_1      | town_city           | county_state | postcode_zip |
      | DO NOT DELETE LINE1 |  DO NOT DELETE Town |              | 12345        |


    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |

