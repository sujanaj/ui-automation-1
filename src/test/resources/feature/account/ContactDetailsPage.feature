#Author: Mona Agrawal
  @regression @account @contactDetails @teamSecure
  Feature: Contact Details
  In order to review and update their contact details and password
  A customer logged into the website
  Should be able to use the Contact Details page

  @done @essential
  Scenario Outline: (1) Amending my details for country Korea in the My Details page and clicking Save Details causes my details to be updated.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the My Details page
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<phone_number>' in the Phone Number field
    And I change the Uni-Pass to <Uni_Pass>
    And I save the Personal Details
    Then A success message should be displayed in '<message>'
    And the Title should contain <title>
    And the First Name should contain <first_name>
    And the Last Name should contain <last_name>
    And the Phone Number should contain <phone_number>
    And the Uni-Pass should contain <Uni_Pass>

    Examples:
      | country           | language | currency | title | first_name | last_name | phone_number  | Uni_Pass      | message                       |
      | Korea Republic of | 한국어      |          | Mrs   | testing in | progress  | 020 7022 0828 | P111111111111 | 고객님의 개인 정보가 변경되었습니다.          |
      | Korea Republic of | English  |          | Mrs   | test       | user      | 020 7022 0828 | P555555555555 | Your profile has been updated |

  @done @essential
  Scenario Outline: (2) Amending my details for country other than Korea in the My Details page and clicking Save Details causes my details to be updated.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the My Details page
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<phone_number>' in the Phone Number field
    And I save the Personal Details
    Then A success message should be displayed in '<message>'
    And the Title should contain <title>
    And the First Name should contain <first_name>
    And the Last Name should contain <last_name>
    And the Phone Number should contain <phone_number>
    And Uni-Pass field should not be visible to customer

    Examples:
      | country        | language | currency | title | first_name | last_name | phone_number  | Uni_Pass      | message                       |
      | United Kingdom | English  |          | Mrs   | testing in | progress  | 020 7022 0828 | P111111111111 | Your profile has been updated |
      | France         | Français |          | Mme   | testing in | progress  | 020 7022 0828 | P111111111111 | Votre profil a été mis à jour |
      | France         | English  |          | Mrs   | testing in | progress  | 020 7022 0828 | P111111111111 | Your profile has been updated |
      | Japan          | 日本語      |          | Mrs   | testing in | progress  | 020 7022 0828 | P111111111111 | プロフィールが更新されました                |
      | Japan          | English  |          | Mrs   | testing in | progress  | 020 7022 0828 | P111111111111 | Your profile has been updated |


  @done @essential @latinChars
  Scenario Outline: (3) Amending my details with non-latin characters in the My Details page and clicking Save should NOT accept non-latin characters.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the My Details page
    When  I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I save the Personal Details
    Then I should see the validation error message '<error>' on save


    Examples:
      | country           | language | currency | first_name | last_name | error                                     |
      | United Kingdom    |          |          | 테스트        | 테스트       | Please use Latin characters only.         |
      | France            | Français |          | 테스트        | 테스트       | Merci de n'utiliser que l'alphabet latin. |
      | France            | English  |          | 테스트        | 테스트       | Please use Latin characters only.         |
      | Korea Republic of | 한국어      |          | 테스트        | 테스트       | 영문만 입력 가능합니다.                             |
      | Korea Republic of | English  |          | 테스트        | 테스트       | Please use Latin characters only.         |
      | Japan             | 日本語      |          | 테스트        | 테스트       | 半角英数字で入力してください                            |
      | Japan             | English  |          | 테스트        | 테스트       | Please use Latin characters only.         |

  @done @essential @failedTSEReg474
  Scenario Outline: (4) Changing my password in the Contact Details page and clicking Save Details causes my password to be updated.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the My Details page
    Then entering valid details in the 'CHANGE PASSWORD' form and clicking 'SAVE PASSWORD' triggers a success message '<success_message>'
    Examples:
      | country           | language | currency | success_message                  |
      | United Kingdom    |          |          | Your password has been changed   |
      | France            | Français |          | Votre mot de passe a été modifié |
      | France            | English  |          | Your password has been changed   |
      | Korea Republic of | 한국어      |          | 비밀번호가 변경되었습니다.                   |
      | Korea Republic of | English  |          | Your password has been changed   |
      | Japan             | 日本語      |          | パスワードが変更されました                    |
      | Japan             | English  |          | Your password has been changed   |


  Scenario Outline: (5) Account should be created successfully when valid credentials are entered

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter new email '<first_name>' in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<Mobile_number>' in the Phone Number field
    And I click Create An Account
    And I should be redirected to the homepage
    And I should be signed in
    And I navigate to the My Details page
    And I select birthday date and month
    Then I should save birthday details and trigger a success message '<success_message>'
    Examples:
      | country           | language | currency | title | first_name | last_name | password   | Mobile_number | success_message                         |
      | United Kingdom    |          |          | Mr    | matches    | autotest  | Matches123 | 5522244788    | Your birthday has been updated          |
      | France            | English  |          | Mr    | matches    | autotest  | Matches123 | 5522244788    | Your birthday has been updated          |
      | France            | Français |          | Mme   | matches    | autotest  | Matches123 | 5522244788    | Votre date de naissance est mise a jour |
      | Korea Republic of | English  |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    | Your birthday has been updated          |
      | Korea Republic of | 한국어      |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    | 고객님의 생일이 저장되었습니다.                       |
      | Japan             | English  |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    | Your birthday has been updated          |
      | Japan             | 日本語      |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    | お誕生日が登録されました                            |






