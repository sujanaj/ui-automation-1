#Author: Mona Agrawal
@regression @account @myCredits @teamSecure
Feature: My Credits page
  In order to view existing credit and activate gift cards
  A customer logged into the website
  Should be able to use the My Credits page

  @done @essential @failedTSEReg474
  Scenario Outline: (1) Input fields should have the correct placeholder text

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    When I navigate to the My Credits page
    Then  the Gift Card Activation Code field should contain '<message>' as Placeholder Text

    Examples:
      | country           | language | currency |message|
      | United Kingdom    |          |          |Enter your 8-digit activation code|
      | France            | Français |          |Code d'activation de 8 caractères |
      | France            | English  |          |Enter your 8-digit activation code|
      | Korea Republic of | 한국어     |          |8자리 코드를 입력해 주세요.             |
      | Korea Republic of | English  |          |Enter your 8-digit activation code|
      | Japan             | 日本語    |          |8桁のアクティベーションコードを入力    |
      | Japan             | English  |          |Enter your 8-digit activation code|


  Scenario Outline: (2) Switching target currency for credit conversion should cause the values to be updated and correctly summed

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    When I navigate to the My Credits page
    Then For county '<country>' the Total should be correctly summed and the currency symbol updated
      | currency_option | currency_symbol |
      | GBP             | £               |
      | USD             | $               |
      | EUR             | €               |
      | HKD             | $               |
      | JPY             | ¥               |
      | AUD             | $               |

    Examples:
      | country        | language | currency |
      | United Kingdom |          |       |
      | United States  |          |       |
      | Australia      |          |       |
      | France         | Français |       |
      | Hong Kong      |          |       |
      | Japan          | 日本語    |       |

