#Author - Dilbag

Feature: Shopping Bag

  @smoke
  Scenario Outline: Using the 'Remove Item' link in the Shopping Bag page to remove all items from your bag

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I navigate to '<url>'
    Then I remove all items from my shopping bag via the link
    And I should see the message '<message>' confirming my Shopping Bag is empty

    Examples:
      | country | language | currency | message | url |
      | United Kingdom |          |          | Your shopping basket is currently empty | /shopping-bag |
      | France         | Français |          | Votre panier est vide                   | /shopping-bag |

  @smoke
  Scenario Outline: Editing shopping bag from Review and Pay Page

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    When I edit the order from Review and Pay page
    Then I should be on '<shopping_bag_title>' page
    And I change the size of the product on shopping bag
    Then I verify the size selected for the product
    And I change the quantity of the product to quantity
    Then I verify the quantity of the product if changed to '<quantity>'
    And I clean my shopping bag

    Examples:
      | country | language | currency | shopping_bag_title | quantity |
      |  United Kingdom      |          |         | Shopping Bag |      |
      | Korea Republic of | 한국어     |          | 쇼핑백| |

  @regression
  Scenario Outline: Editing shopping bag from Review and Pay Page

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    When I edit the order from Review and Pay page
    Then I should be on '<shopping_bag_title>' page
    And I change the size of the product on shopping bag
    Then I verify the size selected for the product
    And I change the quantity of the product to quantity
    Then I verify the quantity of the product if changed to '<quantity>'
    And I remove all items from my shopping bag via the link

    Examples:
      | country           | language | currency | shopping_bag_title | quantity |
      | United Kingdom    |          |          | Shopping Bag       |          |
      | France            | Français |          | PANIER             |          |
      | Korea Republic of | 한국어      |          | 쇼핑백                |          |
      | Japan             | 日本語      |          | ショッピングバッグ          |          |

  @regression
  Scenario Outline: Change in size of same products should merge different sizes of same product into one.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    When I go to 'instock' product page
    When I add a product with '<size_1>' to bag
    And I add same product with '<size_2>' to bag
    And I go to shopping bag clicking on minicart
    Then I should be on '<shopping_bag_title>' page
    And I change the size of first product to '<size_2>'
    Then I verify the size selected for the product
    And I verify the quantity of the product if changed to '<quantity>'
    And I remove all items from my shopping bag via the link

    Examples:
      | country           | language | currency | shopping_bag_title | size_1 | size_2 | quantity |
      | United Kingdom    |          |          | Shopping Bag       |        |        | 2        |
      | France            | Français |          | PANIER             |        |        | 2        |
      | Korea Republic of | 한국어      |          | 쇼핑백                |        |        | 2        |
      | Japan             | 日本語      |          | ショッピングバッグ          |        |        | 2        |