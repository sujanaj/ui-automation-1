#Author: Valdas Ruskonis
@regression @PDP @PDPSocialLinks @teamProduct
Feature: PDP Social Links
  In order to check MATCHESFASHION on social sites
  A customer landed on PDP
  Should be able to access social links

  ######### SMOKE Test- please donot remove #######
  @smoke
  Scenario Outline: (1) Landing on the PDP of a product I should be able to access social links which open in a new window.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then each social icon on PDP opens new window with the correct URL and item description in query string

      | icon      | url_contains             |
      | facebook  | facebook.com/login.php   |
      | twitter   | twitter.com/intent/tweet |
      | pinterest | pinterest                |


    Examples:
      | country           | language | currency         |
      | United Kingdom    |          | US Dollar ($)    |
#      | France            | Français | US Dollar ($)    |
#      | Korea Republic of | 한국어      | US Dollar ($)    |
#      | Japan             | 日本語      | Japanese Yen (¥) |

  @smoke
  Scenario Outline: (2) Landing on the PDP of a product I should be able to access email a friend link which opens in an overlay.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the 'Email a friend' icon on PDP
    Then the 'TELL A FRIEND' overlay is displayed with page title '<title>'
    And clicking on the close button closes the 'TELL A FRIEND' overlay

    Examples:
      | country           | language | currency         | title         |
      | United States     |          |                  | TELL A FRIEND |
#      | France            | Français | US Dollar ($)    | RECOMMANDER   |
#      | Korea Republic of | 한국어      | US Dollar ($)    | 친구와 공유하기      |
#      | Japan             | 日本語      | Japanese Yen (¥) | お友達に知らせる      |



  #########################
  @regression
  Scenario Outline: (1) Landing on the PDP of a product I should be able to access social links which open in a new window.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then each social icon on PDP opens new window with the correct URL and item description in query string

      | icon      | url_contains             |
      | facebook  | facebook.com/login.php   |
      | twitter   | twitter.com/intent/tweet |
      | pinterest | pinterest                |


    Examples:
      | country           | language | currency         |
      | United Kingdom    |          | US Dollar ($)    |
      | France            | Français | US Dollar ($)    |
      | Korea Republic of | 한국어      | US Dollar ($)    |
      | Japan             | 日本語      | Japanese Yen (¥) |

   @regression
  Scenario Outline: (2) Landing on the PDP of a product I should be able to access email a friend link which opens in an overlay.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the 'Email a friend' icon on PDP
    Then the 'TELL A FRIEND' overlay is displayed with page title '<title>'
    And clicking on the close button closes the 'TELL A FRIEND' overlay

    Examples:
      | country           | language | currency         | title         |
      | United States     |          |                  | TELL A FRIEND |
      | France            | Français | US Dollar ($)    | RECOMMANDER   |
      | Korea Republic of | 한국어      | US Dollar ($)    | 친구와 공유하기      |
      | Japan             | 日本語      | Japanese Yen (¥) | お友達に知らせる      |
		
