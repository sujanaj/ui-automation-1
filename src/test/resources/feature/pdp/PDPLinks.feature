#Author: Valdas Ruskonis
@regression @PDP @PDPlinks @teamProduct
Feature: PDP Links
  In order to quickly access other pages via breadcrumbs or colour options, get to designer page or product category
  A customer landed on PDP
  Should be able to use links which lead to that specific content


  ###### SMOKE Test- Please donot remove #####
  @smoke
  Scenario Outline: (2) Landing on the PDP I should be able to use breadcrumbs to access homepage, designers page and specific designer page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then clicking on each PDP breadcrumb link should take me to the correct page
      | url                                              | breadcrumb | destination_url       |
      | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | Womens     | /womens               |
      | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | Designer   | /womens/designers     |
      | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | ATM        | /womens/designers/atm |

    Examples:
      | country           | language | currency          |
      | United Kingdom    |          | British Pound (£) |
#      | United States     |          | US Dollar ($)     |
#      | Australia         |          | US Dollar ($)     |
#      | France            | Français | US Dollar ($)     |
#      | Hong Kong         |          | US Dollar ($)     |
#      | Japan             | 日本語      | Japanese Yen (¥)  |
#      | Korea Republic of | 한국어      | US Dollar ($)     |
#


  ######################

  # Can ignore this scenario as not all the products will have color options
#  Scenario Outline: (1) Landing on the PDP of specific products I should be able to choose other colour options.
#
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    When I go to 'instock' product page
#    When I click on the 'Colour Options' thumbnail image
#    Then the link on the thumbnail image should take me to the correct URL '<page>'
#    And I click on the 'Colour Options' thumbnail image
#    Then the link on the thumbnail image should take me to the correct URL '<page_2>'
#
#    Examples:
#      | country           | language | currency          | page                                                                                                                | page_2                                                       |
#      | France            | Français | US Dollar ($)     | /products/ATM-T-shirt-en-jersey-de-coton-flamm%C3%A9-1009198                                                        | /products/ATM-T-shirt-en-jersey-de-coton-flamm%C3%A9-1009197 |
#      | Japan             | 日本語      | Japanese Yen (¥)  | /products/ATM-Slub-cotton-jersey-T-shirt-1009198                                                                    | /products/ATM-Slub-cotton-jersey-T-shirt-1009197             |
#      | Korea Republic of | 한국어      | US Dollar ($)     | /products/ATM-%EC%8A%AC%EB%9F%AC%EB%B8%8C-%EC%BD%94%ED%8A%BC-%EC%A0%80%EC%A7%80-%ED%8B%B0%EC%85%94%EC%B8%A0-1009198 | /products/ATM-슬러브-코튼-저지-티셔츠-1009197                          |
#      | United Kingdom    |          | British Pound (£) | products/ATM-Slub-cotton-jersey-T-shirt-1009198                                                                     | /products/ATM-T-shirt-en-jersey-de-coton-flamm%C3%A9-1009197 |

   @regression
  Scenario Outline: (2) Landing on the PDP I should be able to use breadcrumbs to access homepage, designers page and specific designer page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then clicking on each PDP breadcrumb link should take me to the correct page
      | url                                              | breadcrumb | destination_url       |
      | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | Womens     | /womens               |
      | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | Designer   | /womens/designers     |
      | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | ATM        | /womens/designers/atm |

    Examples:
      | country           | language | currency          |
      | United Kingdom    |          | British Pound (£) |
      | United States     |          | US Dollar ($)     |
      | Australia         |          | US Dollar ($)     |
      | France            | Français | US Dollar ($)     |
      | Hong Kong         |          | US Dollar ($)     |
      | Japan             | 日本語      | Japanese Yen (¥)  |
      | Korea Republic of | 한국어      | US Dollar ($)     |

  @regression
  Scenario Outline: (3) Landing on the PDP of specific products I should be able to quickly access the designer page via the 'VIEW ALL' link.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    Then clicking the first 'VIEW ALL' link should take me to the correct page '<url>'

    Examples:
      | country           | language | currency          | pdp                                              | url                   |
      | United Kingdom    |          | British Pound (£) | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/designers/atm |
      | United States     |          | US Dollar ($)     | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/designers/atm |
      | Australia         |          | US Dollar ($)     | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/designers/atm |
      | France            | Français | US Dollar ($)     | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/designers/atm |
      | Hong Kong         |          | US Dollar ($)     | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/designers/atm |
      | Japan             | 日本語      | Japanese Yen (¥)  | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/designers/atm |
       | Korea Republic of | 한국어      | US Dollar ($)      | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/designers/atm |

  @regression
  Scenario Outline: (4) Landing on the PDP of specific products I should be able to quickly access the product sub-category page via the 'VIEW ALL' link.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    Then clicking the second 'VIEW ALL' link should take me to the correct page '<url>'

    Examples:
      | country           | language | currency          | pdp                                              | url                        |
      | United Kingdom    |          | British Pound (£) | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/shop/clothing/tops |
      | United States     |          | US Dollar ($)     | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/shop/clothing/tops |
      | Australia         |          | US Dollar ($)     | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/shop/clothing/tops |
      | France            | Français | US Dollar ($)     | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/shop/clothing/tops |
      | Hong Kong         |          | US Dollar ($)     | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/shop/clothing/tops |
      | Japan             | 日本語      | Japanese Yen (¥)  | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/shop/clothing/tops |
      | Korea Republic of | 한국어      | US Dollar ($)      | /products/ATM-Slub-cotton-jersey-T-shirt-1009197 | /womens/shop/clothing/tops |