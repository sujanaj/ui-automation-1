#Author: Valdas Ruskonis
@regression @PDP @PDPSizeGuide @teamProduct
Feature: Size Guide
  In order to check product measurements
  A customer landed on PDP
  Should be able to access size guide

  @regression
  Scenario Outline: (1) Landing on the PDP of a product I should be able to open and close the size guide.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the 'Size Guide' link next to the size dropdown
    Then the 'SIZE AND FIT GUIDE' overlay is displayed
    And clicking on the 'CLOSE' button closes the 'SIZE AND FIT GUIDE' overlay

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          | US Dollar ($)    |
      | United States     |          | US Dollar ($)    |
      | Australia         |          | US Dollar ($)    |
      | France            | Français | US Dollar ($)    |
      | Hong Kong         |          | US Dollar ($)    |
      | Japan             | 日本語    | Japanese Yen (¥) |
      | Korea Republic of | 한국어     | US Dollar ($)    |

   @regression
  Scenario Outline: (2) Landing on the PDP of a product I should be able to open and close the size guide in the size and fit tab.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the 'Size Guide' link in the 'SIZE AND FIT' tab
    Then the 'SIZE AND FIT GUIDE' overlay is displayed
    And clicking on the 'CLOSE' button closes the 'SIZE AND FIT GUIDE' overlay

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          | US Dollar ($)    |
      | United States     |          | US Dollar ($)    |
      | Australia         |          | US Dollar ($)    |
      | France            | Français | US Dollar ($)    |
      | Hong Kong         |          | US Dollar ($)    |
      | Japan             | 日本語    | Japanese Yen (¥) |
      | Korea Republic of | 한국어     | US Dollar ($)    |

  @regression
  Scenario Outline: (3) Landing on the PDP of a product I should be able to select a size of a product and see changes reflected on the size guide.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I select the '<size>' on the PDP
    When I click on the 'Size Guide' link next to the size dropdown
    Then the 'SIZE' field in the Product Details section of the 'SIZE AND FIT GUIDE' overlay has size '<size>' selected
    And the 'SIZE' field in the 'MEASURING GUIDE' section of the 'SIZE AND FIT GUIDE' overlay has size '<size>' selected

    Examples:
      | country           | language | currency         | size |
      | United Kingdom    |          | US Dollar ($)    | XS   |
      | United States     |          | US Dollar ($)    | XS   |
      | Australia         |          | US Dollar ($)    | XS   |
      | France            | Français | US Dollar ($)    | XS   |
      | Hong Kong         |          | US Dollar ($)    | XS   |
      | Japan             | 日本語    | Japanese Yen (¥) | XS   |
      | Korea Republic of | 한국어     | US Dollar ($)    | XS   |
