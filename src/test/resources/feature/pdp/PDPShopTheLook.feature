#Author: Dilbag
@regression @PDP @PDPShopTheLook @teamProduct
Feature: Shop The Look
  In order to see matching products
  A customer landed on PDP
  Should be able to access Shop The Look

   @regression
  Scenario Outline: (1) Landing on the PDP of a product I should be able to open and close 'SHOP THE LOOK'.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the 'SHOP THE LOOK' button on the PDP
    Then the shop look overlay is displayed
    And clicking on the 'CLOSE' button closes the 'SHOP THE LOOK' overlay

    Examples:
      | country           | language | currency         |
      |United Kingdom     |          |                  |
      | France            | Français | Euro (€)    |
      | Korea Republic of | 한국어      | US Dollar ($)   |
      | Japan             | 日本語      | Japanese Yen (¥)|

  @done @essential
  Scenario Outline: (2) Landing on the PDP of a product I should be able to check if products in 'MATCH IT WITH' section appear on 'SHOP THE LOOK' overlay.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the 'SHOP THE LOOK' button on the PDP
    Then all products in the 'MATCH IT WITH' section appear on the 'SHOP THE LOOK' overlay

  #Please note that while all products in the 'MATCH IT WITH' section appear in the 'SHOP THE LOOK' section, not all products in 'SHOP THE LOOK' appear in 'MATCH IT WITH'

    Examples:
      | country           | product |
      | United Kingdom    | 1009198 |
      | France            | 1009198 |
      | Korea Republic of | 1009198 |
      | Japan             | 1009198 |

  @done @essential
  Scenario Outline: (3) Landing on the PDP of a product I should be able to check if prices of products in 'MATCH IT WITH' section are the same as on 'SHOP THE LOOK' overlay.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    When I click on the 'SHOP THE LOOK' button on the PDP
    Then the price of each product in the 'MATCH IT WITH' section is identical to the price of the same product in the 'SHOP THE LOOK' overlay
    Examples:
      | country           | product | currency |
      | Australia         | 1009198 | AUD      |
      | France            | 1009198 | EUR      |
      | Korea Republic of | 1009198 | HKD      |
      | Japan             | 1009198 | JPY      |


   @regression
  Scenario Outline: (4) Landing on the PDP of a product I should be able to scroll through the 'SHOP THE LOOK' overlay

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the 'SHOP THE LOOK' button on the PDP
    Then I should be able to scroll to the bottom of the 'SHOP THE LOOK' overlay

    Examples:
      | country           | language          | currency         |
      | France            | Français          | US Dollar ($)    |
      | Korea Republic of | 한국어               | US Dollar ($)    |
      | Japan             | 日本語               | Japanese Yen (¥) |
      | United Kingdom    | British Pound (£) | US Dollar ($)    |

  @regression
  Scenario Outline: (5) Landing on the PDP of a product I should be able to zoom in on the images on the  'SHOP THE LOOK' overlay

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I click on the 'SHOP THE LOOK' button on the PDP
    When I click on the image of the first 'instock' on the 'SHOP THE LOOK' overlay
    Then the first 'SHOP THE LOOK' 'instock' image zooms in

#	There is similar implementation of this final step in the PDPImagesAndVideo feature file

    Examples:
      | country           | language          | currency         |
      | France            | Français          | US Dollar ($)    |
      | Korea Republic of | 한국어               | US Dollar ($)    |
      | Japan             | 日本語               | Japanese Yen (¥) |
      | United Kingdom    | British Pound (£) | US Dollar ($)    |


#	The scenarios below are very similar to ones in PDPQTYAndSize and PDPAddToBag feature files

   @regression
  Scenario Outline: (6) Landing on the PDP of specific products I should be able to select size and quantity on the 'SHOP THE LOOK' overlay

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the 'SHOP THE LOOK' button on the PDP
    Then by selecting the size drop down list of product on the 'SHOP THE LOOK' overlay I can select different sizes
      | size |
      | S    |
      | M    |

    And Quantity drop down list on the 'SHOP THE LOOK' overlay becomes active
    Then by select the quantity drop down list using product on the 'SHOP THE LOOK' overlay I can select up to ten units
      | quantity |
      | 1        |

    Examples:
      | country           | language          | currency         |
      | France            | Français          | US Dollar ($)    |
      | Korea Republic of | 한국어               | US Dollar ($)    |
      | Japan             | 日本語               | Japanese Yen (¥) |
      | United Kingdom    | British Pound (£) | US Dollar ($)    |

  @regression
  Scenario Outline: (7) Landing on the PDP of a product which has no more than one size I should not be able to interact with quantity drop down list before selecting the size on the 'SHOP THE LOOK' overlay.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'oneSize' product page
    When I click on the 'SHOP THE LOOK' button on the PDP
    Then by clicking on the 'QUANTITY' drop down list on the 'SHOP THE LOOK' overlay for product I will not be able to open it

    Examples:
      | country           | language | currency         |
      | France            | Français | US Dollar ($)    |
      | Korea Republic of | 한국어      | US Dollar ($)    |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | United Kingdom    |          | US Dollar ($)    |

  @regression
  Scenario Outline: (8) Landing on the PDP of a product which has one of its sizes sold out I should be able to select that size on the 'SHOP THE LOOK' overlay and observe 'ADD TO BAG' button disappear.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'soldOut' product page
    When I click on the 'SHOP THE LOOK' button on the PDP
    Then by clicking on the 'SIZE' drop down list on the 'SHOP THE LOOK' overlay and selecting size '<size>' the 'ADD TO BAG' button should disappear

    Examples:
      | country           | language | currency         | size | message                             |
      | United States     |          | US Dollar ($)    | L-   | This item is out of stock           |
      | France            | Français | US Dollar ($)    | L-   | Cet article est en rupture de stock |
      | Korea Republic of | 한국어      | US Dollar ($)    | L-   | 이 상품은 품절입니다.                        |
      | Japan             | 日本語      | Japanese Yen (¥) | L-   | 売り切れ                                |
#
  #Commented as we cannot determine any product with qty 1
#  @regression
#  Scenario Outline: (9) Landing on the PDP of a product which has only one unit in stock of a specific size I should not be able to interact with quantity drop down list on the 'SHOP THE LOOK' overlay.
#
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    When I go to 'oneQty' product page
#    When I click on the 'SHOP THE LOOK' button on the PDP
#    Then by clicking on the 'SIZE' drop down list on the 'SHOP THE LOOK' overlay for the product and selecting size '<size>' I will not be able to open 'QUANTITY' drop down list
#
#    Examples:
#      | country           | language | currency         | size  |
#      | France            | Français | US Dollar ($)    | 38FR- |
#      | Korea Republic of | 한국어      | US Dollar ($)    | 38FR- |
#      | Japan             | 日本語      | Japanese Yen (¥) | 38FR- |
#      | United Kingdom    |          | US Dollar ($)    | 38FR- |
#
  @regression
  Scenario Outline: (10) Landing on the PDP of a product which has only one size I should be able to see text 'ONE SIZE' (or the local language equivalent) in the size drop down list on the 'SHOP THE LOOK' overlay.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'oneSize' product page
    Then I should be able to see the text '<text>' in the size drop down list on the 'SHOP THE LOOK' overlay
    Examples:
      | country           | language | currency         | text          |
      | Korea Republic of | 한국어      | US Dollar ($)    | ONE SIZE      |
      | France            | Français | US Dollar ($)    | TAILLE UNIQUE |
      | Australia         |          |                  | ONE SIZE      |
      | Japan             | 日本語      | Japanese Yen (¥) | ONE SIZE      |

   @regression
  Scenario Outline: (11) Landing on the PDP of a product I should be able to add a product to the shopping bag on the 'SHOP THE LOOK' overlay.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I click on the 'SHOP THE LOOK' button on the PDP
    When I add the product size '<size>' and '<quantity>' to my bag from the 'SHOP THE LOOK' overlay
#    Then the mini bag rollover list appears
    And the number next to the mini bag icon updates to '<quantity>'

    Examples:
      | country           | language | currency         | size | quantity |
      | Korea Republic of | 한국어      | US Dollar ($)    | S    | 1        |
      | France            | Français | US Dollar ($)    | XS   | 2        |
      | Hong Kong         |          | US Dollar ($)    | XS   | 2        |
      | Japan             | 日本語      | Japanese Yen (¥) | XS   | 2        |
		
