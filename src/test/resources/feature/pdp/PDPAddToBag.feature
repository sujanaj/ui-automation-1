#Author: Dilbag Singh
@regression @PDP @PDPAddToBag @teamProduct
Feature: Add to Bag from PDP
  In order to buy a product
  A customer landed on PDP
  Should be able to add a product to the shopping bag

  ########## SMOKE Test - Please donot remove ########
  @smoke
  Scenario Outline: (1) Landing on the PDP of a product I should be able to add a product to the shopping bag.

  The 'Added to bag' button in Wishlist has not been translated for the French website, which will cause this test to fail on the France example.
    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I add '<quantity>' of size '<size>' to my bag from the PDP
    Then the number next to the mini bag icon updates to '<quantity>'

    Examples:
      | country           | language | currency | size | quantity |
      | United Kingdom    |          | EUR €    | S    | 1        |
      | United States     |          | USD $    | S    | 1        |
      | Australia         |          | AUD $    | S    | 1        |
      | France            | Français | EUR €    | S    | 1        |
      | France            | English  | EUR €    | S    | 1        |
      | Korea Republic of | 한국어      | USD $    | S    | 1        |
      | Korea Republic of | English  | EUR €    | S    | 1        |
      | Japan             | 日本語      | JPY ¥    | S    | 1        |
      | Japan             | English  | JPY ¥    | S    | 1        |

  @smoke
  Scenario Outline: (2) Landing on the PDP of a product I should be able to add a product to the shopping bag on the sticky nav wrapper.

  The 'Added to bag' button in Wishlist has not been translated for the French website, which will cause this test to fail on the France example.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I scroll down to the bottom of the page
    And I add '<quantity>' of size '<size>' to my bag from the PDP
    And the number next to the mini bag icon on the sticky nav wrapper updates to '<quantity>'

    Examples:
      | country        | language | currency | size | quantity | text_1     | text_2       |
      | United Kingdom |          |          | S    | 1        | ADD TO BAG | ADDED TO BAG |
#      | United States     |          |          | S    | 1        | ADD TO BAG        | ADDED TO BAG     |
#      | Australia         |          |          | S    | 1        | ADD TO BAG        | ADDED TO BAG     |
#      | Hong Kong         |          |          | S    | 1        | ADD TO BAG        | ADDED TO BAG     |
#      | France            | Français |          | S    | 1        | AJOUTER AU PANIER | AJOUTÉ AU PANIER |
#      | Korea Republic of | 한국어      |          | S    | 1        | 쇼핑백 담기            | 쇼핑백에 담았습니다.      |
#      | Japan             | 日本語      |          | S    | 1        | ショッピングバッグに追加      | 追加しました           |

  @smoke
  Scenario Outline: (5) Landing on the PDP of a product I should be able to add a product to the shopping bag while viewing it in full screen.


    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I click on the PDP 'VIEW FULLSCREEN' button
    And I select size '<size>' from the full screen product overlay size drop down
    And clicking the 'ADD TO BAG' button on the full screen product overlay causes the minibag rollover list to appear
     #And the pdp 'ADD TO BAG' button text is momentarily changed from '<text_1>' to '<text_2>'
    Then the number next to the mini bag icon updates to '1'

    Examples:
      | country        | language | currency | size | text_1 | text_2 |
      | United Kingdom |          |          | S    |        |        |
#      | France            | Français |          | S    | AJOUTER AU PANIER | AJOUTÉ AU PANIER |
#      | Korea Republic of | 한국어      |          | S    | 쇼핑백 담기            | 쇼핑백에 담았습니다. |
#      | Japan             | 日本語      |          | S    | ショッピングバッグに追加     | 追加しました    |


  ##############################

  @regression
  Scenario Outline: (1) Landing on the PDP of a product I should be able to add a product to the shopping bag.

  The 'Added to bag' button in Wishlist has not been translated for the French website, which will cause this test to fail on the France example.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I add '<quantity>' of size '<size>' to my bag from the PDP
    Then the number next to the mini bag icon updates to '<quantity>'

    Examples:
      | country           | language | currency         | size | quantity |
      | United Kingdom    |          |                  | S    | 1        |
      | United States     |          |                  | S    | 1        |
      | Australia         |          |                  | S    | 1        |
      | France            | Français | US Dollar ($)    | S    | 1        |
      | France            | English  | Euro (€)         | S    | 1        |
      | Korea Republic of | 한국어      | US Dollar ($)    | S    | 1        |
      | Korea Republic of | English  | Euro (€)         | S    | 1        |
      | Japan             | 日本語      | Japanese Yen (¥) | S    | 1        |
      | Japan             | English  | US Dollar ($)    | S    | 1        |

  @regression
  Scenario Outline: (2) Landing on the PDP of certain products I should be able to see an error message if I try to add to bag without selecting a size.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then clicking the 'ADD TO BAG' button causes an error message '<error>' to appear under the size tab

    Examples:
      | country           | language | currency | error                            |
      | United Kingdom    |          |          | Please select a size             |
      | France            | Français |          | Veuillez sélectionner une taille |
      | Korea Republic of | 한국어      |          | 사이즈를 선택해 주세요.                    |
      | Japan             | 日本語      |          | サイズ選択                            |

  @regression
  Scenario Outline: (3) Landing on the PDP of a product I should be able to add a product to the shopping bag on the sticky nav wrapper.

  The 'Added to bag' button in Wishlist has not been translated for the French website, which will cause this test to fail on the France example.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I scroll down to the bottom of the page
    And adding '<quantity>' of size '<size>' to bag from the PDP sticky nav wrapper causes the minibag rollover list to appear
  #  And the pdp 'ADD TO BAG' button text is momentarily changed from '<text_1>' to '<text_2>'
    Then the number next to the mini bag icon on the sticky nav wrapper updates to '<quantity>'

    Examples:
      | country           | language | currency | size | quantity | text_1            | text_2           |
      | United Kingdom    |          |          | S    | 1        | ADD TO BAG        | ADDED TO BAG     |
      | United States     |          |          | S    | 1        | ADD TO BAG        | ADDED TO BAG     |
      | Australia         |          |          | S    | 1        | ADD TO BAG        | ADDED TO BAG     |
      | Hong Kong         |          |          | S    | 1        | ADD TO BAG        | ADDED TO BAG     |
      | France            | Français |          | S    | 1        | AJOUTER AU PANIER | AJOUTÉ AU PANIER |
      | Korea Republic of | 한국어      |          | S    | 1        | 쇼핑백 담기            | 쇼핑백에 담았습니다.      |
      | Japan             | 日本語      |          | S    | 1        | ショッピングバッグに追加      | 追加しました           |

  @regression
  Scenario Outline: (4) Landing on the PDP of a product I should be able to see an error message if I try to add to bag without selecting a size on the sticky nav wrapper.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I scroll down to the bottom of the page
    Then clicking the 'ADD TO BAG' button on the sticky nav wrapper causes an error message '<error>' to appear under the size tab

    Examples:
      | country           | language | currency | error                            |
      | United Kingdom    |          |          | Please select a size             |
      | France            | Français |          | Veuillez sélectionner une taille |
      | Korea Republic of | 한국어      |          | 사이즈를 선택해 주세요.                    |
      | Japan             | 日本語      |          | サイズ選択                            |

  @regression
  Scenario Outline: (5) Landing on the PDP of a product I should be able to add a product to the shopping bag while viewing it in full screen.


    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I click on the PDP 'VIEW FULLSCREEN' button
    And I select size '<size>' from the full screen product overlay size drop down
    And clicking the 'ADD TO BAG' button on the full screen product overlay causes the minibag rollover list to appear
     #And the pdp 'ADD TO BAG' button text is momentarily changed from '<text_1>' to '<text_2>'
    Then the number next to the mini bag icon updates to '1'

    Examples:
      | country           | language | currency | size | text_1            | text_2           |
      | United Kingdom    |          |          | S    | ADD TO BAG        | ADDED TO BAG     |
      | France            | Français |          | S    | AJOUTER AU PANIER | AJOUTÉ AU PANIER |
      | Korea Republic of | 한국어      |          | S    | 쇼핑백 담기            | 쇼핑백에 담았습니다.      |
      | Japan             | 日本語      |          | S    | ショッピングバッグに追加      | 追加しました           |

  @regression @DilbagPDP
  Scenario Outline: (6) Landing on the PDP of a product I should be able to see an error message if I try to add to bag without selecting a size on the full screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the PDP 'VIEW FULLSCREEN' button
    Then clicking the 'ADD TO BAG' button on the full screen product overlay causes an error message '<error>' to appear under the size tab

    Examples:
      | country           | language | currency | error                            |
      | United States     |          |          | Please select a size             |
      | France            | Français |          | Veuillez sélectionner une taille |
      | Japan             | 日本語      |          | サイズ選択                            |
      | Korea Republic of | 한국어      |          | 사이즈를 선택해 주세요.                    |
