#Author: Dilbag
@regression @PDP @PDPImagesAndVideo @teamProduct
Feature: PDP Images and video
  In order to see how the product looks
  A customer landed on PDP
  Should be able to scroll through images and watch a video

  ######### SMOKE Test - Please donot remove ########

  #@smoke
  Scenario Outline: (1) Landing on the PDP of certain products I should be able to watch a video by clicking 'VIEW VIDEO' link.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the VIEW VIDEO link
    Then the video should begin playing over the main PDP image
    And the video will continue playing until I click close

    Examples:
      | country           | language | currency          |
      | United Kingdom    |          |                   |
#      | United States     |          |                   |
#      | Australia         |          |                   |
#      | France            | English  | British Pound (£) |
#      | France            | Français | Euro (€)          |
#      | Japan             | 日本語      | Japanese Yen (¥)  |
#      | Korea Republic of | 한국어      | US Dollar ($)     |

  @smoke @failed
  Scenario Outline: (4) On the PDP clicking the image thumbnails should cause the main PDP image to scroll

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then clicking on each thumbnail image causes the main PDP image to change

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | Euro (€)         |
      | Japan             | 日本語      | Japanese Yen (¥)|
      | Korea Republic of | 한국어      | US Dollar ($)    |
#
#
  @smoke
  Scenario Outline: (7) Landing on the PDP of a product I should be able to zoom in on the main PDP image

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the main PDP image
    Then the main PDP image zooms in
    And moving the cursor away from the image causes the image to return to normal

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
#      | France            | Français | Euro (€)         |
#      | Japan             | 日本語      | Japanese Yen (¥) |
#      | Korea Republic of | 한국어      | US Dollar ($)    |
#
#
#  ############################

  @regression
  Scenario Outline: (1) Landing on the PDP of certain products I should be able to watch a video by clicking 'VIEW VIDEO' link.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the VIEW VIDEO link
    Then the video should begin playing over the main PDP image
    And the video will continue playing until I click close

    Examples:
      | country           | language | currency          |
      | United Kingdom    |          |                   |
      | United States     |          |                   |
      | Australia         |          |                   |
      | France            | English  | British Pound (£) |
      | France            | Français | Euro (€)          |
      | Japan             | 日本語      | Japanese Yen (¥)  |
      | Korea Republic of | 한국어      | US Dollar ($)     |

   @regression
  Scenario Outline: (2) While watching a video of a product I should be able to pause it.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I click on the VIEW VIDEO link
    When I click on the pause button
    Then the video should be paused
    And when I click on the play button
    Then the video will begin playing

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | Euro (€)         |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | Korea Republic of | 한국어      | US Dollar ($)    |

  @regression
  Scenario Outline: (3) On the PDP clicking the image thumbnails should cause the main PDP image to scroll

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then clicking on each thumbnail image causes the main PDP image to change

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | Euro (€)         |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | Korea Republic of | 한국어      | US Dollar ($)    |

  @regression
  Scenario Outline: (4) Landing on the PDP of a product I should be able to view a bigger image of small images by cycling through them with arrows.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then clicking on the 'Next' arrow causes the main PDP image to change
    And clicking on the 'Previous' arrow causes the main PDP image to change

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | Euro (€)         |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | Korea Republic of | 한국어      | US Dollar ($)    |

  @regression
  Scenario Outline: (5) Landing on the PDP of a product I should be able to view large thumbnails of the product images.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I select VIEW LARGE THUMBNAILS button
    Then I should see all big images of the product displayed in a single column
    And I select VIEW SMALL THUMBNAILS button
    Then I should see only one big image of the first thumbnail displayed

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | Euro (€)         |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | Korea Republic of | 한국어      | US Dollar ($)    |

  @regression
  Scenario Outline: (6) Landing on the PDP of a product I should be able to zoom in on the main PDP image

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the main PDP image
    Then the main PDP image zooms in
    And moving the cursor away from the image causes the image to return to normal

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | Euro (€)         |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | Korea Republic of | 한국어      | US Dollar ($)    |

  @regression
  Scenario Outline: (7) On the PDP fullscreen mode clicking the image thumbnails should cause the main PDP image to scroll

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the PDP 'VIEW FULLSCREEN' button
    Then I should see the main PDP image in full screen
    And clicking on each thumbnail causes the main PDP image to change
    And with each scrolled image numbers on the bottom of the page should update
    When I click on the PDP 'CLOSE' button
    Then the view should reset to default

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | Euro (€)         |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | Korea Republic of | 한국어      | US Dollar ($)    |

  @regression
  Scenario Outline: (8) Landing on the PDP of a product in full screen I should be able to view a bigger image of small images by cycling through them with arrows.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I click on the PDP 'VIEW FULLSCREEN' button
    Then clicking on the fullscreen 'Next' arrow causes the main PDP image to change
    And clicking on the fullscreen 'Previous' arrow causes the main PDP image to change

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | Euro (€)         |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | Korea Republic of | 한국어      | US Dollar ($)    |
