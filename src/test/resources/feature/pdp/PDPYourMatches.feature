#Author: Sujana Jakkula
@regression @pdp @pdpYourMatches @teamProduct
Feature: Recently Viewed from PDP
  In order to see the recently viewed and match it with products
  A customer landed on PDP
  Should be able to view all the recently viewed and match it with products in 'YOUR MATCHES' block in the PDP


  @regression
  Scenario Outline: (1) Verify 'RECENTLY VIEWED' block title is displayed on the PDP when customer landed on PDP

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock_SecondProduct' product page
    And refresh the page
    When I go to 'instock' product page
    And refresh the page
    When I go to 'instock_ThirdProduct' product page
    Then I should see the 'RECENTLY VIEWED' section with title '<title>' on the PDP

    Examples:
      | country           | language | currency         | title           |
      | United States     |          | US Dollar ($)    | RECENTLY VIEWED |
      | France            | Français | US Dollar ($)    | VUS RÉCEMMENT   |
      | Korea Republic of | 한국어      | US Dollar ($)    | 최근 본 상품         |
      | Japan             | 日本語      | Japanese Yen (¥) | 最近チェックしたアイテム    |


   @regression
  Scenario Outline: (2) Verify 'YOUR MATCHES' block title is displayed on the PDP

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock_SecondProduct' product page
    And refresh the page
    When I go to 'instock' product page
     And refresh the page
     When I go to 'instock_ThirdProduct' product page
    Then I should see the 'YOUR MATCHES' section with title '<title>' on the PDP

    Examples:
      | country           | language | currency         | title              |
      | Australia         |          | US Dollar ($)    | YOUR MATCHES       |
      | France            | Français | US Dollar ($)    | VOUS AIMEREZ AUSSI |
      | Korea Republic of | 한국어      | US Dollar ($)    | YOUR MATCHES       |
      | Japan             | 日本語      | Japanese Yen (¥) | YOUR MATCHES       |


   @regression
  Scenario Outline: (3) Verify sub block title 'MATCH IT WITH' and 'RECENTLY VIEWED' is displayed within the block title on the PDP

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock_SecondProduct' product page
    And refresh the page
    When I go to 'instock' product page
     And refresh the page
     When I go to 'instock_ThirdProduct' product page
    Then I should see sub block titles '<sub_title1> ' and '<sub_title2>' beneath the block title '<title>' in the 'YOUR MATCHES' section of the PDP

    Examples:
      | country           | language | currency         | sub_title1    | sub_title2      | title              |
      | Korea Republic of | 한국어      | US Dollar ($)    | 스타일링 제안       | 최근 본 상품         | 최근 본 상품       |
      | France            | Français | US Dollar ($)    | PORTEZ AVEC   | VUS RÉCEMMENT   | VOUS AIMEREZ AUSSI |
      | Japan             | 日本語      | Japanese Yen (¥) | コーディネートアイテム   | 最近チェックしたアイテム    | YOUR MATCHES       |


   @regression
  Scenario Outline: (4) Verify only previously viewed products are displayed in 'Recently Viewed' tab
    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock_SecondProduct' product page
    And refresh the page
    When I go to 'instock' product page
     And refresh the page
     When I go to 'instock_ThirdProduct' product page
    When I click on the 'RECENTLY VIEWED' tab in the 'YOUR MATCHES' section
    Then I should see my previously viewed product '<product_description>' displayed in the 'RECENTLY VIEWED' tab

    Examples:
      | country           | language | currency          | product_description        |
      | United Kingdom    |          | British Pound (£) | Slub cotton-jersey T-shirt |
      | Korea Republic of | 한국어      | US Dollar ($)     | Slub cotton-jersey T-shirt |
      | France            | Français | US Dollar ($)     | Slub cotton-jersey T-shirt |
      | Japan             | 日本語      | Japanese Yen (¥)  | Slub cotton-jersey T-shirt |

  @regression
  Scenario Outline: (5) Verify clicking on 'RECENTLY VIEWED' product takes me to the correct page


    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock_SecondProduct' product page
    And refresh the page
    When I go to 'instock' product page
    And refresh the page
    When I go to 'instock_ThirdProduct' product page
    When I click on the 'RECENTLY VIEWED' tab in the 'YOUR MATCHES' section
    And I click the '<product>' product link in the 'RECENTLY VIEWED' tab of the PDP
    Then I am taken to the correct page '<target_url>'

    Examples:
      | country           | language | currency         | product                    | target_url                                          |
      | United States     |          | US Dollar ($)    | Slub cotton-jersey T-shirt | /us/products/ATM-Slub-cotton-jersey-T-shirt-1009197 |
      | Korea Republic of | 한국어      | US Dollar ($)    | Slub cotton-jersey T-shirt | /kr/products/ATM-Slub-cotton-jersey-T-shirt-1009197 |
      | France            | Français | US Dollar ($)    | Slub cotton-jersey T-shirt | /fr/products/ATM-Slub-cotton-jersey-T-shirt-1009197 |
      | Japan             | 日本語      | Japanese Yen (¥) | Slub cotton-jersey T-shirt | /jp/products/ATM-Slub-cotton-jersey-T-shirt-1009197 |

  @regression
  Scenario Outline: (6) Verify clicking on 'MATCH IT WITH' product takes me to the correct page

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And refresh the page
    When I go to 'product_Matchitwith' product page
    And refresh the page
    When I go to 'instock_ThirdProduct' product page
    When I click on the 'MATCH IT WITH' tab in the 'YOUR MATCHES' section
    And I click the '<product>' product link in the 'MATCH IT WITH' tab of the PDP
    Then I am taken to the correct page '<target_url>'

    Examples:
      | country           | language | currency         | product                              | target_url                                                                                                                                                          |
      | Australia         |          | US Dollar ($)    | Short-sleeved striped linen shirt    | /au/products/120%25-Lino-Short-sleeved-striped-linen-shirt-1192544                                                                                                  |
      | Korea Republic of | 한국어      | US Dollar ($)    | 쇼트 슬리브 스트라이프 리넨 셔츠                   | /kr/products/120%25-Lino-%EC%87%BC%ED%8A%B8-%EC%8A%AC%EB%A6%AC%EB%B8%8C-%EC%8A%A4%ED%8A%B8%EB%9D%BC%EC%9D%B4%ED%94%84-%EB%A6%AC%EB%84%A8-%EC%85%94%EC%B8%A0-1192544 |
      | France            | Français | US Dollar ($)    | Chemise manches courtes rayée en lin | /fr/products/120%25-Lino-Chemise-manches-courtes-ray%C3%A9e-en-lin-1192544                                                                                          |
      | Japan             | 日本語      | Japanese Yen (¥) | Short-sleeved striped linen shirt    | /jp/products/120%25-Lino-Short-sleeved-striped-linen-shirt-1192544                                                                                                  |

  @regression
  Scenario Outline: (7) Verify 'MATCH IT WITH' products are consistently displayed in both the 'YOUR MATCHES' section and the 'Product Details' section of the PDP
    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And refresh the page
    When I go to 'product_Matchitwith' product page
    And refresh the page
    When I go to 'instock_ThirdProduct' product page
    When I click on the 'MATCH IT WITH' tab in the 'YOUR MATCHES' section
    Then the first product in the 'MATCH IT WITH' tab of the 'YOUR MATCHES' section is '<product_description>'
    When I click the '<product_description>' product link in the 'MATCH IT WITH' tab of the PDP
    Then 'DESCRIPTION' tab of the Product Details section of the PDP contains '<product_description>'


    Examples:
      | country           | language | currency         | product_description                  |
    #  | Hong Kong         |          | US Dollar ($)    | Short-sleeved striped linen shirt    |
      | Korea Republic of | 한국어      | US Dollar ($)    | 쇼트 슬리브 스트라이프 리넨 셔츠                   |
      | France            | Français | US Dollar ($)    | Chemise manches courtes rayée en lin |
      | Japan             | 日本語      | Japanese Yen (¥) | Short-sleeved striped linen shirt    |

