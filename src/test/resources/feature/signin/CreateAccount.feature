#Author: Mona Agrawal

@regression @signIn @createAccount @teamSecure
Feature: Create an account
  In order to get the benefits of a registered account
  A customer using the website
  Should be able to create an account

  @done @essential
  Scenario Outline: (1) Verify 'Create an account' text on signin page

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I navigate to '/login'
    Then I should see title as '<title>'

    Examples:
      | country           | language | currency | title             |
      | United Kingdom    |          |          | CREATE AN ACCOUNT |
      | France            | Français |          | Créer un compte   |
      | France            | English  |          | CREATE AN ACCOUNT |
      | Korea Republic of | 한국어      |          | 회원 가입             |
      | Korea Republic of | English  |          | CREATE AN ACCOUNT |
      | Japan             | 日本語      |          | アカウント新規作成         |
      | Japan             | English  |          | CREATE AN ACCOUNT |


  @done @essential @failedTSEReg474
  Scenario Outline: (2) Input fields should have the correct placeholder text

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I navigate to '/login'
    Then the fields in the 'CREATE AN ACCOUNT' section should contain the correct placeholder text '<title_placeholdertext>' '<first_name_placeholdertext>' '<last_name_placeholdertext>' '<email_placeholdertext>' '<password_placeholdertext>' '<confirm_password_placeholdertext>' '<country_placeholdertext>' '<phone_number_placeholdertext>'

    Examples:
      | country           | language | currency | title_placeholdertext | first_name_placeholdertext | last_name_placeholdertext | email_placeholdertext | password_placeholdertext                                    | confirm_password_placeholdertext | country_placeholdertext | phone_number_placeholdertext |
      | United Kingdom    |          |          | Title                 | First name*                | Last name*                | Email*                | Password* (must contain letters and numbers)                | Confirm Password*                | United Kingdom          | Mobile Number                |
      | France            | Français |          | Civilité              | Prénom*                    | Nom*                      | E-mail*               | Mot de passe* (doit être composé de lettres et de chiffres) | Confirmer le mot de passe*       | France                  | Numéro de téléphone*         |
      | France            | English  |          | Title                 | First name*                | Last name*                | Email*                | Password* (must contain letters and numbers)                | Confirm Password*                | France                  | Mobile Number                |
      | Korea Republic of | 한국어      |          | 호칭                    | 이름*                        | 성*                        | 이메일*                  | 비밀번호*(대문자와 숫자 포함, 최소 6자 이상)                                 | 비밀번호 확인*                         | 대한민국                    | 전화번호*                        |
      | Korea Republic of | English  |          | Title                 | First name*                | Last name*                | Email*                | Password* (must contain letters and numbers)                | Confirm Password*                | Korea Republic of       | Mobile Number                |
      | Japan             | 日本語      |          | 敬称*                   | 名*                         | 姓*                        | メールアドレス*              | パスワード*（大文字と数字を含めた6文字以上）                                     | パスワードを再入力*                       | 日本                      | 携帯電話番号*                      |
      | Japan             | English  |          | Title                 | First name*                | Last name*                | Email*                | Password* (must contain letters and numbers)                | Confirm Password*                | Japan                   | Mobile Number                |


  @done @essential
  Scenario Outline: (3) Clicking Create An Account without entering any details results in error messages to all form fields

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I navigate to '/login'
    And I click Create An Account
    Then I should see following the error messages '<title_error_message>' '<first_name_error_message>' '<last_name_error_message>' '<email_error_message>' '<password_error_message>' '<confirm_password_error_message>' '<phone_number_error_message>'

    Examples:
      | country           | language | currency | title_error_message                  | first_name_error_message     | last_name_error_message   | email_error_message                      | password_error_message                                                                                    | confirm_password_error_message        | phone_number_error_message                |
      | United Kingdom    |          |          | Please select your title             | Please enter your first name | Please enter your surname | The Email field is invalid               | Your password must be at least 6 characters long and contain at least one uppercase letter and a number   | Please confirm your password          | Please enter your mobile phone number     |
      | France            | Français |          | Veuillez sélectionner votre civilité | Veuillez saisir votre prénom | Veuillez saisir votre nom | Le champ de l’e-mail contient une erreur | Votre mot de passe doit comprendre au moins 6 caractères dont au moins une lettre majuscule et un chiffre | Veuillez confirmer votre mot de passe | Veuillez saisir votre numéro de téléphone |
      | France            | English  |          | Please select your title             | Please enter your first name | Please enter your surname | The Email field is invalid               | Your password must be at least 6 characters long and contain at least one uppercase letter and a number   | Please confirm your password          | Please enter your mobile phone number     |
      | Korea Republic of | 한국어      |          | 호칭을 선택해 주세요.                         | 이름을 입력해 주세요.                 | 성을 입력해 주세요.               | 유효한 이메일 주소를 입력해 주세요.                     | 비밀번호는 최소 대문자 하나와 숫자 하나를 포함해 최소 6자 이상이어야 합니다.                                                              | 비밀번호를 확인해 주세요.                        | 전화번호를 입력해 주세요.                            |
      | Korea Republic of | English  |          | Please select your title             | Please enter your first name | Please enter your surname | The Email field is invalid               | Your password must be at least 6 characters long and contain at least one uppercase letter and a number   | Please confirm your password          | Please enter your mobile phone number     |
      | Japan             | 日本語      |          | 敬称を選択してください                          | 名を入力してください                   | 姓を入力してください                | メールアドレスが正しくありません                         | パスワードは6文字以上で、大文字と数字それぞれひとつ以上を含んでいなければなりません。                                                               | パスワードを再入力してください                       | 携帯電話番号を入力してください                           |
      | Japan             | English  |          | Please select your title             | Please enter your first name | Please enter your surname | The Email field is invalid               | Your password must be at least 6 characters long and contain at least one uppercase letter and a number   | Please confirm your password          | Please enter your mobile phone number     |


  @done @essential
  Scenario Outline: (4) Account should not be created and an error message displayed if entered email already exist

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I navigate to '/login'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter existing email in the Email field
    And I enter existing password in the Password field
    And I enter existing password in the Confirm Password field
    And I enter '<Mobile_number>' in the Phone Number field
    And I click Create An Account
    Then account should not be created
    And I should see the '<error_message>' Error Message

    Examples:
      | country           | language | currency | title | first_name | last_name | password             | Mobile_number | error_message                                                   |
      | United Kingdom    |          |          | Mr    | matches    | autotest  | 242Eyvg8pJwpWcBxE7wz | 5522244788    | An account already exists for this email address.               |
      | France            | Français |          | Mme   | matches    | autotest  | 242Eyvg8pJwpWcBxE7wz | 5522244788    | Nous avons déjà un compte enregistré pour cette adresse e-mail. |
      | Korea Republic of | 한국어      |          | Mrs   | matches    | autotest  | 242Eyvg8pJwpWcBxE7wz | 5522244788    | 본 이메일 주소를 가진 회원 계정이 존재합니다.                                      |
      | Japan             | 日本語      |          | Mrs   | matches    | autotest  | 242Eyvg8pJwpWcBxE7wz | 5522244788    | このメールアドレスは別のアカウントで使用されています。                                     |

  @done @detail @essential @latinChars
  Scenario Outline: (5) Account should not be created and an error message displayed when firstName or lastName contains non-latin characters

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I navigate to '/login'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter existing email in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<Mobile_number>' in the Phone Number field
    And I click Create An Account
    And I should see the '<error_message>' validation Error Message to not enter non-latin characters

    Examples:
      | country           | language | currency | title | first_name | last_name | password   | Mobile_number | error_message                             |
      | United Kingdom    |          |          | Mr    | 테스트        | 테스트       | Matches123 | 5522244788    | Please use Latin characters only.         |
      | France            | English  |          | Mr    | 테스트        | 테스트       | Matches123 | 5522244788    | Please use Latin characters only.         |
      | France            | Français |          | Mme   | 테스트        | 테스트       | Matches123 | 5522244788    | Merci de n'utiliser que l'alphabet latin. |
      | Korea Republic of | English  |          | Mrs   | 테스트        | 테스트       | Matches123 | 5522244788    | Please use Latin characters only.         |
      | Korea Republic of | 한국어      |          | Mrs   | 테스트        | 테스트       | Matches123 | 5522244788    | 영문만 입력 가능합니다.                             |
      | Japan             | English  |          | Mrs   | 테스트        | 테스트       | Matches123 | 5522244788    | Please use Latin characters only.         |
      | Japan             | 日本語      |          | Mrs   | 테스트        | 테스트       | Matches123 | 5522244788    | 半角英数字で入力してください                            |
