#Author: Himaja D
@regression
Feature: Designers page
  Validation of email signup for designer updates
  A user should be able to submit signup form and able to switch to other gender page for same designer


  Scenario Outline: 1.User should be able to switch to different gender for same designer.

    Given I am on the website with '<country>' '<language>' ''
    And I navigate to 'designers_page'
    When I click on the other gender of that designer's page
    Then I should switch to the other gender of that designer's page

    Examples:
      | country           | language |
      | United Kingdom    | English  |
      | France            | Français |
      | Korea Republic of | 한국어     |
      | Japan             | 日本語    |

   Scenario Outline: 2. User should be able to open and close the email signup for designer updates pop-up

     Given I am on the website with '<country>' '<language>' ''
     And I navigate to 'designers_page'
     When I click on sign up for designer updates
     Then I switch to iFrame
     And I submit the designer update email form
     Then I should see the following error message '<error>' on designer pop up
     And I should be able to close signup designer updates pop-up


     Examples:
       | country           | language | error |
       | United Kingdom    | English  |Please include a valid email address Please select your title Please include your first name Please include your last name Please select your country|
       | France            | Français | Veuillez renseigner une adresse e-mail valide Veuillez sélectionner votre civilité Veuillez renseigner votre prénom Veuillez renseigner votre nom de famille Veuillez sélectionner votre pays      |
       | Korea Republic of | 한국어     | 유효한 이메일 주소를 입력해 주세요. 호칭을 선택해 주세요. 이름을 입력해 주세요. 성을 입력해 주세요. 국가를 선택해 주세요       |
       | Japan             | 日本語    | 有効なメールアドレスを入力してください 敬称を選択してください 姓を入力してください 名を入力してください 国・地域を選択してください        |


  Scenario Outline: 3. User should be able to submit the email signup for designer updates

    Given I am on the website with '<country>' '<language>' ''
    And I navigate to 'designers_page'
    When I click on sign up for designer updates
    Then I switch to iFrame
    And I enter '<email>' in email address field
    And I select '<title>' from title drop down
    And I enter '<first_name>' in first name field
    And I enter '<last_name>' in last name field
    And I select '<country>' from country drop down
    And I submit the designer update email form
    Then I see '<thank_you>' and '<msg>' message on the pop-up
    And I should be able to close signup designer updates pop-up


    Examples:
      | country | language | email | title | first_name | last_name|thank_you| msg|
      | United Kingdom    | English  | abc@gmail.com|Mr | ABC | DEF | THANK YOU | Your email preferences have been updated.|
      | France            | Français | abc@gmail.com|Mr | ABC | DEF |  THANK YOU    |   Vos préférences e-mail ont été mises à jour.|
      | Korea Republic Of | 한국어      |abc@gmail.com|Mr | ABC | DEF | 감사합니다     |      고객님의 이메일 설정이 변경되었습니다.            |
      | Japan             | 日本語    | abc@gmail.com|Mr | ABC | DEF |  THANK YOU    |  ニュースレター配信設定が更新されました。   |
