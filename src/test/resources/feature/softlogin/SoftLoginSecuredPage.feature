#Author: Mona Agrawal

@regression @order @placeOrder @teamSecure
Feature:SoftLoginSecuredPage
  In order to stay signed in
  A customer logged into the website
  Should be able to access all unsecured pages even after session expired

  
  @regression
  Scenario Outline:(1) New hard login page should appear, if customer lost session while navigating to secured pages.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials and select 'Stay signed in' checkbox
    And I navigate to the page '<url>'
    When I delete the JSessionId from cookies and reload page
    Then I should be navigated to new hard login page '<loginURL>'
    When I enter password and click on login button
    Then I should be navigated to '<url>'

    Examples:
      | country           | language | currency | url                  |loginURL|
      | United Kingdom    |          |          | /account/addressbook |/login  |
      | United Kingdom    |          |          | /account/mycards     |/login  |
      | France            | Français |          | /fr/account/addressbook |/login  |
      | France            | Français |          |/fr/account/mycards      |/login  |
      | France            | English  |          | /en-fr/account/addressbook |/login  |
      | France            | English  |          |/en-fr/account/mycards      |/login  |
      | Korea Republic of | 한국어     |          |/kr/account/addressbook   |/login  |
      | Korea Republic of | 한국어     |          |/kr/account/mycards      |/login  |
      | Korea Republic of | English  |          |/en-kr/account/addressbook   |/login  |
      | Korea Republic of | English  |          |/en-kr/account/mycards      |/login  |
      | Japan             | 日本語    |          |/jp/account/addressbook   |/login  |
      | Japan             | 日本語    |          | /jp/account/mycards     |/login  |
      | Japan             | English  |          |/en-jp/account/addressbook   |/login  |
      | Japan             | English  |          | /en-jp/account/mycards     |/login  |



  Scenario Outline:(2) Customer should able to perform wish list actions, even after losing session.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials and select 'Stay signed in' checkbox
    And I go to 'instock' product page
    And I select the '<size>' on the PDP
    When I click on the 'ADD TO WISHLIST' button on the PDP
    And I navigate to the page '<url>'
    When I delete the JSessionId from cookies and reload page
    Then I should be navigated to '<url>'
    When clicking on the 'ADD TO BAG' button for the first product causes the minibag rollover to appear with '<add_to_bag>' text


    Examples:
      | country           | language | currency | url                | size | first_change          | add_to_bag        |
      | United Kingdom    |          |          | /account/wishlist  | S    | ADDED TO WISHLIST     | ADD TO BAG        |
      | France            | Français |          | /fr/account/wishlist  | S    | AJOUTER À LA WISHLIST | AJOUTER AU PANIER |
      | France            | English  |          | /en-fr/account/wishlist | S    | ADDED TO WISHLIST     | ADD TO BAG        |
      | Korea Republic of | 한국어     |          | /kr/account/wishlist  | S    | 위시리스트에 담았습니다.    | 쇼핑백 담기          |
      | Korea Republic of | English  |          | /en-kr/account/wishlist  | S    | ADDED TO WISHLIST     | ADD TO BAG        |
      | Japan             | 日本語    |          | /jp/account/wishlist  | S    | 欲しいものリストに登録   | ショッピングバッグに追加      |
      | Japan             | English  |          | /en-jp/account/wishlist  | S    | ADDED TO WISHLIST     | ADD TO BAG        |


  @regression
  Scenario Outline:(3) Customer should able to place order successfully, if lost session while proceeding to checkout.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials and select 'Stay signed in' checkbox
    And I add products to my cart
    And I navigate to the page '<url>'
    When I delete the JSessionId from cookies and reload page
    Then I should be navigated to '<url>'
    When I check out
    Then I should be navigated to new hard login page '<loginURL>'
    When I enter password and click on login button
    Then I should be navigated to '<url1>'
    When I select the saved '<card_type>' credit card
    And I place the order
    Then I should see the order confirmation page


    Examples:
      | country           | language | currency | url           | loginURL          | url1                     | card_type |
      | United Kingdom    |          |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | Visa      |
      | France            | Français |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | Visa      |
      | Korea Republic of | 한국어     |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | Visa      |
      | Japan             | 日本語    |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | Visa      |


  @regression
  Scenario Outline:(4)  Customer should able to place order successfully, if lost session while proceeding to guest checkout.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I check out
    Then I should be navigated to '<url1>'
    When I log in with the following credentials and select 'Stay signed in' checkbox
    Then I should be navigated to '<url2>'
    When I navigate to the page '<url>'
    And I delete the JSessionId from cookies and reload page
    Then I should be navigated to '<url>'
    When I check out
    Then I should be navigated to new hard login page '<loginURL>'
    When I enter password and click on login button
    Then I should be navigated to '<url2>'
    When I select the saved '<card_type>' credit card
    And I place the order
    Then I should see the order confirmation page



    Examples:
      | country           | language | currency | url           | url1              | url2                     | loginURL | card_type |
      | United Kingdom    |          |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | /checkout/sign-in   | Visa      |
      | France            | Français |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | /checkout/sign-in   | Visa      |
      | France            | English  |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | /checkout/sign-in   | Visa      |
      | Korea Republic of | 한국어     |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | /checkout/sign-in   | Visa      |
      | Korea Republic of | English  |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | /checkout/sign-in   | Visa      |
      | Japan             |  日本語   |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | /checkout/sign-in   | Visa      |
      | Japan             | English  |          | /shopping-bag | /checkout/sign-in | /checkout/review-and-pay | /checkout/sign-in   | Visa      |


    @regression
    Scenario Outline:(5) 'Remember Me' cookie should be deleted, if customer sign-out from the website.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials and select 'Stay signed in' checkbox
    And 'Remember Me' cookie exist after sign-in
    When I sign out when county '<country>' and language '<language>'
    Then 'Remember Me' cookie shouldn't exist


    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |
      | France            | Français |          |
      | France            | English  |          |
      | Korea Republic of | 한국어     |          |
      | Korea Republic of | English  |          |
      | Japan             | 日本語    |          |
      | Japan             | English  |          |

  @regression
  Scenario Outline:(6) Customer should able to view Order history, even after losing session.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials and select 'Stay signed in' checkbox
    And I navigate to the page '<url>'
    When I delete the JSessionId from cookies and reload page
    And I should be navigated to '<url>'
    And I navigate to the page '<url1>'
    Then I should be navigated to '<url1>'

    Examples:
      | country           | language | currency | url               | url1                  |
      | United Kingdom    |          |          | /account/wishlist | /account/orderhistory |
      | France            | Français |          | /account/wishlist | /account/orderhistory |
      | France            | English  |          | /account/wishlist | /account/orderhistory |
      | Korea Republic of | 한국어     |          | /account/wishlist | /account/orderhistory |
      | Korea Republic of | English  |          | /account/wishlist | /account/orderhistory |
      | Japan             | 日本語    |          | /account/wishlist | /account/orderhistory |
      | Japan             | English  |          | /account/wishlist | /account/orderhistory |


  @regression
  Scenario Outline: (7) Customer should be navigated to Login page on click on 'Sign in to a different Account' link and able to login with valid credential

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials and select 'Stay signed in' checkbox
    And I navigate to the page '<page_url>'
    When I delete the JSessionId from cookies and reload page
    Then I should be navigated to '<loginURL1>'
    When I click on ‘Signed into different account’ link
    Then I should be navigated to '<loginURL2>'
    When I log in with the following credentials
    Then I should be signed in


    Examples:
      | country           | language | currency | loginURL1 | page_url                | loginURL2      |
      | United Kingdom    |          |          | /login    | /account/contactdetails | /login?cta=nya |
      | France            | Français |          | /login    | /account/contactdetails | /login?cta=nya |
      | France            | English  |          | /login    | /account/contactdetails | /login?cta=nya |
      | Korea Republic of | 한국어     |          | /login    | /account/contactdetails | /login?cta=nya |
      | Korea Republic of | English  |          | /login    | /account/contactdetails | /login?cta=nya |
      | Japan             | 日本語    |          | /login    | /account/contactdetails | /login?cta=nya |
      | Japan             | English  |          | /login    | /account/contactdetails | /login?cta=nya |

  @regression
  Scenario Outline: (8) Customer should be navigated to Login page on click on 'Create a New Account' link and account should be created successfully

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials and select 'Stay signed in' checkbox
    And I navigate to the page '<page_url>'
    When I delete the JSessionId from cookies and reload page
    Then I should be navigated to '<loginURL1>'
    When I click on ‘Create a new account’ link
    Then I should be navigated to '<loginURL2>'
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter new email '<first_name>' in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<Mobile_number>' in the Phone Number field
    And I select the promotional email checkbox
    And I click Create An Account
    And I should be redirected to the homepage


    Examples:
      | country           | language | currency | loginURL1 | page_url                | loginURL2      | title | first_name | last_name | password   | Mobile_number |
      | United Kingdom    |          |          | /login    | /account/contactdetails | /login?cta=nya | Mr    | matches    | autotest  | Matches123 | 5522244788    |
      | France            | Français |          | /login    | /account/contactdetails | /login?cta=nya | Mme    | matches    | autotest  | Matches123 | 5522244788    |
      | France            | English  |          | /login    | /account/contactdetails | /login?cta=nya | Mr   | matches    | autotest  | Matches123 | 5522244788    |
      | Korea Republic of | 한국어     |          | /login    | /account/contactdetails | /login?cta=nya | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Korea Republic of | English  |          | /login    | /account/contactdetails | /login?cta=nya | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Japan             | 日本語    |          | /login    | /account/contactdetails | /login?cta=nya | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Japan             | English  |          | /login    | /account/contactdetails | /login?cta=nya | Mrs   | matches    | autotest  | Matches123 | 5522244788    |


  @regression
  Scenario Outline: (9) Customer should be navigated to Login page on click on 'Checkout as a guest' link and able to place order as a guest user.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials and select 'Stay signed in' checkbox
    And I add products to my cart
    When I navigate to the page '<url>'
    And I delete the JSessionId from cookies and reload page
    Then I should be navigated to '<url>'
    When I check out
    Then I should be navigated to '<loginURL>'
    When I click on ‘Checkout as a guest’ link
    Then I should be navigated to '<loginURL2>'
    When I login as a guest user '<first_name>'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<phone_number>' in the Phone Number field
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    Then I should see the order confirmation page


    Examples:
      | country           | language | currency | url           | loginURL2                 | loginURL          | card_type | card_number      | card_name  | expiry_month | expiry_year | security_code | title | first_name | last_name | phone_number | address_saved |
      | United Kingdom    |          |          | /shopping-bag | /checkout/sign-in?cta=nya | /checkout/sign-in | Visa      | 4444333322221111 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
      | France            | Français |          | /shopping-bag | /checkout/sign-in?cta=nya | /checkout/sign-in | Visa      | 4444333322221111 | ORDER TEST | 01           | 2022        | 123           | Mme   | GuestUser  | Test      | 0765432198   | ADRESSE ENREGISTRÉE |
      | France            | English  |          | /shopping-bag | /checkout/sign-in?cta=nya | /checkout/sign-in | Visa      | 4444333322221111 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
      | Korea Republic of | 한국어     |          | /shopping-bag | /checkout/sign-in?cta=nya | /checkout/sign-in | 비자      | 4444333322221111 | ORDER TEST | 01           | 2022        | 123           | Mrs   | GuestUser  | Test      | 0765432198   | 주소 저장 완료 |
      | Korea Republic of | English  |          | /shopping-bag | /checkout/sign-in?cta=nya | /checkout/sign-in | Visa      | 4444333322221111 | ORDER TEST | 01           | 2022        | 123           | Mrs   | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
      | Japan             | 日本語    |          | /shopping-bag | /checkout/sign-in?cta=nya | /checkout/sign-in | Visa      | 4444333322221111 | ORDER TEST | 01           | 2022        | 123           | Mrs   | GuestUser  | Test      | 0765432198   | 住所が登録されました|
      | Japan             | English  |          | /shopping-bag | /checkout/sign-in?cta=nya | /checkout/sign-in | Visa      | 4444333322221111 | ORDER TEST | 01           | 2022        | 123           | Mrs   | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |


