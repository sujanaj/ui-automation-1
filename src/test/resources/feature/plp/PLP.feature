#Author: Dilbag Singh
Feature: PLP

  @testrun
  Scenario Outline: 1. verify breadcrumb(Men & women) is navigating to correct page for clothing, shoes, bags, accessories

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to '<gender>' and '<url>'
    Then I should see breadcrumb links are clickable and navigating to the correct page '<url>'

    Examples:
      | country           | language | currency | gender | url            |
      | United Kingdom    |          |          | /womens  | /shop/clothing |
      | United Kingdom    |          |          | /womens  | /shop/shoes |
      | United Kingdom    |          |          | /womens  | /shop/bags |
      | United Kingdom    |          |          | /womens  | /shop/accessories |
      | France            | Français |          | /womens  | /shop/clothing |
      | France            | Français |          | /womens  | /shop/shoes |
      | France            | Français |          | /womens  | /shop/bags |
      | France            | Français |          | /womens  | /shop/accessories |
      | Korea Republic of | 한국어      |          | /womens  | /shop/clothing |
      | Korea Republic of | 한국어      |          | /womens  | /shop/shoes |
      | Korea Republic of | 한국어      |          | /womens  | /shop/bags |
      | Korea Republic of | 한국어      |          | /womens  | /shop/accessories |
      | Japan             | 日本語      |          | /womens  | /shop/clothing |
      | Japan             | 日本語      |          | /womens  | /shop/shoes |
      | Japan             | 日本語      |          | /womens  | /shop/bags |
      | Japan             | 日本語      |          | /womens  | /shop/accessories |


    Scenario Outline:2. Number of results on PLP must be greater than 0

      Given I am on the website with '<country>' '<language>' '<currency>'
      When I go to '<gender>' and '<url>'
      Then I should see the number of results more than 0

      Examples:
        | country           | language | currency | gender | url            |
        | United Kingdom    |          |          | /womens  | /shop/clothing |
        | France            |  Français        |          | /mens   | /shop/bags        |
        | Korea Republic of |    한국어      |          | /womens | /shop/shoes       |
        | Japan             |   日本語       |          | /mens   | /shop/accessories |


    Scenario Outline: 3. Title of the page is the selected category
        Given I am on the website with '<country>' '<language>' '<currency>'
        When I go to '<gender>' and '<url>'
        And I select '<category>' in the left hand facet from category list
        Then I see the title of the page is the selected '<category>'

      Examples:
          | country           | language | currency | gender | url            | category|
          | United Kingdom    |          |          | /womens  | /shop/clothing | Dresses|
          | France            |  Français        |          | /mens   | /shop/bags        |Sacs à dos|
          | Korea Republic of |    한국어      |          | /womens | /shop/shoes       |로퍼|
          | Japan             |   日本語       |          | /mens   | /shop/accessories |腕時計|


     Scenario Outline:  4. Select any level 3 category, selected level-3 category should be in the breadcrumb and has to be the title
       Given I am on the website with '<country>' '<language>' '<currency>'
       When I go to '<gender>' and '<url>'
       And I select '<category>' in the left hand facet from category list
       And I select '<sub_category>' in the left hand facet from category list
       Then I should see '<sub_category>' in the breadcrumbs
        And I see the title of the page is the selected '<sub_category>'

       Examples:
         | country           | language | currency | gender | url            | category| sub_category|
         | United Kingdom    |          |          | /womens  | /shop/clothing | Dresses| Evening Dresses|
         | France            |  Français        |          | /womens   | /shop/clothing       |Robes| Robes de soirée  |
         | Korea Republic of |    한국어      |          | /womens | /shop/clothing       |드레스|      파티 드레스         |
         | Japan             |   日本語       |          | /womens   | /shop/clothing  |ワンピース＆ドレス|イブニングドレス          |


  Scenario Outline:  5. Select any 2 level 3 categories, level-2 category must be the title
    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to '<gender>' and '<url>'
    And I select '<category>' in the left hand facet from category list
    And I select '<sub_category_1>' in the left hand facet from category list
    And I select '<sub_category_2>' in the left hand facet from category list
    Then I see the title of the page is the selected '<category>'

    Examples:
      | country           | language | currency | gender | url            | category| sub_category_1| sub_category_2|
      | United Kingdom    |          |          | /womens  | /shop/clothing | Dresses| Evening Dresses| Embellished Dresses|
      | France            |  Français        |          | /womens   | /shop/clothing       |Robes| Robes de soirée  |  Robes ornementées      |
      | Korea Republic of |    한국어      |          | /womens | /shop/clothing       |드레스|      파티 드레스         |  시퀸 드레스         |
      | Japan             |   日本語       |          | /womens   | /shop/clothing  |ワンピース＆ドレス|イブニングドレス          | デコラティブドレス           |

    Scenario Outline: : 6. Back to top button on right hand side when scrolled down
      Given I am on the website with '<country>' '<language>' '<currency>'
      When I go to '<gender>' and '<url>'
      And I scroll down to a point on the page
      Then I should see Back To Top button on right hand side of the page
      And I click on the Back To top button
      Then I should go to the top of the page

      Examples:
        | country           | language | currency | gender | url            |
        | United Kingdom    |          |          | /womens  | /shop/clothing |
        | France            |  Français        |          | /mens   | /shop/bags        |
        | Korea Republic of |    한국어      |          | /womens | /shop/shoes       |
        | Japan             |   日本語       |          | /mens   | /shop/accessories |