Feature: PLP - Images related scenarios will be captured here like:
  Slider on each image
 Designer Name, Product Description and price for a product on PLP


  Scenario Outline: 1. Designer Name, product description and price for products on PLP is not empty.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to '<gender>' and '<url>'
    Then I should see designer Name, product description and price for products on PLP

    Examples:
      | country           | language | currency | gender  | url               |
      | United Kingdom    |          |          | /womens | /shop/clothing    |
      | France            |          |          | /mens   | /shop/bags        |
      | Korea Republic of |          |          | /womens | /shop/shoes       |
      | Japan             |          |          | /mens   | /shop/accessories |


  Scenario Outline: 2. Images must be scrollable with a slider for each product on PLP.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to '<gender>' and '<url>'
    And I click on 'Next' arrow on first product
    Then Image is scrolled on clicking 'Next'
    And I click on 'Previous' arrow on first product
    Then Image is scrolled on clicking 'Previous'

    Examples:
      | country           | language | currency | gender  | url               |
      | United Kingdom    |          |          | /womens | /shop/clothing    |
      | France            | Français |          | /mens   | /shop/bags        |
      | Korea Republic of | 한국어      |          | /womens | /shop/shoes       |
      | Japan             | 日本語      |          | /mens   | /shop/accessories |


  Scenario Outline: 3. Last Image must not have 'Next' arrow and first image should not have 'Previous'

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to '<gender>' and '<url>'
    Then I should not see 'Previous' slider on first image
    And I scroll to the last image of first product
    Then I should not see 'Next' slider on last image

    Examples:
      | country           | language | currency | gender  | url               |
      | United Kingdom    |          |          | /womens | /shop/clothing    |
      | France            | Français |          | /mens   | /shop/bags        |
      | Korea Republic of | 한국어      |          | /womens | /shop/shoes       |
      | Japan             | 日本語      |          | /mens   | /shop/accessories |


    Scenario Outline: 4.  Mousehover on image shows the size guide of the product on PLP

      Given I am on the website with '<country>' '<language>' '<currency>'
      When I go to '<gender>' and '<url>'
      And I mousehover on image on PLP
      Then I see the size guide on product image

      Examples:
        | country           | language | currency | gender  | url               |
        | United Kingdom    |          |          | /womens | /shop/clothing    |
        | France            | Français |          | /mens   | /shop/bags        |
        | Korea Republic of | 한국어      |          | /womens | /shop/shoes       |
        | Japan             | 日本語      |          | /mens   | /shop/accessories |
