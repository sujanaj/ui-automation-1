#Author: Mahwish Ahmed
@regression @wishlist @viewWishlist @teamProduct
Feature: Wishlist page
  As a customer
  I want to view my Wishlist
  So that I can view the products I have added to my Wishlist page

  This feature tests the look and feel of the Wishlist page

	###### SMOKE Test - Please donot remove #####

  @smoke
  Scenario Outline: (smoke-6) Remove product from Wishlist

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Wishlist page
    And I add items to my wishlist
      | item          |
      | 1009197000004 |
      | 1009197000005 |
    When I click on the link '<remove_link>' for the first product in my Wishlist
    Then the first product is removed from my Wishlist
    Then clicking on the Wishlist 'CONTINUE SHOPPING' button should take me to the PreHome Page

    Examples:
      | country           | language | currency | remove_link               |
      | United Kingdom    | English  |          | Remove item from wishlist |
      | France            | Français |          | Supprimer de la wishlist  |
      | France            | English  |          | Remove item from wishlist |
      | Korea Republic of | 한국어      |          | 위시리스트에서 상품 삭제       |
      | Korea Republic of | English  |          | Remove item from wishlist |
      | Japan             | 日本語      |        | 削除                      |
      | Japan             | English  |          | Remove item from wishlist |



  ##################


	# For LOAD MORE TO WORK , put the maximum items per page on wishlist as 3.

	# Refer a friend do not work on test environments, hence commented.
  @regression
  Scenario Outline: (1) Checking the left side navigation links the Wishlist page for a non-loyalty customer

    Given I am on the website with '<country>' '<language>' '<currency>'
		#And I am signed in to the website as a customer
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Wishlist page
    Then every link on the Wishlist left side should take me to the correct page
      | link     | url                        | page_title |
      | <link_1> | /account/contactdetails    | <link_1>   |
      | <link_2> | /account/addressbook       | <link_2>   |
      | <link_3> | /account/orderhistory      | <link_3>   |
      | <link_4> | /account/wishlist          | <link_4>   |
      | <link_5> | /account/managepreferences | <link_5>   |
      | <link_6> | /account/mycards           | <link_6>   |
      | <link_7> | /account/credits           | <link_7>   |
#	  | <link_8> | /account/referafriend | <refer_a_friend_title>    |

		Examples:
		|country | language|currency| link_1|link_2|link_3|link_4|link_5|link_6|link_7| link_8| refer_a_friend_title|
#		| United Kingdom    | English | |MY DETAILS| ADDRESS BOOK| ORDER HISTORY| WISHLIST| MANAGE PREFERENCES| PAYMENT CARDS| MY CREDITS| REFER A FRIEND|CHOOSE HOW TO SHARE WITH YOUR FRIENDS|
#		| Japan            | 日本語|       |連絡先情報 | アドレス帳 | 注文履歴 | 欲しいものリスト | ニュースレター配信設定  | 支払いカード |マイクレジット  | ご友人紹介プログラム    | MATCHESFASHION.COM    |
#		| France           | Français|  |COORDONNÉES| CARNET D’ADRESSES| MES COMMANDES| WISHLIST| MES PRÉFÉRENCES| CARTES DE PAIEMENT|MES AVOIRS| PARRAINEZ UN AMI| COMMENT PARRAINER VOS PROCHES|
#		|Korea Republic of | 한국어  |     |연락처 정보 | 주소록 | 주문 내역        | 위시리스트   | 마케팅 수신 설정 | 결제 카드             | 내 크레딧    | 친구 추천           | MATCHESFASHION.COM           |

#	@Dilbag
#	Scenario Outline: (2) Empty Wishlist page that displays a message
#
#		Given I am on the website with '<country>' '<language>' '<currency>'
#		And I navigate to '/login'
##		And I log in with following user which has no wishlist
##			| email | password|
##			|matches.whitelist@gmail.com| Pass1234 |
#		And I log in with the following credentials
#		And I navigate to the Wishlist page
#		And I remove items from my wishlist
#		Then clicking on the Wishlist 'CONTINUE SHOPPING' button should take me to the PreHome Page
#	Examples:
#		|country | language|currency|
#		| United Kingdom    | English ||
#		| Japan            | 日本語|      |
#		| France           | Français| |
#		|Korea Republic of | 한국어  |    |


  @regression
  Scenario Outline: (3) Checking the column titles on the Wishlist page

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
		#And I am signed in to the website as a customer
    And I log in with the following credentials
    And I navigate to the Wishlist page
    And I add items to my wishlist
      | item          |
      | 1009197000003 |
    Then the Wishlist page should have the correct column names '<first_column>' '<second_column>' '<third_column>' '<fourth_column>' '<fifth_column>'
    Then I remove items from my wishlist

    Examples:
      | country           | language | currency | first_column | second_column | third_column | fourth_column | fifth_column  |
      | United Kingdom    |          |          | Description  | Colour        | Size         | Unit price    | Availability: |
      | France            | Français |          | Description  | Couleur       | Taille       | Prix unitaire | DisponibilitE |
      | France            | English  |          | Description  | Colour        | Size         | Unit price    | Availability: |
      | Japan             | 日本語      |          | -            | カラー           | サイズ          | 単価            | 在庫状況          |
      | Japan             | English  |          | Description  | Colour        | Size         | Unit price    | Availability: |
      | Korea Republic of | 한국어      |          | 상품 설명        | 색상            | 사이즈          | 수량별 가격        | 구매 가능 여부:     |
      | Korea Republic of | English  |          | Description  | Colour        | Size         | Unit price    | Availability: |

  @done @detailed @removeProductsFromWishlist @regression
  Scenario Outline: (4) Selecting and unselecting all products listed in the Wishlist

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
		#And I am signed in to the website as a customer
    And I log in with the following credentials
    And I navigate to the Wishlist page
    And I add items to my wishlist
      | item          |
      | 1009197000004 |
      | 1009197000005 |

    When I click on 'Select/Clear All' link on the Wishlist page
    Then the 'Select/Clear All' link text changes to '<clearAll_link_text>'
    And all the visible products listed in my wishlist are selected
    And the 'manage selected' drop down list is enabled

    When I click on 'Select/Clear All' link on the Wishlist page
    Then the 'Select/Clear All' link text changes to '<selectAll_link_text>'
    And all the products listed in my wishlist are unselected
    And the 'manage selected' drop down list is disabled
		#And clearAll items in the wishlist

    Examples:
      | country           | language | currency | clearAll_link_text | selectAll_link_text |
      | United Kingdom    | English  |          | Clear All          | Select All          |
      | France            | Français |          | Tout effacer       | Tout sélectionner   |
      | France            | English  |          | Clear All          | Select All          |
      | Japan             | 日本語      |          | すべて選択              | すべてクリア              |
      | Japan             | English  |          | Clear All          | Select All          |
      | Korea Republic of |          | 한국어      | 모두 선택              | 모두 해제               |
      | Korea Republic of |          | English  | Clear All          | Select All          |


  @regression
  Scenario Outline: (5) Add product to shopping bag using the add to bag button from Wishlist page

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
		#When I am signed in to the website as a customer
    And I log in with the following credentials
    And I navigate to the Wishlist page
        #Then I should see a message on the Wishlist page that contains '<message>'
    And I add items to my wishlist
      | item          |
      | 1009197000004 |
    When clicking on the 'ADD TO BAG' button for the first product causes the minibag rollover to appear with '<add_to_bag>' text
    Then I clean my shopping bag

    Examples:
      | country           | language | currency | add_to_bag        |
      | United Kingdom    | English  |          | ADD TO BAG        |
      | France            | Français |          | AJOUTER AU PANIER |
      | France            | English  |          | ADD TO BAG        |
      | Korea Republic of | 한국어     |         | 쇼핑백 담기          |
      | Korea Republic of | English  |          | ADD TO BAG        |
      | Japan             | 日本語      |        | ショッピングバッグに追加|
      | Japan             | English  |          | ADD TO BAG        |

  @regression
  Scenario Outline: (6) Remove product from Wishlist

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
	#	And I am signed in to the website as a customer
    And I log in with the following credentials
    And I navigate to the Wishlist page
    And I add items to my wishlist
      | item          |
      | 1009197000004 |
      | 1009197000005 |
    When I click on the link '<remove_link>' for the first product in my Wishlist
    Then the first product is removed from my Wishlist

    Examples:
      | country           | language | currency | remove_link               |
      | United Kingdom    | English  |          | Remove item from wishlist |
      | France            | Français |          | Supprimer de la wishlist  |
      | France            | English  |          | Remove item from wishlist |
      | Korea Republic of | 한국어      |          | 위시리스트에서 상품 삭제             |
      | Korea Republic of | English  |          | Remove item from wishlist |
      | Japan             | 日本語      |          | 削除                        |
      | Japan             | English  |          | Remove item from wishlist |

  @regression
  Scenario Outline: (7) Add product to shopping bag from Wishlist page via the drop down list

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
	#	And I am signed in to the website as a customer
    And I log in with the following credentials
    And I navigate to the Wishlist page
    And I add items to my wishlist
      | item          |
      | 1009197000004 |
    And I tick the first product from my Wishlist
    When I select '<wishlist_action>' from the drop down list
    Then the mini bag rollover list appears
    Then I clean my shopping bag
    Examples:
      | country           | language | currency | wishlist_action     |
      | United Kingdom    | English  |          | Add to Shopping Bag |
      | France            | Français |          | Ajouter au panier   |
      | France            | English  |          | Add to Shopping Bag |
      | Japan             | 日本語      |          | ショッピングバッグに追加する      |
      | Japan             | English  |          | Add to Shopping Bag |
      | Korea Republic of | English  |          | Add to Shopping Bag |
      | Korea Republic of | 한국어      |          | 쇼핑백 담기              |

  @regression
  Scenario Outline: (8) Remove product from Wishlist page via the drop down list and then cancel the removal of the product by clicking the Cancel button in the overlay message

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
	#	And I am signed in to the website as a customer
    And I log in with the following credentials
    And I navigate to the Wishlist page
    And I add items to my wishlist
      | item          |
      | 1009197000004 |
    And I tick the first product from my Wishlist
    When I select '<wishlist_action>' from the drop down list
    And a 'DELETE ITEMS' overlay appears containing the message '<message>'
    And I click 'CANCEL' on the 'DELETE ITEMS' overlay
    Then the 'DELETE ITEMS' overlay is closed

    When I select '<wishlist_action>' from the drop down list
    And I click on the 'X' close icon in the right hand corner of the 'DELETE ITEMS' overlay
    Then the 'DELETE ITEMS' overlay is closed

    When I select '<wishlist_action>' from the drop down list
    And I click anywhere on the page outside of the 'DELETE ITEMS' overlay
    Then the 'DELETE ITEMS' overlay is closed
    And the product has not been removed from my Wishlist

    Examples:
      | country           | language | currency | wishlist_action          | message                                                                         |
      | United Kingdom    | English  |          | Remove From Wishlist     | You are about to delete items from your wishlist. Continue?                     |
      | France            | Français |          | Supprimer de la wishlist | Vous êtes sur le point de supprimer des articles de votre wishlist. Continuer ? |
      | France            | English  |          | Remove From Wishlist     | You are about to delete items from your wishlist. Continue?                     |
      | Korea Republic of | 한국어      |          | 위시리스트에서 삭제               | 위시리스트에서 상품을 삭제할까요?                                                              |
      | Korea Republic of | English  |          | Remove From Wishlist     | You are about to delete items from your wishlist. Continue?                     |
      | Japan             | 日本語      |          | 削除                       | 欲しいものリストからアイテムを削除します。続けますか？                                                     |
      | Japan             | English  |          | Remove From Wishlist     | You are about to delete items from your wishlist. Continue?                     |

  @done @essential @removeProductsFromWishlist
  Scenario Outline: (9) Using the Load More button on the Wishlist page loads additional unselected products
  If a customer is on the Wishlist page and scrolls to the bottom of the page, clicking the Back To Top link should scroll to the top of the page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
	#	Given I am signed in to the website as a customer
    And I log in with the following credentials
    And I navigate to the Wishlist page
    And I add items to my wishlist
      | item          |
      | 1009197000003 |
      | 1009197000004 |
      | 1009197000005 |
      | 1009197000006 |
    Then '<wishlist_items>' items are listed on the Wishlist page
    And I click on 'Select/Clear All' link on the Wishlist page
    When I click on the 'LOAD MORE...' button on the Wishlist page
    Then number of wishlist items must be more than '<wishlist_items>' after LOAD MORE
    And the last '<wishlist_items>' on the Wishlist page are not selected
    When I scroll to the bottom of the page

    Examples:
      | country | language | currency | wishlist_items |
#			|United Kingdom| English | |3 |
#			| Japan            | 日本語|  |3  |
#			|Japan             | English||3|
#			| France           | Français| |3|
#			| France           |English  | |3 |
#			|Korea Republic of | 한국어  |    |3   |
#			| Korea Republic of| English|  |3  |

