#Author: Rob Brightwell
@regression @homepage @preHomepage @teamContent
Feature: Pre-Homepage
  In order to navigate the website
  First time customers
  Should see all links available in the homepage mega nav

  ########### SMOKE Test - Please donot remove ########
  @smoke
  Scenario Outline: Pre-homepage should display the same gender-specific homepage links as shown in the Mega Nav

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then I should see the homepage links in the correct order in '<language>'

    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |
#      | United Kingdom    |          |          |
#      | United States     |          |          |
#      | Japan             | 日本語    |          |
#      | France            | Français |          |
#      | Korea Republic of | 한국어     |          |


  ###########################



  @regression
  Scenario Outline: Pre-homepage should display the same gender-specific homepage links as shown in the Mega Nav

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then I should see the homepage links in the correct order in '<language>'

    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |
      | United Kingdom    |          |          |
      | United States     |          |          |
      | Japan             | 日本語    |          |
      | France            | Français |          |
      | Korea Republic of | 한국어     |          |



