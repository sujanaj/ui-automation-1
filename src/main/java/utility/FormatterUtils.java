package utility;

public class FormatterUtils {

    public static String parseCurrency(String productPrice) {
        return Character.toString(productPrice.replaceAll("[a-zA-Z0-9]","").replaceAll("\n|\t|\\s","").charAt(0));

    }

}
