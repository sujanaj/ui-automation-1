package utility;


import java.io.*;
import java.util.*;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;

public class Email {
    public String[] msgAndOrderNum;
    public String fromMsg;
    public ArrayList<String> subAndFromText;
    public String emailContent;


    public ArrayList<String> check(String host, String user,
                                   String password) {
        try {

            Properties properties = new Properties();
            properties.put("mail.pop3.host", host);
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);

            //create the POP3  object and connect with the pop server
            Store store = emailSession.getStore("pop3s");

            store.connect(host, user, password);

            // folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);

            Message[] messages = emailFolder.getMessages();

            for (int i = 0, n = messages.length; i < n; i++) {
                Message message = messages[i];
                String emailSubject = message.getSubject();
                msgAndOrderNum = emailSubject.split(":");
                fromMsg = message.getFrom()[0].toString();
                subAndFromText = new ArrayList<>(Arrays.asList(msgAndOrderNum));
                subAndFromText.addAll(Arrays.asList(fromMsg));

                //getText(message);


            }

            //close the pop store and folder objects
            emailFolder.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subAndFromText;
    }

    private String getText(Part part) throws
            MessagingException, IOException {
        if (part.isMimeType("text/*")) {
            emailContent = (String) part.getContent();
            //   textIsHtml = p.isMimeType("text/html");
            return emailContent;
        }

        if (part.isMimeType("multipart/alternative")) {
            Multipart multipart = (Multipart) part.getContent();
            String text = null;
            for (int i = 0; i < multipart.getCount(); i++) {
                Part bp = multipart.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null)
                        text = getText(bp);
                   // System.out.println(text);
                    continue;
                } else if (bp.isMimeType("text/html")) {
                    emailContent = getText(bp);
                  //  System.out.println(emailContent);
                    if (emailContent != null)
                        return emailContent;
                } else {
                    return getText(bp);
                }
            }
            return text;
        } else if (part.isMimeType("multipart/*")) {
            Multipart multipart = (Multipart) part.getContent();
            for (int i = 0; i < multipart.getCount(); i++) {
                emailContent = getText(multipart.getBodyPart(i));
                if (emailContent != null)
                    return emailContent;
            }
        }

        return null;
    }

//    public static void main(String[] args) {
//
//        //////
//
//        String host = "pop.gmail.com";
//        String mailStoreType = "pop3";
//        String username = "matches.whitelist@gmail.com";
//        String password1 = "Matches123";
//
//        check(host, mailStoreType, username, password1);
//
//
//    }

}