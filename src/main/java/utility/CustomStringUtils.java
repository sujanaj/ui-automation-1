package utility;

import com.google.common.base.Splitter;
import config.Constants;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class CustomStringUtils {

    public static  String getExpectedPageData(String expectedValue,String language) {
        Map<String, String> map=null;
        if(expectedValue.contains(":")) {
            map = Splitter.on(",").withKeyValueSeparator(":").split(expectedValue);
            if(language.equalsIgnoreCase(Constants.NO_LANGUAGE_FOUND)|| StringUtils.isBlank(language))
                expectedValue=map.get("English");
            else
                expectedValue=map.get(language);

        }
        return expectedValue;
    }

    public String generateRandomEmail(String username){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = sdf.format(new Date());
        return username+ date +"@gmail.com";
    }

    public static String removeProtocolFromURL(String actualURL) {
        return  actualURL.replace("http://", "").replace("https://", "");
    }
}
