package utility;

import org.junit.Assert;

public class Validations {

	
	public static void verifyImagesStatusCodeInProductView(String imageURL, String statusCode) {
		Assert.assertTrue("Product has issues with the url: " +imageURL, statusCode.contentEquals("200"));
	}
}
