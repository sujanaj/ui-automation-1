package utility;

import java.io.InputStream;
import java.util.Properties;
import config.Constants;

public class ConfigUtilis {

	private static Properties prop = new Properties();
	private static InputStream input = null;
	private String  reportConfigPath = "";


	public String getReportConfigPath(){
		String reportConfigPath = "src/main/java/config/extent-config.xml";
		if(reportConfigPath!= null) return reportConfigPath;
		else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath"); 
	}
}
