package baseclass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverFactory {
//cross browser details
	static String username = "dilbag.singh%40matchesfashion.com"; // Your username
	static String authkey = "ud4eb9944f190ba6";  // Your authkey
	//sauce lab details
	//public static String username = "dilbagsingh"; // Your username
	//public static String authkey = "120c2887-c52f-4b28-8ff6-fef0cc8d3d33";  // Your authkey

	String testScore = "unset";
	public static String browser = null;
	 DesiredCapabilities caps;
	static WebDriver create(String type) throws IllegalAccessException, MalformedURLException {
		WebDriver driver;
		//RemoteWebDriver driver;
		DesiredCapabilities caps;
		switch(type) {
		case "Safari":
//			caps = new DesiredCapabilities();
//			caps.setCapability("name", "Safari");
//			caps.setCapability("build", "1.0");
//			caps.setCapability("browserName", "safari");
//			caps.setCapability("version", "12");
//			caps.setCapability("safari.options", "object");
//			caps.setCapability("platform", "Mac OSX 10.12");
//			caps.setCapability("screenResolution", "1024x768");
//			caps.setCapability("record_video", "true");
//			caps.setCapability("record_network", "false");
//			driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@hub.crossbrowsertesting.com:80/wd/hub"), caps);
//
//			DesiredCapabilities caps1 = new DesiredCapabilities();
//			//capabilities.setCapability("username", "dilbag.singh@matchesfashion.com");
//			//capabilities.setCapability("accessKey", "120c2887-c52f-4b28-8ff6-fef0cc8d3d33");
//			caps1.setCapability("browserName", "Safari");
//			caps1.setCapability("platform", "macOS 10.12");
//			caps1.setCapability("version", "11.0");
//			//capabilities.setCapability("extendedDebugging: true");
//			driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@ondemand.eu-central-1.saucelabs.com/wd/hub"), caps1);
//driver.get("https://www.matchesfashion.com");

		//	https://ondemand.eu-central-1.saucelabs.com/wd/hub
			driver= new SafariDriver();
			driver.manage().window().maximize();
			long safari_ID= Thread.currentThread().getId();
			browser = "Safari";
			System.out.println("Safari browser thread id is" + safari_ID);
			break;

			case "Edge":
				System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
//				caps = new DesiredCapabilities();
//				caps.setCapability("name", "Edge");
//				caps.setCapability("build", "1.0");
//				caps.setCapability("browserName", "MicrosoftEdge");
//				caps.setCapability("version", "18");
//				//caps.setCapability("safari.options", "object");
//				caps.setCapability("platform", "Windows 10");
//				caps.setCapability("screenResolution", "1366x768");
//				caps.setCapability("record_video", "true");
//				caps.setCapability("record_network", "false");
//				driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@hub.crossbrowsertesting.com:80/wd/hub"), caps);


				driver= new EdgeDriver();
				driver.manage().window().maximize();
				browser="Edge";
				break;

		case "Firefox":
			System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver");

//			caps = new DesiredCapabilities();
//			caps.setCapability("name", "Firefox");
//			caps.setCapability("build", "1.0");
//			caps.setCapability("browserName", "firefox");
//			caps.setCapability("version", "64");
//			caps.setCapability("platform", "Windows 10");
//			caps.setCapability("screenResolution", "1366x768");
//			caps.setCapability("record_video", "true");
//			caps.setCapability("record_network", "false");
		//	driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@hub.crossbrowsertesting.com:80/wd/hub"), caps);

			driver= new FirefoxDriver();
			driver.manage().window().maximize();
			long firegox_ID= Thread.currentThread().getId();
			browser="Firefox";
			System.out.println("Firefox browser thread id is" + firegox_ID);
			break;

			case "IE":
				//System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver");

				caps = new DesiredCapabilities();
				caps.setCapability("name", "IE");
				caps.setCapability("build", "1.0");
				caps.setCapability("browserName", "firefox");
				caps.setCapability("version", "64");
				caps.setCapability("platform", "Windows 10");
				caps.setCapability("screenResolution", "1366x768");
				caps.setCapability("record_video", "true");
				caps.setCapability("record_network", "false");
				driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@hub.crossbrowsertesting.com:80/wd/hub"), caps);

				//	driver= new FirefoxDriver();
				long IE_ID= Thread.currentThread().getId();
				browser="IE";
				System.out.println("Firefox browser thread id is" + IE_ID);
				break;
		case "Chrome":

			System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
//			caps = new DesiredCapabilities();
//			caps.setCapability("name", "Chrome");
//			caps.setCapability("build", "1.0");
//			caps.setCapability("browserName", "chrome");
//			caps.setCapability("version", "70");
//			caps.setCapability("platform", "Windows 10");
//			caps.setCapability("screenResolution", "1366x768");
//			caps.setCapability("record_video", "true");
//			caps.setCapability("record_network", "false");
//			driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@hub.crossbrowsertesting.com:80/wd/hub"), caps);
//			driver.manage().window().maximize();

//			DesiredCapabilities capabilities1 = new DesiredCapabilities();
//			//capabilities.setCapability("username", "dilbag.singh@matchesfashion.com");
//			//capabilities.setCapability("accessKey", "120c2887-c52f-4b28-8ff6-fef0cc8d3d33");
//			capabilities1.setCapability("browserName", "Chrome");
//			capabilities1.setCapability("platform", "Windows 10");
//			capabilities1.setCapability("version", "59.0");
//			//capabilities.setCapability("extendedDebugging: true");
//			driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@ondemand.eu-central-1.saucelabs.com/wd/hub"), capabilities1);
			driver= new ChromeDriver();
			driver.manage().window().maximize();
			long chrome_ID= Thread.currentThread().getId();
			browser="Chrome";
			System.out.println("Chrome browser thread id is" + chrome_ID);
			break;

		default:
			throw new IllegalAccessException();
		}
		return driver;
	}

}
