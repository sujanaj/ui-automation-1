package baseclass;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

import java.net.MalformedURLException;

@CucumberOptions(features={
		"src/test/resources/feature/header",
		"src/test/resources/feature/footer/FooterAppStoreLinks.feature",
		"src/test/resources/feature/footer/FooterEmailPreferenceForm.feature",
		"src/test/resources/feature/footer/FooterLinksNew.feature",
		"src/test/resources/feature/footer/FooterShippingCountry.feature",
		"src/test/resources/feature/footer/FooterSocialNetworkingSites.feature",
		"src/test/resources/feature/header/CurrencySwitchFromHeader.feature",
		"src/test/resources/feature/homepage/GenderHomepage.feature",
		"src/test/resources/feature/homepage/PreHomePage.feature",
		"src/test/resources/feature/pdp/PDPAddToBag.feature",
		"src/test/resources/feature/pdp/PDPContactUs.feature",
		"src/test/resources/feature/pdp/PDPImagesAndVideo.feature",
		"src/test/resources/feature/pdp/PDPLinks.feature",
		"src/test/resources/feature/pdp/PDPProductDetails.feature",
		"src/test/resources/feature/pdp/PDPQTYAndSize.feature",
		"src/test/resources/feature/pdp/PDPReturnsAndDelivery.feature",
		"src/test/resources/feature/pdp/PDPShopTheLook.feature",
		"src/test/resources/feature/pdp/PDPSizeGuide.feature",
		"src/test/resources/feature/pdp/PDPSocialLinks.feature",
		"src/test/resources/feature/pdp/PDPYourMatches.feature",
		"src/test/resources/feature/placeOrder/PlaceOrder.feature",
		"src/test/resources/feature/wishlist/AddToWishlistSignedIn.feature",
		"src/test/resources/feature/wishlist/ShareWishlistPage.feature",
		"src/test/resources/feature/wishlist/ViewWishlist.feature",
		"src/test/resources/feature/wishlist/WishlistSignInRedirect.feature",
		"src/test/resources/feature/account/AddressBookPage.feature",
		"src/test/resources/feature/account/ContactDetailsPage.feature",
		"src/test/resources/feature/account/ManagePreferences.feature",
		"src/test/resources/feature/account/MyAccountPage.feature",
		"src/test/resources/feature/account/MyCreditsPage.feature",
		"src/test/resources/feature/signin/CreateAccount.feature",
		"src/test/resources/feature/softlogin/SoftLoginSecuredPage.feature",
		"src/test/resources/feature/placeOrder/3DSecure.feature",
		"src/test/resources/feature/shoppingBag/ShoppingBag.feature",
		"src/test/resources/feature/plp/PLP.feature",
		"src/test/resources/feature/plp/PLPImages.feature",
		"src/test/resources/feature/dlp/dlp.feature",
		"src/test/resources/feature/placeOrder/GiftCard.feature"
}
,glue={"stepdefinition"}
		,plugin = {"pretty", "html:target/cucumber",
"json:target/cucumber/cucumber.json"})

public class BaseClass extends AbstractTestNGCucumberTests {
	static String username = "dilbag.singh%40matchesfashion.com"; // Your username
	static String authkey = "ud4eb9944f190ba6";  // Your authkey
	public static ThreadLocal<WebDriver> dr = new ThreadLocal<WebDriver>();
	@Parameters("Browser")
	@BeforeClass
	public void beforeClass(String Browser) throws IllegalAccessException, MalformedURLException {
		WebDriver driver = new WebDriverFactory().create(Browser);
		setWebDriver(driver);

	}

	public void setWebDriver(WebDriver driver) {
		dr.set(driver);
	}

	public  WebDriver getWebDriver() {
		return dr.get();
	}


	@AfterClass
	public void afterClass() {
		getWebDriver().close();
		dr.set(null);
	}




	/*@BeforeClass
	@org.testng.annotations.Parameters(value={"os", "browser"})
	public void setUp(String os,String browser) throws Exception {
		DesiredCapabilities capability = new DesiredCapabilities();
		capability.setCapability("os_api_name", os);
		capability.setCapability("browser_api_name", browser);
		capability.setCapability("name", "TestNG-Parallel");
		driver = new RemoteWebDriver(
				new URL("http://" + username + ":" + authkey + "@hub.crossbrowsertesting.com:80/wd/hub"),
				capability);
	}*/

}
