package pages;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.Set;

public class FooterPage extends CommonFunctions {

    SoftAssert softAssert = new SoftAssert();
    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
    String footerLinksContainer = obj_Property.readProperty("footerLinksByClassName");
    String emailPreferencesFooter = obj_Property.readProperty("emailPreferencesFooterByCSS");
    String emailSignUpBtnFooter = obj_Property.readProperty("emailSignUpBtnFooterByCSS");
    String errorEmailSignUp = obj_Property.readProperty("errorMessageEmailSignupFooterByCSS");
    String emailPreferencesLinkFooter = obj_Property.readProperty("emailPreferencesLinkFooterByCSS");
    String footerLinksList = obj_Property.readProperty("footerLinksListByCSS");

    public void clickOnAppStoreImagesLinks(String link) {
        waitForPageToLoad();
        focusElement(footerLinksContainer);
        String ourAppsImageLinks = obj_Property.readProperty("footerAppsLocatorByCSS");
        clickOnElementInListBySrcContains(ourAppsImageLinks, link);
        waitForPageToLoad();
    }

    public void clickOnOurAppsTextLinks(String link) {
        waitForPageToLoad();
        String ourAppsLinksTexts = obj_Property.readProperty("footerAppsLinksTextsByCSS");
        focusElement(footerLinksContainer);
        clickOnElementInListByContains(ourAppsLinksTexts, link);
        waitForPageToLoad();
    }

    public void clickOnElementInListBySrcContains(String listCss, String elementName) {
        List<WebElement> list = waitForListElementByCssLocator(listCss);
        boolean found = false;
        for (WebElement e : list) {
            if (e.getAttribute("src").trim().contains(elementName)) {
                e.click();
                found = true;
                break;
            }
        }
        softAssert.assertTrue(found,elementName + " Element was not found" + " Actual: " +found );
        softAssert.assertAll();
    }

    public void clickOnElementInListByContains(String listCss, String elementName) {
        List<WebElement> list = getWebDriver().findElements(By.cssSelector(listCss));
        boolean found = false;
        for (WebElement e : list) {
            if (retrieveTextByWebElement(e).trim().contains(elementName)) {
                e.click();
                found = true;
                softAssert.assertTrue(found,elementName + " Element was not found" + " Actual: " +e.getText() );
                break;
            }
        }
        softAssert.assertAll();
    }

    public void switchTabVerifyUrlAndClose(String url) {
        refreshPage();
        waitForPageToLoad();
        Set<String> windows = getWebDriver().getWindowHandles();
        String mainwindow = getWebDriver().getWindowHandle();
        if(WebDriverFactory.browser == "Firefox" && url.equalsIgnoreCase("pinterest.co.uk/matchesfashion")){
            url = "pinterest.com/matchesfashion";
        }
        for (String handle : windows) {
            getWebDriver().switchTo().window(handle);
            String pageUrl = getWebDriver().getCurrentUrl();
            if(pageUrl.contains(url)){
                waitForPageToLoad();
                getWebDriver().close();
            }
        }
        getWebDriver().switchTo().window(mainwindow);
        focusElement(footerLinksContainer);
    }

    public void clickOnSocialIcon(String icon) {
        waitForPageToLoad();
        String topOfFooter = obj_Property.readProperty("topofFooterByClassName");
        String socialIcons = obj_Property.readProperty("socialIconsByCSS");
        focusElement(topOfFooter);
        clickOnElementInListBySrcContains(socialIcons, icon);
        refreshPage();
        waitForPageToLoad();
    }

    public void clickShippingCountryButton() {
        waitForPageToLoad();
        String shippingCountryFooter = obj_Property.readProperty("shippingCountryFooterByCSS");
        focusElement(footerLinksContainer);
        clickElementByCSS(shippingCountryFooter);
    }

    public String grabEmailPlaceHolderText() {
        waitForPageToLoad();
        return getWebElement(emailPreferencesFooter).getAttribute("placeholder").trim();
    }

    public void writeInFooterEmailField(String email) {
        waitForPageToLoad();
        focusElement(footerLinksContainer);
        clickElementByCSS(emailPreferencesFooter);
        getWebElement(emailPreferencesFooter).sendKeys(email);
    }

    public void clickOnSignUpButton() {
        waitForPageToLoad();
        focusElement(footerLinksContainer);
        clickElementByCSS(emailSignUpBtnFooter);
    }

    public String grabFooterInvalidErrorMessage() {
        waitForPageToLoad();
        focusElement(footerLinksContainer);
        return retrieveText(errorEmailSignUp);
    }

    public void clickOnEmailPreferences() {
        waitForPageToLoad();
        focusElement(emailPreferencesLinkFooter);
        clickElementByCSS(emailPreferencesLinkFooter);
    }

    public void clickOnFooterLinkList(String link) {
        waitForPageToLoad();
        focusElement(footerLinksContainer);
        clickOnElementInList(footerLinksList, link);
    }
}
