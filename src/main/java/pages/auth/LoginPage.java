package pages.auth;

import commonfunctions.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class LoginPage extends CommonFunctions {

    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
    private String logInButton = obj_Property.readProperty("logInButton");
    String loginUserName=obj_Property.readProperty("loginUserName");
    String loginPassword=obj_Property.readProperty("loginPassword");

    public void logIn(String username, String userpass) {
        enterTextInputBox(loginUserName, username);
        enterTextInputBox(loginPassword, userpass);
        clickOnLoginButton();
        waitForPageToLoad();
        waitForPageToLoad();
    }

    public void clickOnLoginButton() {
        waitForPageToLoad();
        getWebDriver().findElement(By.cssSelector(logInButton)).click();
    }

    public void clickOnSignOutLink(String country, String language) {
        waitForScriptsToLoad();
        waitForPageToLoad();
        if (!country.equalsIgnoreCase("France") && !country.equalsIgnoreCase("Korea Republic of") && !country.equalsIgnoreCase("Japan")) {
            ((JavascriptExecutor) getWebDriver()).executeScript("$(\".authenticated-user select\").val(\"/logout\").change()");
        } else if (country.equalsIgnoreCase("France") && language.equalsIgnoreCase("Français")) {
            ((JavascriptExecutor) getWebDriver()).executeScript("$(\".authenticated-user select\").val(\"/fr/logout\").change()");
        } else if (country.equalsIgnoreCase("France") && language.equalsIgnoreCase("English")) {
            ((JavascriptExecutor) getWebDriver()).executeScript("$(\".authenticated-user select\").val(\"/en-fr/logout\").change()");
        } else if (country.equalsIgnoreCase("Korea Republic of") && language.equalsIgnoreCase("한국어")) {
            ((JavascriptExecutor) getWebDriver()).executeScript("$(\".authenticated-user select\").val(\"/kr/logout\").change()");
        } else if (country.equalsIgnoreCase("Korea Republic of") && language.equalsIgnoreCase("English")) {
            ((JavascriptExecutor) getWebDriver()).executeScript("$(\".authenticated-user select\").val(\"/en-kr/logout\").change()");
        } else if (country.equalsIgnoreCase("Japan") && language.equalsIgnoreCase("日本語")) {
            ((JavascriptExecutor) getWebDriver()).executeScript("$(\".authenticated-user select\").val(\"/jp/logout\").change()");
        } else if (country.equalsIgnoreCase("Japan") && language.equalsIgnoreCase("English")) {
            ((JavascriptExecutor) getWebDriver()).executeScript("$(\".authenticated-user select\").val(\"/en-jp/logout\").change()");
        }
        waitForPageToLoad();
    }
}
