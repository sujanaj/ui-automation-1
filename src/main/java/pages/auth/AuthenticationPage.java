package pages.auth;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import config.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AuthenticationPage extends CommonFunctions {

    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();

    private String isSignedIn = obj_Property.readProperty("isSignedIn");
    private String signInLink = obj_Property.readProperty("signInLink");
    private String myAccountNameLink = obj_Property.readProperty("myAccountNameLink");
    private String myAccountLinks = obj_Property.readProperty("myAccountLinks");
    private String myAccountLabel = obj_Property.readProperty("myAccountLabel");

    public String getUserName() {
        return userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    String userName = null;
    String userPassword = null;


    public boolean checkIfSignedIn() {
        waitForPageToLoad();
        waitForScriptsToLoad();
        if (isElementDisplayed(isSignedIn)) {
            return true;
        } else {
            return false;
        }
    }

    public void setUserCredentials(){
        if(WebDriverFactory.browser.equalsIgnoreCase("Chrome")) {
            userName = Constants.CHROME_USERNAME;
            userPassword = Constants.CHROME_PASSWORD;
        }
        else if (WebDriverFactory.browser.equalsIgnoreCase("Safari")) {
            userName = Constants.SAFARI_USERNAME;
            userPassword = Constants.SAFARI_PASSWORD;
        }
        else if (WebDriverFactory.browser.equalsIgnoreCase("Firefox")) {
            userName = Constants.FIREFOX_USERNAME;
            userPassword = Constants.FIREFOX_PASSWORD;
        }
        else if (WebDriverFactory.browser.equalsIgnoreCase("Edge")) {
            userName = Constants.EDGE_USERNAME;
            userPassword = Constants.EDGE_PASSWORD;
        }
    }


    public void clickOnSignInButton() {
        waitForPageToLoad();
        waitForPageToLoad();
        waitForElementByCssLocator(signInLink);
        clickElementByCSS(signInLink);
        waitForPageToLoad();
    }

    public void clickAccountLink(String linkName) {
        waitForPageToLoad();

        WebElement accountLink = getWebElement(myAccountNameLink);
        accountLink.click();
        waitForPageToLoad();
        List<WebElement> accountOptions = waitForListElementByCssLocator(myAccountLinks);
        for (WebElement webElement : accountOptions) {
                if (webElement.getAttribute("href").contains(linkName.replaceAll(" ","").toLowerCase())) {
                clickElementByWebElement(webElement);
                waitForPageToLoad();
                break;
            }
        }
    }

    public String grabMyAccountLabel() {
        return  getTextByCssSelector(myAccountLabel);
    }
}
