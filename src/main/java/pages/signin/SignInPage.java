package pages.signin;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import config.ObjPropertyReader;
import org.testng.asserts.SoftAssert;
import pages.account.AddressBookPage;
import pages.auth.AuthenticationPage;

public class SignInPage extends CommonFunctions {

    RegisterSignInPage registerSignInPage = new RegisterSignInPage();
    AuthenticationPage authenticationPage = new AuthenticationPage();
    AddressBookPage addressBookPage = new AddressBookPage();
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    ObjPropertyReader objPropertyReader = new ObjPropertyReader();
    private String emailLocator = objPropertyReader.readProperty("emailLocator");
    private String url_address_book_page = objPropertyReader.readProperty("URL_ADDRESS_BOOK_PAGE");
    String loginUserName = objPropertyReader.readProperty("loginUserNameById");
    String loginPassword = objPropertyReader.readProperty("loginPasswordById");
    String logInButton = objPropertyReader.readProperty("logInButton");
    SoftAssert assertion = new SoftAssert();



    public void registerCustomerIfNotExist() {

        try {
            if (registerSignInPage.getLoginError().equalsIgnoreCase("You have entered incorrect details for email address or password, or both.")) {
                registerSignInPage.selectTitle("Mr", "");
                registerSignInPage.inputFirstName("Test Automation");
                registerSignInPage.inputLastName("Test Automation");
                if (WebDriverFactory.browser.equalsIgnoreCase("Chrome")) {
                    enterTextInputBox(emailLocator, Constants.CHROME_USERNAME);
                } else if (WebDriverFactory.browser.equalsIgnoreCase("Safari")) {
                    enterTextInputBox(emailLocator,Constants.SAFARI_USERNAME);
                } else if (WebDriverFactory.browser.equalsIgnoreCase("Firefox")) {
                    enterTextInputBox(emailLocator,Constants.FIREFOX_USERNAME);
                }
                registerSignInPage.inputPassword(Constants.CHROME_PASSWORD);
                registerSignInPage.inputConfirmPassword(Constants.CHROME_PASSWORD);
                registerSignInPage.inputPhone("4354354354556");
                registerSignInPage.selectEmailable();
                registerSignInPage.clickOnCreateAnAccount();
                String accountLabel = authenticationPage.grabMyAccountLabel();
                assertion.assertTrue(!accountLabel.toUpperCase().contains("SIGN IN"), "Failure: Sign in is still displayed on homepage. ");
                assertion.assertAll();
                waitForPageToLoad();
                navigateUsingURL.navigateToRelativeURL(url_address_book_page);
                waitForPageToLoad();
                clickElementByCSS(".useGIManualLink");
                waitForPageToLoad();
                addressBookPage.enterAddress1("ad1");
                addressBookPage.enterCity("city");
                addressBookPage.enterPostcode("av1 4gd");
                addressBookPage.clickOnSaveAddress();
                waitForPageToLoad();
            }
        } catch (Exception e) {

        }
    }

    public void enterCredentialsToLogin(String userName, String password){
        if((null==System.getProperty("username")) && (null==System.getProperty("password"))) {
            enterTextInputBox(loginUserName, userName);
            enterTextInputBox(loginPassword, password);
        } else {
            enterTextInputBox(loginUserName, System.getProperty("username"));
            enterTextInputBox(loginPassword, System.getProperty("password"));
        }
    }

    public void enterCredentials(String userName, String password){
        enterCredentialsToLogin(userName,password);
        clickOnLoginButton();
        waitForScriptsToLoad();
        registerCustomerIfNotExist();
    }

    public void clickOnLoginButton() {
        clickOnElement(getWebElement(logInButton));
        waitForScriptsToLoad();
    }
}
