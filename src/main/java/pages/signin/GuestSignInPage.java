package pages.signin;

import commonfunctions.CommonFunctions;
import config.ObjPropertyReader;

public class GuestSignInPage extends CommonFunctions {
    ObjPropertyReader objPropertyReader = new ObjPropertyReader();
    private String guestEmailInputLocator = objPropertyReader.readProperty("guestEmailInputLocator");
    private String guestButtonLocatorByCss = objPropertyReader.readProperty("guestButtonLocatorByCss");

    public void inputGuestEmail(String email) {
        waitForPageToLoad();
        enterTextInputBox(guestEmailInputLocator,email);
    }

    public void clickContinueGuest() {
        clickElementByCSS(guestButtonLocatorByCss);
    }
}
