package pages;

import commonfunctions.CommonFunctions;
import config.ObjPropertyReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HeaderPage extends CommonFunctions {

    ObjPropertyReader objPropertyReader = new ObjPropertyReader();
    private String minicartIcon = objPropertyReader.readProperty("minicartIconByCSS");
    private String cartButtonLocator = objPropertyReader.readProperty("shoppingCartIconByCSS");
    private String myAccountLinkLoggedInUser = objPropertyReader.readProperty("myAccountLinkLoggedInUser");
    private String loggedInUserOptionsDropdownLocator = objPropertyReader.readProperty("loggedInUserOptionsDropdownLocator");
    private String myAccountContainerLinksLocator = objPropertyReader.readProperty("myAccountContainerLinksLocator");
    private  String myCreditsLinkLoggedInUser = objPropertyReader.readProperty("myCreditsLinkLoggedInUser");
    private String myPaymentCard = objPropertyReader.readProperty("myPaymentCard");

    public void clickWishlistButton() {
        waitForPageToLoad();
        waitForElementByCssLocator("li[class$='wishlist authenticated-user'] a");
        WebElement wishlistButton = getWebDriver().findElement(By.cssSelector("li[class$='wishlist authenticated-user'] a"));
        wishlistButton.click();
        waitForPageToLoad();
    }

    public void goToImpersonationCart() {
        waitForPageToLoad();
        scrollToPageTop();
        focusElement(minicartIcon);
        getWebDriver().findElement(By.cssSelector(minicartIcon)).click();
    }

    public void goToCart() {
        waitForPageToLoad();
        scrollToPageTop();
        getWebDriver().findElement(By.cssSelector(cartButtonLocator)).click();
        waitForPageToLoad();
        waitForScriptsToLoad();
    }

    public void selectMyAccountOption() {
        waitForPageToLoad();
        loggedInUserAccountOptions(loggedInUserOptionsDropdownLocator,myAccountLinkLoggedInUser);
        waitForPageToLoad();
        waitForScriptsToLoad();
    }

    public void selectMyCreditsOption() {
        waitForPageToLoad();
        loggedInUserAccountOptions(loggedInUserOptionsDropdownLocator,myCreditsLinkLoggedInUser);
        waitForPageToLoad();
        waitForScriptsToLoad();
    }

    public void selectPaymentCardsOption() {
        waitForPageToLoad();
        loggedInUserAccountOptions(loggedInUserOptionsDropdownLocator,myPaymentCard);
        waitForPageToLoad();
        waitForScriptsToLoad();
    }

    public void clickOnMyAccountLink(String linkName) {
        waitForPageToLoad();
        List<WebElement> linksList = waitForListElementByCssLocator(myAccountContainerLinksLocator);
        for (WebElement webElement : linksList) {
            if (webElement.getAttribute("href").toLowerCase().contains(linkName.replaceAll(" ", "").toLowerCase())) {
                webElement.click();
                waitForPageToLoad();
                break;
            }
        }
    }

}
