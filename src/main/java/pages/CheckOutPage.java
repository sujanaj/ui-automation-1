package pages;

import baseclass.BaseClass;
import commonfunctions.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CheckOutPage extends BaseClass
{
    CommonFunctions commonFunctions = new CommonFunctions();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    private String enterCardRadioLocator = obj_Property.readProperty("enterCardRadioLocator");
    private String cardNumberInputLocator = obj_Property.readProperty("cardNumberInputLocator");
    private String nameOnCardInputLocator = obj_Property.readProperty("nameOnCardInputLocator");
    private String monthOnCardInputLocator = obj_Property.readProperty("monthOnCardInputLocator");
    private String yearOnCardInputLocator = obj_Property.readProperty("yearOnCardInputLocator");
    private String securityCodeLocator = obj_Property.readProperty("securityCodeLocator");
    private String threeDSecureSessionId = obj_Property.readProperty("threeDSecureSessionId");
    private String otpiFrameName = obj_Property.readProperty("otpiFrameName");
    private String otpFieldByCss = obj_Property.readProperty("otpFieldByCss");
    private String enterOTPInputLocator = obj_Property.readProperty("enterOTPInputLocator");
    private String submitOTPInputLocator = obj_Property.readProperty("submitOTPInputLocator");
    private String saveCardByDefaultLocator = obj_Property.readProperty("saveCardByDefaultLocator");
    private String errorMessageOnPlaceOrder = obj_Property.readProperty("errorMessageOnPlaceOrder");
    private String paymentCardRowsListLocator = obj_Property.readProperty("paymentCardRowsListLocator");
    private String existingCardTypeInfoLocator = obj_Property.readProperty("existingCardTypeInfoLocator");
    private String threeDSV1otpiFrameName = obj_Property.readProperty("threeDSV1otpiFrameName");
    private String threeDSV1otpFieldByCss = obj_Property.readProperty("threeDSV1otpFieldByCss");
    private String threeDSV1enterOTPInputLocator = obj_Property.readProperty("threeDSV1enterOTPInputLocator");
    private String threeDSV1submitOTPInputLocator = obj_Property.readProperty("threeDSV1submitOTPInputLocator");
    private String continueButtonLocator = obj_Property.readProperty("continueButtonLocator");

    public void selectSavedCreditCard(String cardType, String digits)
    {
        commonFunctions.waitForPageToLoad();

        WebElement el = getSavedCreditCardElement(cardType, digits);
        if(el != null){
            commonFunctions.scrollIntoViewByCss(existingCardTypeInfoLocator);
            el.click();
        }
        else {
            System.out.println(String.format("NO SAVED %s %s CARD FOUND",cardType, digits));
        }
    }

    public WebElement getSavedCreditCardElement(String cardName, String digits)
    {
        List<WebElement> remainingBalanceRowsList = getWebDriver().findElements(By.cssSelector(paymentCardRowsListLocator));
        for (WebElement row : remainingBalanceRowsList) {
            final String cardInfo = row.findElement(By.cssSelector(existingCardTypeInfoLocator)).getText();
            if (cardInfo.contains(cardName) && cardInfo.contains(digits)) {
                return row.findElement(By.cssSelector(".cardtype-image label"));
            }
        }
        return null;
    }

    public void selectSShippingMethod(String shipping_method, String paymentCardRowsListLocator, String existingCardTypeInfoLocator) {
        commonFunctions.waitForPageToLoad();

        List<WebElement> remainingBalanceRowsList = getWebDriver().findElements(By.cssSelector(paymentCardRowsListLocator));
        for (WebElement row : remainingBalanceRowsList) {
            if (row.findElement(By.cssSelector(existingCardTypeInfoLocator)).getText().contains(shipping_method)) {
                WebElement el = row.findElement(By.cssSelector(".select-delivery"));
                commonFunctions.scrollIntoViewByCss(existingCardTypeInfoLocator);
                el.click();
            }
        }
    }

    public void clickEnterCardDetails() {
        commonFunctions.waitForPageToLoad();
        commonFunctions.scrollScreen500Pixel();
        commonFunctions.clickElementByCSS("#continueBtn");
        commonFunctions.scrollIntoViewByCss(enterCardRadioLocator);
        commonFunctions.selectRadioButton(enterCardRadioLocator);
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForScriptsToLoad();
    }

    public void selectCardType(String cardType) {
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
        ((JavascriptExecutor) getWebDriver()).executeScript("$(\".cardType .selecter select\").val($(\".cardType .selecter select option:contains("+cardType+")\").val()).change()");
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
    }


    public void inputCardNumber(String cardNumber) {
        commonFunctions.enterTextInputBox(cardNumberInputLocator,cardNumber);
    }

    public void inputCardName(String cardName) {
        commonFunctions.enterTextInputBox(nameOnCardInputLocator,cardName);
    }

    public void inputExpireYear(String year) {
        commonFunctions.waitForPageToLoad();
        commonFunctions.scrollScreen500Pixel();
        commonFunctions.selectFromDropdownByText(yearOnCardInputLocator,year);
    }

    public void inputExpireMonth(String month) {
        commonFunctions.waitForPageToLoad();
        commonFunctions.scrollScreen500Pixel();
        commonFunctions.waitForListElementByCssLocator(monthOnCardInputLocator);
        commonFunctions.selectFromDropdownByText(monthOnCardInputLocator,month);
    }

    public void inputSecurityCode(String securityCode) {
        commonFunctions.enterTextInputBox(securityCodeLocator,securityCode);
    }

    public boolean isthreeDSecureSessionIdPresent() {
        boolean isthreeDSecureSessionIdPresent = false;
        if (commonFunctions.getWebElement(threeDSecureSessionId).isDisplayed()) {
            isthreeDSecureSessionIdPresent = true;
        }
        return isthreeDSecureSessionIdPresent;
    }

    public void enterOTP() {
        String otp = "";
        commonFunctions.waitForScriptsToLoad();
        commonFunctions.waitForPageToLoad();
        commonFunctions.goTOIframe(otpiFrameName);
        commonFunctions.waitForPageToLoad();
        otp = commonFunctions.retrieveText(otpFieldByCss);
        otp = otp.substring(otp.length()-5,otp.length()-1);
        commonFunctions.enterTextInputBox(enterOTPInputLocator, otp);
    }

    public void submitOTP() {
        commonFunctions.waitForPageToLoad();
        commonFunctions.clickElementByCSS(submitOTPInputLocator);
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForScriptsToLoad();
        commonFunctions.waitForPageToLoad();
    }

    public void unselectSaveCardByDefaultCheckbox(){
        commonFunctions.selectRadioButton(saveCardByDefaultLocator);
        commonFunctions.waitForPageToLoad();
    }

    public boolean getErrorMessageOnPlaceOrder()
    {
        boolean isErrorExist = false;
        try{
            if (commonFunctions.isElementDisplayed(errorMessageOnPlaceOrder)) {
                isErrorExist = true;
                System.out.println("PLACE ORDER FAILED WITH ERROR MESSAGE: " + commonFunctions.getTextByCssSelector(errorMessageOnPlaceOrder));
            }
    }
    catch (Exception e){
            e.getMessage();
        }
        return isErrorExist;
    }


    public void enterOTPFor3dSV1() {
        String otp = "";
        commonFunctions.waitForScriptsToLoad();
        commonFunctions.waitForPageToLoad();
        commonFunctions.goTOIframe(threeDSV1otpiFrameName);
        commonFunctions.waitForPageToLoad();
        otp = commonFunctions.retrieveText(threeDSV1otpFieldByCss);
        otp = otp.substring(otp.length()-5,otp.length()-1);
        commonFunctions.enterTextInputBox(threeDSV1enterOTPInputLocator, otp);
    }

    public void submitOTPFor3dSV1() {
        commonFunctions.waitForPageToLoad();
        commonFunctions.clickElementByCSS(threeDSV1submitOTPInputLocator);
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForScriptsToLoad();
        commonFunctions.waitForPageToLoad();
    }
}
