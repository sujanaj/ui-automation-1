package pages.account;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import config.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ContactDetailsPage extends CommonFunctions {
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    private String pcccFieldLocator = obj_Property.readProperty("pcccFieldLocator");
    private String saveButtonDetailsFormLocator = obj_Property.readProperty("saveButtonDetailsFormLocator");
    private String personalDetailsAlert = obj_Property.readProperty("personalDetailsAlert");
    private String personalDetailsErrorAlert = obj_Property.readProperty("personalDetailsErrorAlert");
    private String currentPasswordInputLocator = obj_Property.readProperty("currentPasswordInputLocator");
    private String newPasswordInputLocator = obj_Property.readProperty("newPasswordInputLocator");
    private String confirmPasswordInputLocator = obj_Property.readProperty("confirmPasswordInputLocator");
    private String saveButtonPasswordFormLocator = obj_Property.readProperty("saveButtonPasswordFormLocator");
    private String birthDayText = obj_Property.readProperty("birthDayText");
    private String birthDayDate = obj_Property.readProperty("birthDayDate");
    private String birthDayMonth = obj_Property.readProperty("birthDayMonth");
    private String submitButtonBirthDayDetails = obj_Property.readProperty("submitButtonBirthDayDetails");
    private String savedBirthDayDetails = obj_Property.readProperty("savedBirthDayDetails");

    public boolean isPCCCDisplayed() {
        waitForPageToLoad();
        waitForPageToLoad();
        boolean isPCCCDisplayed = false;
        try {
            if (isElementDisplayed(pcccFieldLocator))
                isPCCCDisplayed = true;
            return isPCCCDisplayed;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            e.printStackTrace();
        }
        return isPCCCDisplayed;
    }


    public void enterPCCC(String pccc){
        waitForPageToLoad();
        try {
            if (isPCCCDisplayed()){
                enterTextInputBox(pcccFieldLocator,pccc);
            }
        }catch (org.openqa.selenium.NoSuchElementException e){
           e.printStackTrace();
        }
    }

    public String grabPCCC() {
        try {
            if (isPCCCDisplayed()){
                return getWebDriver().findElement(By.cssSelector(pcccFieldLocator)).getAttribute("value");
            }
        }catch (org.openqa.selenium.NoSuchElementException e){
            e.printStackTrace();
        }
        return "";
    }

    public void clickDetailsFormSaveButton(){
        waitForElementByCssLocator(saveButtonDetailsFormLocator).click();
        clickElementByCSS(saveButtonDetailsFormLocator);
    }

    public String grabDetailsFormAlertMessage(){
        waitForPageToLoad();
        String result = "";
        result =  waitForElementByCssLocator(personalDetailsAlert).getText();
        return result;
    }

    public List<String> getErrorMessage()
    {
        List<String> errorMessages = new ArrayList<>();
        List<WebElement> elements = getWebDriver().findElements(By.cssSelector(personalDetailsErrorAlert));
        for (WebElement el : elements){
            errorMessages.add(el.getText());
        }
        return errorMessages;
    }

    private String getPassword(){
        String userPassword= null;
        if(WebDriverFactory.browser.equalsIgnoreCase("Chrome")) {
            userPassword = Constants.CHROME_PASSWORD;
        }
        else if (WebDriverFactory.browser.equalsIgnoreCase("Safari")) {
            userPassword = Constants.SAFARI_PASSWORD;
        }
        else if (WebDriverFactory.browser.equalsIgnoreCase("Firefox")) {
            userPassword = Constants.FIREFOX_PASSWORD;
        }
        else if (WebDriverFactory.browser.equalsIgnoreCase("Edge")) {
            userPassword = Constants.EDGE_PASSWORD;
        }
        else if (WebDriverFactory.browser.equalsIgnoreCase("IE")) {
            userPassword = Constants.IE_PASSWORD;
        }

        return userPassword;
    }

    public void setCurrentPassword(){
        enterTextInputBox(currentPasswordInputLocator,getPassword());
    }

    public void setNewPassword(){
        enterTextInputBox(newPasswordInputLocator,getPassword());
    }

    public void setConfirmPassword(){
        enterTextInputBox(confirmPasswordInputLocator,getPassword());
    }

    public void clickPasswordFormSaveButton(){
        clickElementByCSS(saveButtonPasswordFormLocator);
    }

    public void clickBirthDayDetailsSubmitButton(){selectRadioButton(submitButtonBirthDayDetails);    }

    private boolean isBirthDayTextVisible(){
        waitForPageToLoad();
        boolean isBirthDayTextVisible = false;
        if(getWebElement(birthDayText).isDisplayed()){
            isBirthDayTextVisible = true;
        }
        return isBirthDayTextVisible;
    }

    public void selectBirthDayDate() {
        try {
            waitForPageToLoad();
            if (isBirthDayTextVisible()) {
                selectRandomValueFromDropdownElements(birthDayDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectBirthDayMonth() {
        try {
            waitForPageToLoad();
            if (isBirthDayTextVisible()) {
                selectRandomValueFromDropdownElements(birthDayMonth);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
