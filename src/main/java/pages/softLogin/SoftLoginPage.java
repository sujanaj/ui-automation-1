package pages.softLogin;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;

import java.util.Set;

public class SoftLoginPage extends CommonFunctions {
    NavigateUsingURL navigateUsingURL= new NavigateUsingURL();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    WebDriverFactory webDriverFactory = new WebDriverFactory();

    private String relativeURL;
    private String ExpectedURL;
    private String loginUserName = obj_Property.readProperty("loginUserNameById");
    private String loginPassword = obj_Property.readProperty("loginPasswordById");
    private String signInToADifferentAccountLocatorByCSS = obj_Property.readProperty("signInToADifferentAccountLocatorByCSS");
    private String createANewAccountLocatorByCSS = obj_Property.readProperty("createANewAccountLocatorByCSS");
    private String checkoutAsAGuestLinkLocatorByCSS = obj_Property.readProperty("checkoutAsAGuestLinkLocatorByCSS");


    public String getExpectedURL(String url) {
        relativeURL = navigateUsingURL.getrelativeURLJenkins();
        //String url = "/login";
        if (relativeURL == null) {
//            waitForPageToLoad();
            ExpectedURL = Constants.BASE_URL + url;
        } else {
            ExpectedURL = relativeURL + url;
        }
        return ExpectedURL;
    }

    public boolean isEmailIdEmpty() {
        boolean isEmailIdEmpty = true;
        isEmailIdEmpty = getWebElement(loginUserName).getAttribute("value").isEmpty();
        return isEmailIdEmpty;
    }

    public void enterPassword(){
        if (webDriverFactory.browser.equalsIgnoreCase(Constants.CHROME)) {
            enterTextInputBox(loginPassword, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.SAFARI)) {
            enterTextInputBox(loginPassword, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.FIREFOX)) {
            enterTextInputBox(loginPassword, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.IE)) {
            enterTextInputBox(loginPassword, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.EDGE)) {
            enterTextInputBox(loginPassword, Constants.CHROME_PASSWORD);
        }
    }


    public void deleteJSessionID(){
        Set<Cookie> cookies = getWebDriver().manage().getCookies();
        for (Cookie cookie: cookies){
            if (cookie.getName().contains("JSESSIONID")){
                String javascriptCall = "document.cookie = \"" + cookie.getName() + "=; path=" + cookie.getPath() + "; expires=Thu, 01-Jan-1970 00:00:01 GMT;\"";
                ((JavascriptExecutor) getWebDriver()).executeScript(javascriptCall);
            }

        }
    }

    public boolean isRememberMeCookieExist(){
        boolean isRememberMeCookieExist = false;
        Set<Cookie> cookies = getWebDriver().manage().getCookies();
        for (Cookie cookie: cookies){
            if(cookie.getName().contains("mfstorefrontRememberMe")){
                isRememberMeCookieExist =true;
                return isRememberMeCookieExist;
            }
        }
        return isRememberMeCookieExist;
    }

    public void clickOnSignedIntoDifferentAccountLink(){
        clickElementByCSS(signInToADifferentAccountLocatorByCSS);
        waitForPageToLoad();
    }

    public void ClickOnCreateANewAccountLink(){
        clickElementByCSS(createANewAccountLocatorByCSS);
        waitForPageToLoad();
    }

    public void ClickOnCheckoutAsAGuestLink() {
        clickElementByCSS(checkoutAsAGuestLinkLocatorByCSS);
        waitForPageToLoad();
    }
}
