package pages;

import commonfunctions.CommonFunctions;
import config.ObjPropertyReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.LinkedList;
import java.util.List;

public class ShippingFormPage extends CommonFunctions {

    ObjPropertyReader objPropertyReader = new ObjPropertyReader();
    String errorMessagesOnSubmission = objPropertyReader.readProperty("errorMessagesOnSubmissionByCss");
    String continueButtonLocator = objPropertyReader.readProperty("continueButtonLocator");


    public List<String> grabValidationMessages() {
        List<String> errorMessages = new LinkedList<>();
        List<WebElement> elements = grabValidationErrorMessages();
        for(WebElement element : elements) {
            errorMessages.add(element.getText());
        }
        return errorMessages;
    }

    public List<WebElement> grabValidationErrorMessages() {
        waitForPageToLoad();
        waitForScriptsToLoad();
        return waitForListElementByCssLocator(errorMessagesOnSubmission);
    }

    public void clickOnContinue() {
        clickElementByCSS(continueButtonLocator);
        waitForPageToLoad();
    }


}
