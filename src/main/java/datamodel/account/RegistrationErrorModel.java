package datamodel.account;

public class RegistrationErrorModel {
    private String title;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String checkPassword;
    private String phone;

    public RegistrationErrorModel() {
        this.title = "";
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.password = "";
        this.checkPassword = "";
        this.phone = "";
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCheckPassword() {
        return checkPassword;
    }

    public void setCheckPassword(String checkPassword) {
        this.checkPassword = checkPassword;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
