package runner;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.asserts.SoftAssert;
public class Test {
	SoftAssert assertions = new SoftAssert();
	public  List<WebElement> link;
	private WebDriver driver;
	public String linktext;
	String pageTitle;
	SoftAssert softAssertion= new SoftAssert();

	public Test(WebDriver  driver) {

		this.driver=driver;

	}

	@Parameters("Browser")
	@BeforeClass
	public void beforeClass(String Browser) throws IllegalAccessException {
		switch(Browser) {
		case "Safari":
			driver= new SafariDriver();
			driver.manage().window().maximize();
			long safari_ID= Thread.currentThread().getId();
			System.out.println("Safari browser thread id is" + safari_ID);
			break;
		case "Firefox":
			System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver");
			driver= new FirefoxDriver();
			long firegox_ID= Thread.currentThread().getId();
			System.out.println("Firefox browser thread id is" + firegox_ID);
			break;
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
			driver= new ChromeDriver();
			long chrome_ID= Thread.currentThread().getId();
			System.out.println("Chrome browser thread id is" + chrome_ID);
			break;

		default:
			throw new IllegalAccessException();
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

	@org.testng.annotations.Test(enabled = true,dataProvider =  "accessoriesCategories", description = "menu links")
	public void testcase2(String linktext) {
		driver.get("https://regression.matchesfashion.com/mens");
		driver.manage().window().maximize();
		driver.navigate().refresh();

		driver.findElement(By.linkText(linktext)).click();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			boolean fg=	 driver.findElement(By.cssSelector(".grid-container h1")).isEnabled();

			if(fg=true) {
				pageTitle=	driver.findElement(By.cssSelector(".grid-container h1")).getText();


				System.out.println("Page title displayed" + pageTitle);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

		boolean flag=true;
		if(pageTitle.isEmpty()) {
			flag=false;
			assertions.assertTrue(true, "main menu link is not working"+linktext);  

		}
		softAssertion.assertTrue(flag,  "Page tile text not found");

	}

	@DataProvider(name = "accessoriesCategories")
	public Object[][] createData1() {
		return new Object[][] {
			{ "SALE"},
			{ "JUST IN"},
			{ "DESIGNERS"},
			{ "CLOTHING"},
			{ "SHOES"},
			{ "BAGS"},
			{ "ACCESSORIES"},
			{ "HOME"},
			{ "STORIES"},
			{ "WHAT'S ON"},
			{ "GIFTS"},

		};
	}

	@DataProvider(name = "cat")
	public Object[][] createData2() {
		return new Object[][] {
			{ "JUST IN"},
			{ "DESIGNERS"},

		};
	}












}
