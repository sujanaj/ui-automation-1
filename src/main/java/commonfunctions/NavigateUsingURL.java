package commonfunctions;
import cucumber.api.Scenario;
import org.apache.commons.lang3.StringUtils;
import config.Constants;

public class NavigateUsingURL {
    String url;
    String relativeURL;
    CommonFunctions commonFunctions = new CommonFunctions();

    public String getrelativeURLJenkins() {
        url = System.getProperty("envURL");
        return url;
    }

    public void iAmOnTheWebsiteWithDeliveryCountry(String relativeCountryURL) {
        commonFunctions.deleteAllCookies();
        commonFunctions.refreshPage();


        if (getrelativeURLJenkins() == null) {
            commonFunctions.navigateTo(Constants.BASE_URL + StringUtils.defaultString(relativeCountryURL, StringUtils.EMPTY));
        } else {
            String relativeURLJenkins = getrelativeURLJenkins();
            System.out.println("relative URL is" +relativeURLJenkins );
            commonFunctions.navigateTo(relativeURLJenkins);

        }
    }

    public void navigateToRelativeURL(String url) {
        relativeURL = getrelativeURLJenkins();
        if (relativeURL == null) {
            commonFunctions.waitForPageToLoad();
            commonFunctions.navigateTo(Constants.BASE_URL + url);
        } else {
            String relativeURLJenkins = getrelativeURLJenkins() + url;
            commonFunctions.navigateTo(relativeURLJenkins);
        }

        }

    public String getURL() {
        if (getrelativeURLJenkins()==null){
            return Constants.BASE_URL;
        }else {
            return getrelativeURLJenkins();
        }
    }

    public void before(Scenario scenario){
        scenario.getName();
    }

    public void changeLanguageAndCountryUsingURL(String countryURL){
        commonFunctions.deleteAllCookies();
        if (getrelativeURLJenkins() == null) {
            commonFunctions.navigateTo(Constants.BASE_URL+countryURL );
        } else{
            String relativeURLJenkins = getrelativeURLJenkins();
            System.out.println("relative URL is" + relativeURLJenkins);
            commonFunctions.navigateTo(relativeURLJenkins + countryURL);
        }
    }

}

