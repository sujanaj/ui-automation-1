package commonfunctions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import config.Constants;
import config.ProductsJsonJavaBean;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.*;

public class ProductDetailsFromJson {
    public JsonParser parser;
    public JsonObject json;
    public Gson gson;
    public List<Object> productSize;
    public Object productPrice;
    public Object designers;
    String designerName;
    String formattedPrice;
    public static ArrayList<String> productCodeAndSize;
    public List<Object> sizeListCatOfProduct;
    CommonFunctions commonFunctions = new CommonFunctions();
    ApiSetUp testData = new ApiSetUp();
    ProductsJsonJavaBean jsonTestData = new ProductsJsonJavaBean();
    public static Map<String,String> availableProductDetails;
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    String pdp_SizeDropDown_CSS = obj_Property.readProperty("pdp_SizeDropDown_CSS");
    private NavigateUsingURL navigateUsingURL = new NavigateUsingURL();

// Donot remove this code, can be used for future
//    static {
//
//    ApiSetUp testData = new ApiSetUp();
//    ProductsJsonJavaBean jsonTestData = new ProductsJsonJavaBean();
//}

    public void baseURI() {
//        if (testData == null)
//        {
//            testData = new ApiSetUp();
//        }
        testData.baseURI();
    }

    public io.restassured.response.Response endPoint() {
        return testData.hitEndPoint();
    }

    public io.restassured.response.Response oneSizeProductEndPoint() {
        return testData.hitOneSizeProductEndpoint();
    }

    public void setHeader() {
        testData.setHeader();
    }


    public int getResponseCode() {
        mapJsonToJavaClass();
        return testData.responseCode();
    }

    public void mapJsonToJavaClass() {
        parser = new JsonParser();
        json = (JsonObject) parser.parse(testData.response.getBody().asString());
        gson = new Gson();
        jsonTestData = gson.fromJson(json, ProductsJsonJavaBean.class);

    }

    public ArrayList<String> getInStockProductIdWithAvailableVarients() {
        baseURI();
        setHeader();
        endPoint();
        getResponseCode();
        Set<String> productCodeSet = new HashSet<String>();
        productCodeAndSize = new ArrayList<>();
        for (int i = 0; i <= jsonTestData.getResults().size() - 1; ++i) {
            productSize = Arrays.asList(((LinkedTreeMap) jsonTestData.getResults().get(i)).get("sizeList"));
            String productCode = ((LinkedTreeMap) jsonTestData.getResults().get(i)).get("code").toString();
//            String productDescription = ((LinkedTreeMap) jsonTestData.getResults().get(i)).get("name").toString();
//            designers = ((LinkedTreeMap) jsonTestData.getResults().get(1)).get("designer");
//            designerName= ((LinkedTreeMap) designers).get("name").toString().toLowersCase();
//            productPrice = Arrays.asList(((LinkedTreeMap) jsonTestData.getResults().get(i)).get("price"));
//            formattedPrice = ((LinkedTreeMap) ((List) productPrice).get(0)).get("formattedValue").toString().toLowerCase();


//            if (verifyProductPrice()) {
                for (int iSize = 0; iSize <= ((ArrayList) productSize.get(0)).size() - 1; ++iSize) {
                    Iterator sizelist = ((LinkedTreeMap) ((ArrayList) productSize.get(0)).get(iSize)).entrySet().iterator();
                    String sizeValue = null;
                    while (sizelist.hasNext()) {
                        Object entry = sizelist.next();
                        Map.Entry me = (Map.Entry) entry;
                        String entryKey = me.getKey().toString();
                        String isAvailable;
                        if (entryKey.equalsIgnoreCase("available")) {
                            isAvailable = me.getValue().toString();
                            if (isAvailable.equalsIgnoreCase("true")) {
                                productCodeSet.add(productCode);

//                                productCodeAndSize.add(sizeValue);
//                                productCodeAndSize.add(designerName);
//                                productCodeAndSize.add(productDescription);
//                                productCodeAndSize.add(formattedPrice);
                            }
                        }
                        if (entryKey.equalsIgnoreCase("value")) {
                            sizeValue = me.getValue().toString();
                        }

                    }
                }
//            }
            if (productCodeSet.isEmpty()) {

            } else {
             //   productCodeAndSize.add(productCode);
//                Collections.sort(productCodeAndSize);
              //  break;
            }
        }
        if(!productCodeSet.isEmpty()){
            productCodeAndSize.addAll(productCodeSet);
        }
        return productCodeAndSize;

    }

    public ArrayList<String> getInStockOneSizeProductId() {
        baseURI();
        setHeader();
        oneSizeProductEndPoint();
        getResponseCode();
        ArrayList<String> productCodeList = new ArrayList<>();
        String isAvailable;
        for (int i = 0; i <= jsonTestData.getResults().size() - 1; ++i) {
            sizeListCatOfProduct = Arrays.asList(((LinkedTreeMap) jsonTestData.getResults().get(i)).get("sizeList"));
            String productCode = ((LinkedTreeMap) jsonTestData.getResults().get(i)).get("code").toString();
            if (sizeListCatOfProduct.size() == 1) {
                for (int iSize = 0; iSize <= ((ArrayList) sizeListCatOfProduct.get(0)).size() - 1; ++iSize) {
                    Iterator sizelist = ((LinkedTreeMap) ((ArrayList) sizeListCatOfProduct.get(0)).get(iSize)).entrySet().iterator();
                    while (sizelist.hasNext()) {
                        Object entry = sizelist.next();
                        Map.Entry me = (Map.Entry) entry;
                        String entryKey = me.getKey().toString();
                        if (entryKey.equalsIgnoreCase("available")) {
                            isAvailable = me.getValue().toString();
                            if (isAvailable.equalsIgnoreCase("true")) {
                                productCodeList.add(productCode);
                            }
                        }
                    }
                }
            }
        }
        return productCodeList;

    }


    public ArrayList<String>  getInStockProductSKUCode() {
        baseURI();
        setHeader();
        endPoint();
        getResponseCode();
        ArrayList<String> productCodeAndSize = new ArrayList<>();
        for (int i = 0; i <= jsonTestData.getResults().size() - 1; ++i) {
            productSize = Arrays.asList(((LinkedTreeMap) jsonTestData.getResults().get(i)).get("sizeList"));
            String productCode = ((LinkedTreeMap) jsonTestData.getResults().get(i)).get("code").toString();
//            if (verifyProductPrice() == true) {
                for (int iSize = 0; iSize <= ((ArrayList) productSize.get(0)).size() - 1; ++iSize) {
                    Iterator sizelist = ((LinkedTreeMap) ((ArrayList) productSize.get(0)).get(iSize)).entrySet().iterator();
                    String sizeValue = null;
                    while (sizelist.hasNext()) {
                        Object entry = sizelist.next();
                        Map.Entry me = (Map.Entry) entry;
                        String entryKey = me.getKey().toString();
                        String isAvailable;
                        if (entryKey.equalsIgnoreCase("available")) {
                            isAvailable = me.getValue().toString();
                            if (isAvailable.equalsIgnoreCase("true")) {
                                if (sizeValue.equalsIgnoreCase("XXS")) {
                                    productCodeAndSize.add(productCode + "000002");
                                } else if (sizeValue.equalsIgnoreCase("XS")) {
                                    productCodeAndSize.add(productCode + "000003");
                                } else if (sizeValue.equalsIgnoreCase("S")) {
                                    productCodeAndSize.add(productCode + "000004");
                                } else if (sizeValue.equalsIgnoreCase("M")) {
                                    productCodeAndSize.add(productCode + "000005");
                                } else if (sizeValue.equalsIgnoreCase("L")) {
                                    productCodeAndSize.add(productCode + "000006");
                                } else if (sizeValue.equalsIgnoreCase("XL")) {
                                    productCodeAndSize.add(productCode + "000007");
                                }

                            }

                        }
                        if (entryKey.equalsIgnoreCase("value")) {
                            sizeValue = me.getValue().toString();
                        }

                    }
                }
//            }
            if (productCodeAndSize.isEmpty()) {

            } else {
                // productCodeAndSize.add(productCode);
                Collections.sort(productCodeAndSize);
//                break;
            }

        }
        return productCodeAndSize;

    }

    public ArrayList<String> getOutOfStockProductIdWithAvailableVarients() {
        baseURI();
        setHeader();
        endPoint();
        getResponseCode();
        ArrayList<String> productCodeAndSize = new ArrayList<>();
        for (int i = 0; i <= jsonTestData.getResults().size() - 1; ++i) {
            productSize = Arrays.asList(((LinkedTreeMap) jsonTestData.getResults().get(i)).get("sizeList"));
            String productCode = ((LinkedTreeMap) jsonTestData.getResults().get(i)).get("code").toString();


            for (int iSize = 0; iSize <= ((ArrayList) productSize.get(0)).size() - 1; ++iSize) {
                Iterator sizelist = ((LinkedTreeMap) ((ArrayList) productSize.get(0)).get(iSize)).entrySet().iterator();
                String sizeValue = null;
                while (sizelist.hasNext()) {
                    Object entry = sizelist.next();
                    Map.Entry me = (Map.Entry) entry;
                    String entryKey = me.getKey().toString();
                    String isAvailable;
                    if (entryKey.equalsIgnoreCase("available")) {
                        isAvailable = me.getValue().toString();
                        if (isAvailable.equalsIgnoreCase("false")) {
                            productCodeAndSize.add(sizeValue);
                        }

                    }
                    if (entryKey.equalsIgnoreCase("value")) {
                        sizeValue = me.getValue().toString();
                    }

                }
            }

            if (productCodeAndSize.isEmpty()) {

            } else {
                productCodeAndSize.add(productCode);
                Collections.sort(productCodeAndSize);
                break;
            }

        }
        return productCodeAndSize;

    }

    public String getProductURL() {
        baseURI();
        setHeader();
        endPoint();
        getResponseCode();
        String productCode = null;
        for (int i = 0; i <= jsonTestData.getResults().size() - 1; ++i) {
            productCode = ((LinkedTreeMap) jsonTestData.getResults().get(i)).get("url").toString();

        }

        return productCode;
    }


    public Boolean verifyProductPrice() {
        Boolean flag = null;
        for (int i = 0; i <= jsonTestData.getResults().size() - 1; ++i) {
            productPrice = Arrays.asList(((LinkedTreeMap) jsonTestData.getResults().get(i)).get("price"));
            try {
                String formattedPrice = ((LinkedTreeMap) ((List) productPrice).get(0)).get("formattedValue").toString().toLowerCase();
                if (formattedPrice.isEmpty()) {
                    flag = false;
                    break;
                } else {
                    flag = true;
                    break;
                }
            } catch(Error e){

                }
        }
        return flag;
    }

//    public String getDesignerName() {
//
//        for (int i = 0; i <= jsonTestData.getResults().size() - 1; ++i) {
//            String productCode = ((LinkedTreeMap) jsonTestData.getResults().get(i)).get("code").toString();
//            productPrice = Arrays.asList(((LinkedTreeMap) jsonTestData.getResults().get(i)).get("designer"));
//            designerName = ((LinkedTreeMap) ((List) productPrice).get(0)).get("name").toString().toLowerCase();
//
//        }
//        return designerName;
//    }

    public Map<String,String> retrieveAvailableProductDetails() {
        boolean status = true;
            int index = 0;
            availableProductDetails = new HashMap<>();
            String productPriceOnPDP = obj_Property.readProperty("productPriceOnPDPByCSS");
            String productDesignerName = obj_Property.readProperty("productDesignerNameByCSS");
            String productDescription = obj_Property.readProperty("productDescriptionByCSS");
            while(status){
                Select sizeDropdownlist = new Select(commonFunctions.getWebElement(pdp_SizeDropDown_CSS));
                List<WebElement> sizeWebElementsList = sizeDropdownlist.getOptions();
                for (int i=1 ; i < sizeWebElementsList.size() ; i++) {
                    WebElement sizeWebElement = sizeWebElementsList.get(i);
                    if(!sizeWebElement.getText().contains("-")){
                        availableProductDetails.put("productId",getInStockProductIdWithAvailableVarients().get(index));
                        availableProductDetails.put("size",sizeWebElement.getText().trim());
                        availableProductDetails.put("price",commonFunctions.getTextByCssSelector(productPriceOnPDP));
                        availableProductDetails.put("designerName",commonFunctions.getTextByCssSelector(productDesignerName));
                        availableProductDetails.put("description",commonFunctions.getTextByCssSelector(productDescription));
                        status = false;
                        i=sizeWebElementsList.size();
                    }
                }
                if(status){
                    index++;
                    if (navigateUsingURL.getrelativeURLJenkins() == null) {
                        commonFunctions.navigateTo(Constants.BASE_URL + "/p/" +getInStockProductIdWithAvailableVarients().get(index));
                    } else {
                        String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
                        commonFunctions.navigateTo(relativeURLJenkins + "/p/" +getInStockProductIdWithAvailableVarients().get(index));
                    }
                }
            }
        return availableProductDetails;
    }

    public List<String> retrieveAvailableSizes(){
        Select sizeDropdownlist = new Select(commonFunctions.getWebElement(pdp_SizeDropDown_CSS));
        List<WebElement> sizeWebElementsList = sizeDropdownlist.getOptions();
        List<String> availableSizes = new ArrayList<>();
        for(int i=1 ; i < sizeWebElementsList.size() ; i++){
            WebElement sizeWebElement = sizeWebElementsList.get(i);
            if(!sizeWebElement.getText().contains("-")){
                availableSizes.add(commonFunctions.retrieveTextByWebElement(sizeWebElement).replaceAll("\n|\t","").trim());
            }
        }
        return availableSizes;
    }

    public String retrieveSoldOutSize(){
        Select sizeDropdownlist = new Select(commonFunctions.getWebElement(pdp_SizeDropDown_CSS));
        List<WebElement> sizeWebElementsList = sizeDropdownlist.getOptions();
        String soldOutSize = "";
        int i=1;
        boolean status = true;
        while(status){
            soldOutSize = sizeWebElementsList.get(i).getText();
            if(soldOutSize.contains("Sold out") || soldOutSize.contains("Article épuisé") ||
                    soldOutSize.contains("품절") || soldOutSize.contains("売り切れ")){
                soldOutSize =  soldOutSize.replaceAll("\n|\t|\\s","").toLowerCase();
                status = false;
            }
            i++;
        }
        return soldOutSize;
    }
}


