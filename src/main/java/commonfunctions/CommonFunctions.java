package commonfunctions;

        import java.io.IOException;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.util.Iterator;
        import java.util.List;
        import java.util.Random;
        import java.util.Set;

        import baseclass.WebDriverFactory;
        import org.openqa.selenium.*;

        import org.openqa.selenium.remote.RemoteWebDriver;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.Select;
        import org.openqa.selenium.support.ui.WebDriverWait;
        import org.testng.asserts.SoftAssert;
        import baseclass.BaseClass;
        import config.Constants;

public class CommonFunctions extends BaseClass {
    public String add = null;
    SoftAssert assertions = new SoftAssert();
    Capabilities caps = ((RemoteWebDriver) getWebDriver()).getCapabilities();
//    private NavigateUsingURL navigateUsingURL = new NavigateUsingURL();

    public void refreshPage() {
        waitForPageToLoad();
        getWebDriver().navigate().refresh();
    }

    public void deleteAllCookies() {
//        waitForPageToLoad();
//        refreshPage();
        waitForPageToLoad();
        getWebDriver().manage().deleteAllCookies();
        if (getWebDriver().manage().getCookies().size() > 0) {
            Set<Cookie> allCookies = getWebDriver().manage().getCookies();
            for (Cookie cookie : allCookies) {
                waitForPageToLoad();
                getWebDriver().manage().deleteCookieNamed(cookie.getName());

                if (cookie.getName().contains("language")) {
                    getWebDriver().manage().deleteCookieNamed("language");
                }
                String javascriptCall = "document.cookie = \"" + cookie.getName() + "=; path=" + cookie.getPath() + "; expires=Thu, 01-Jan-1970 00:00:01 GMT;\"";
                ((JavascriptExecutor) getWebDriver()).executeScript(javascriptCall);
                //Will remove below code once above working fine on all browsers
//				if(cookie.getName().contains("language")||cookie.getName().contains("loggedIn")||cookie.getName().contains("userID"))
//				{
//					getWebDriver().manage().deleteCookieNamed("language");
//				}
            }
        }

    }

    public void navigateTo(String url) {
        getWebDriver().get(url);
    }

    public synchronized String getTextGoToWishList(String cssLocator) {
        getWebDriver().switchTo().defaultContent();
        //  getWebDriver().navigate().refresh();
        waitForScriptsToLoad();
        waitForPageToLoad();
        waitForPageToLoad();
        return retrieveText(cssLocator).trim();
    }

    public synchronized String getTextByCssSelector(String cssLocator) {
        waitForPageToLoad();
        waitForPageToLoad();
        return retrieveText(cssLocator).trim();
    }

    public void skipPopups() {
        getWebDriver().navigate().refresh();
    }

    public void clickElementByCSS(String cssLocator) {
        waitForPageToLoad();
        waitForPageToLoad();
        try{
        if(getWebElement(cssLocator).isDisplayed()) {
            getWebElement(cssLocator).click();
            waitForPageToLoad();
            waitForPageToLoad();
        }

        }catch (Exception e){
            e.getMessage();
        }
    }

    public void clickOnSpecificElementByCSS(String cssLocator, String text) {
        waitForPageToLoad();
        waitForPageToLoad();
        cssLocator = cssLocator.replace("elementtext", text);
        clickElementByCSS(cssLocator);
        waitForPageToLoad();
        waitForPageToLoad();
    }

    public void selectFromDropdownByCSS(String cssLocator, String textValue) {
        waitForPageToLoad();
        waitForPageToLoad();
        try {
            WebElement dropdown = getWebElement(cssLocator + " ~ span");
            clickWebElementByJSE(dropdown);

            List<WebElement> optionsList = waitForListElementByCssLocator(cssLocator + " ~ div .cs__item");
            for (WebElement optionNow : optionsList) {
                if (textValue.contains("-")) {
                    if (retrieveTextByWebElement(optionNow).trim().toLowerCase().replaceAll("\\n", "").replaceAll(" ", "").contains(textValue.trim().toLowerCase())) {
                        clickWebElementByJSE(optionNow);
                        waitForPageToLoad();
                        break;
                    }
                } else {
                    if (retrieveTextByWebElement(optionNow).trim().toLowerCase().replaceAll(" ","").equalsIgnoreCase(textValue.trim().toLowerCase().replaceAll(" ",""))) {
                        clickWebElementByJSE(optionNow);
                        waitForPageToLoad();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }


    public WebElement waitForElementByCssLocator(String cssLocator) {
        return getWebDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssLocator)));

    }

    public List<WebElement> waitForListElementByCssLocator(String cssLocator) {
        // return getWebDriverWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(cssLocator)));
        return getWebDriver().findElements(By.cssSelector(cssLocator));
    }

    protected WebDriverWait getWebDriverWait() {
        return new WebDriverWait(getWebDriver(), Constants.WAIT_TIME_LARGE_SEC);
    }


    public String retrieveText(String csslocator) {
        waitForPageToLoad();
        return getWebElement(csslocator).getText();
    }

    public String retrieveTextByWebElement(WebElement webElement) {
        return webElement.getText();
    }

    public String retrieveLowercaseTextWithNoEscapeChars(WebElement we){
        String lowercaseTextWithNoEscapeChars = we.getText().toLowerCase().trim().replaceAll(" ","").replaceAll("\n|\t|\\s","");
        return lowercaseTextWithNoEscapeChars;
    }

    public String retrieveTextWithNoEscapeChars(String cssSelector){
        return retrieveText(cssSelector).replaceAll(" ","").replaceAll("\n|\t|\\s","");
    }

    public void waitForPageToLoad() {
        int retry = 0;
        String response = "";
        do {
            try {
                if (caps.getBrowserName().trim().equalsIgnoreCase(Constants.SAFARI)) {
                    Thread.sleep(Constants.WAIT_TIME_CONSTANT_SAFARI);
                } else {
                    Thread.sleep(Constants.WAIT_TIME_CONSTANT);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            response = String.valueOf(((JavascriptExecutor) getWebDriver()).executeScript("return document.readyState"));
            retry++;
        } while (retry <= Constants.PAGE_LOAD_MAX_RETRY && response.equals("complete") != true);
    }

    public void waitForScriptsToLoad() {
        int retry = 0;

        String response = "";
        do {
            try {
                Thread.sleep(Constants.WAIT_TIME_CONSTANT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            response = String.valueOf(((JavascriptExecutor) getWebDriver())
                    .executeScript("return window.jQuery != undefined && jQuery.active == 0"));
            retry++;
        } while (retry <= Constants.PAGE_LOAD_MAX_RETRY && response.equals("complete") != true);
    }

    public void scrollToPageTop() {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.scrollTo(document.body.scrollHeight,0);");
    }

    public void scrollToPageBottom() {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.scrollTo(0,document.body.scrollHeight);");

    }

    public void scrollIntoViewByCss(String css) {
        ((JavascriptExecutor) getWebDriver()).executeScript("$('" + css + "').get(0).scrollIntoView();");
    }

    public void scrollToElement(String elementLocator) {
        ((JavascriptExecutor) getWebDriver()).executeScript("var a = $('" + elementLocator
                + "')[0].offsetTop; $('.mCustomScrollbar').mCustomScrollbar('scrollTo',a);");
        waitForPageToLoad();
    }

    public void waitForElementTextToChangeTo(String cssLocator, String text) {
        int retry = 0;
        String response = "";
        do {
            try {
                response = retrieveText(cssLocator);
            } catch (Exception e) {
                response = "Button not found.";
            }
            retry++;
        } while (retry <= Constants.PAGE_LOAD_MAX_RETRY && response.equals(text) != true);
    }

    public void scrollScreen300Pixel() {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.scrollBy(0,300)");
    }
    public void scrollScreen50Pixel() {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.scrollBy(0,50)");
    }

    public void scrollScreen500Pixel() {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.scrollBy(0,500)");
    }

    public void clickOnElementInList(String listCss, String elementName) {
        waitForPageToLoad();
        List<WebElement> list = waitForListElementByCssLocator(listCss);
        boolean found = false;
        for (WebElement e : list) {
            if (retrieveTextByWebElement(e).trim().contentEquals(elementName)) {
                clickWebElement(e);
                found = true;
                break;
            }
        }
        assertions.assertTrue(found, elementName + " element was not found");
    }

    public void getTextFromElementInList(String listCss, String elementName) {
        waitForPageToLoad();
        List<WebElement> AddedToWishlist = waitForListElementByCssLocator(listCss);
        Iterator<WebElement> itr = AddedToWishlist.iterator();
        while (itr.hasNext()) {
            add = retrieveTextByWebElement(itr.next());
        }
        assertions.assertTrue(true, elementName + " element was not found");
    }


    public void enterTextInputBox(String cssLocator, String text) {
        getWebElement(cssLocator).clear();
        getWebElement(cssLocator).sendKeys(text);
        waitForPageToLoad();
    }

    public void enterText(String elementType, String element, String textValue) {
        waitForPageToLoad();
        if (elementType.equals("id"))
            getWebDriver().findElement(By.id(element)).sendKeys(textValue);
        if (elementType.equals("cssSelector"))
            getWebDriver().findElement(By.cssSelector(element)).sendKeys(textValue);
    }

    public String getSrcAttribute(String cssLocator) {
        waitForPageToLoad();
        return getWebElement(cssLocator).getAttribute("src");
    }

    public boolean isElementDisplayed(String cssLocator) {
        return getWebElement(cssLocator).isDisplayed();
    }

    public String isDropDownEnabled(String cssLocator) {
        return getWebElement(cssLocator).getAttribute("class");
    }

    public Boolean isDropDownEnabledByCSS(String cssLocator) {
        return getWebElement(cssLocator).isEnabled();
    }

    public boolean isClickable(WebElement element) {
        boolean result = false;
        try {
            result = getWebDriverWait()
                    .until(ExpectedConditions.elementToBeClickable(element)) != null;
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public WebElement getWebElement(String cssLocator) {
        return getWebDriver().findElement(By.cssSelector(cssLocator));
    }

    public boolean getDropDownMenuIsOpened(String cssLocator) {
        waitForPageToLoad();
        return !getWebElement(cssLocator).isDisplayed();
    }

    public void focusElement(String cssSelector) {
        ((JavascriptExecutor) getWebDriver()).executeScript("$('" + cssSelector + "')[0].scrollIntoView(true);");
        waitForPageToLoad();
    }

    public void fireEvent(String elementId, String action) {
        ((JavascriptExecutor) getWebDriver()).executeScript(
                "function FireEvent(a,b){if(null!=document.getElementById(a))if(document.getElementById(a).fireEvent)document.getElementById(a).fireEvent(\"on\"+b);else{var c=document.createEvent(\"Events\");c.initEvent(b,!0,!1),document.getElementById(a).dispatchEvent(c)}} FireEvent( \""
                        + elementId + "\", \"" + action + "\" );");
    }

    public void selectRadioButton(String cssSelector) {
        WebElement el = getWebElement(cssSelector);
        clickWebElementByJSE(el);
    }

    public List<WebElement> getSelectDropdownElements(String cssLocator) {
        Select selectDropdown = new Select(getWebElement(cssLocator));
        List<WebElement> dropdownElements = selectDropdown.getOptions();
        return dropdownElements;
    }

    public void mouseOverElement(String cssSelector) {
        ((JavascriptExecutor) getWebDriver()).executeScript("$('" + cssSelector + "').mouseover();");
        waitForPageToLoad();
    }

    public void selectCustomDropdown(String cssLocator, String textValue) {
        waitForPageToLoad();
        waitForElementByCssLocator(cssLocator);
        clickElementByCSS(cssLocator + " span.cs__selected");
        List<WebElement> optionList = getWebElement(cssLocator).findElements(By.cssSelector("span.cs__item:not(.disabled)"));
        for (WebElement element : optionList) {
            if (retrieveTextByWebElement(element).trim().contains(textValue)) {
                clickWebElement(element);
                break;
            }
        }
    }


    public int ElementInListIsDisplayed(String csslocator) {
        waitForPageToLoad();
        int shopTheLookProductsCount = getWebDriver().findElements(By.cssSelector(csslocator)).size();
        return shopTheLookProductsCount;
    }

    public String getStyleAttributeOfSpecificElement(String csslocator, String text) {
        waitForPageToLoad();
        csslocator = csslocator.replace("elementtext", text);
        return waitForElementByCssLocator((csslocator)).getAttribute("style");

    }

    public String getCurrentURL() {
        return getWebDriver().getCurrentUrl();

    }

    public String getUppercaseText(String cssLocator) {
        waitForPageToLoad();
        return retrieveText(cssLocator).toUpperCase();
    }

    public boolean isSuccessStatus(String text) {
        boolean ret = getWebDriver().getPageSource().contains(text);
        return ret;
    }

    public void waitForElementToBeClickable(String cssLocator) {
        WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.WAIT_TIME_LARGE_SEC);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssLocator)));
    }

    public WebElement waitForElementToBeClickableByElement(WebElement el) {
        WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.WAIT_TIME_LARGE_SEC);
        return wait.until(ExpectedConditions.elementToBeClickable(el));
    }

    public void clickElementByWebElement(WebElement el) {
        if (WebDriverFactory.browser.equalsIgnoreCase("Safari")) {
            clickWebElement(el);
        } else {
            clickWebElementByJSE(el);
        }
    }

    public void clickWebElementByJSE(WebElement webElement) {
        JavascriptExecutor executor = (JavascriptExecutor) getWebDriver();
        executor.executeScript("arguments[0].click()", webElement);
    }

    public void clickWebElement(WebElement webElement) {
        waitForPageToLoad();
        webElement.click();
    }

    public void selectRandomValueFromDropdownElements(String cssLocator) {
        Random random = new Random();
        waitForPageToLoad();
        waitForPageToLoad();
        try {
            WebElement dropdown = waitForElementByCssLocator(cssLocator + " ~ span");
            clickWebElementByJSE(dropdown);
            waitForPageToLoad();

            List<WebElement> optionsList = waitForListElementByCssLocator(cssLocator + " ~ div .cs__item");
            int optionIndex = random.nextInt(optionsList.size() - 4);
            WebElement optionNow = optionsList.get(optionIndex);
            waitForPageToLoad();
            clickWebElementByJSE(optionNow);
            waitForPageToLoad();

        } catch (Exception e) {
            e.getMessage();
        }
    }


    protected boolean isElementPresentByCssLocator(String cssLocator) {
        return waitForListElementByCssLocator(cssLocator).size() != 0;
    }

    public String getAttribute(String csslocator, String attributename) {
        return getWebDriver().findElement(By.cssSelector(csslocator)).getAttribute(attributename);
    }

    public void clickOnElement(WebElement webElement) {
        waitForPageToLoad();
        waitForPageToLoad();
        if (WebDriverFactory.browser.equalsIgnoreCase("safari"))
            clickWebElementByJSE(webElement);
        else
            clickWebElement(webElement);
    }

    public void loggedInUserAccountOptions(String loggedInUserOptionsDropdownLocator, String optionsLocator) {
        //Created script and script1 variable as Javascript was getting failed to create query with $.
        final String script = "$(\"" + loggedInUserOptionsDropdownLocator + "\").click()";
        ((JavascriptExecutor) getWebDriver()).executeScript(script);

        final String script1 = "$(\"" + optionsLocator + "\").click()";
        ((JavascriptExecutor) getWebDriver()).executeScript(script1);
    }


    public String isLinkBroken(String linkURL) throws IOException {
        URL url = new URL(linkURL);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.connect();
        int responsecode = urlConnection.getResponseCode();
        if (responsecode == 200) {
            return Integer.toString(urlConnection.getResponseCode());
        } else {
            return Integer.toString(urlConnection.getResponseCode());
        }
    }

    public void navigateToWishlistPage(){
        navigateTo(Constants.BASE_URL +"/account/wishlist");
        /*if (navigateUsingURL.getrelativeURLJenkins() == null) {
            navigateTo(Constants.BASE_URL +"/account/wishlist");
        } else {
            String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
            navigateTo(relativeURLJenkins +"/account/wishlist");
        }*/
    }

    public void selectFromDropdownByText(String cssLocator, String textValue) {
        WebElement dropdown = getWebDriver().findElement(By.cssSelector(cssLocator + " ~ span"));
        clickWebElementByJSE(dropdown);

        List<WebElement> optionsList = getWebDriver().findElements(By.cssSelector(cssLocator + " ~ div .cs__item"));
        for (WebElement optionNow : optionsList) {
            if (retrieveTextByWebElement(optionNow).trim().toLowerCase().contains(textValue.trim().toLowerCase())) {
                clickWebElementByJSE(optionNow);
                break;
            }
        }
    }

    public void goTOIframe(String iFrameName) {
        waitForPageToLoad();
        getWebDriver().switchTo().frame(iFrameName);
        waitForPageToLoad();
    }

    public void deleteJSessionID(){
        Set<Cookie> cookies = getWebDriver().manage().getCookies();
        for (Cookie cookie: cookies){
            if (cookie.getName().contains("JSESSIONID")){
                String javascriptCall = "document.cookie = \"" + cookie.getName() + "=; path=" + cookie.getPath() + "; expires=Thu, 01-Jan-1970 00:00:01 GMT;\"";
                ((JavascriptExecutor) getWebDriver()).executeScript(javascriptCall);
                getWebDriver().manage().deleteCookie(cookie);
//                break;
            }

        }
    }

    public void switchBackToParentFrame(){
        getWebDriver().switchTo().parentFrame();
    }

    public void selectValueFromDropDownviaSelect(String cssLocator,String optionValue) {
        Select dropDown = new Select(getWebElement(cssLocator));
        dropDown.selectByValue(optionValue);
    }

    public void getValueFromDropDownviaSelect(String cssLocator, String optionValue) {
        Select dropDown = new Select(getWebElement(cssLocator));
        dropDown.selectByVisibleText(optionValue);
        dropDown.getOptions();
    }

    public void clickUsingLinkText(String linkText){
        clickOnElement(getWebDriver().findElement(By.linkText(linkText)));
    }

    public void fireEventByCSS(String css, String action) {
        ((JavascriptExecutor) getWebDriver()).executeScript(
                "function FireEvent(css,action){if(null!=document.querySelector(css))if(document.querySelector(css).fireEvent)" +
                        "document.querySelector(css).fireEvent(\"on\"+action);else{var c=document.createEvent(\"Events\");" +
                        "c.initEvent(action,!0,!1),document.querySelector(css).dispatchEvent(c)}} FireEvent( \""
                        + css + "\", \"" + action + "\" );");
    }

    public void clickOnBTTLink(String css){
        focusElement(css);
        clickElementByCSS(css);
    }
}