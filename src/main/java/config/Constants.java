package config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Constants {

	public static final long WAIT_TIME_LARGE_SEC = 10;
	public static final long WAIT_TIME_CONSTANT_SAFARI = 800;
	public static final long WAIT_TIME_CONSTANT = 500;
	public static final int PAGE_LOAD_MAX_RETRY = 30;
    public static final String BASE_URL = "https://regression.matchesfashion.com";
	public static final String SOLRAPI_URL = "https://regression-solr-master.matcheslocal.com/solr/master_matchesfashion_Product/select?q=*:*&rows=1000&fl=code_text,stockLevelStatus_string,%20url_en_string%20size_en_string_mv%20availableSize_string_mv&wt=json";
	public static final String API_ENDPOINT_JSON= "/mens/shop/clothing?format=json";
	public static final String ONE_ENDPOINT_SIZE_JSON = "mens/shop/bags?format=json";

    public static final String SAFARI ="safari";
    public static final String CHROME ="chrome";
    public static final String EDGE ="edge";
    public static final String FIREFOX ="firefox";
    public static final String IE ="ie";
    public static final String GIFTITEM1 = "1097761000001";


//"https://dev9-solr-master.matcheslocal.com/solr/master_matchesfashion_Product/select?q=*%3A*&rows=200&fl=code_text,stockLevelStatus_string%2C+availableSize_string_mv&wt=json";

    public static final Map<String, String> COUNTRY_ISOCODE_MAP = new HashMap<String, String>() {{
        put("United Kingdom", "");
        put("United States", "/us");
        put("Australia", "/au");
        put("France", "/fr");
        put("Korea Republic of", "/kr");
        put("Japan", "/jp");
    }};


    public static final Map<String, String> COUNTRY = new HashMap<String, String>() {{
        put("Japan", " Japan");
        put("France", "France");
        put("Korea Republic of", "Korea Republic of");
        put("United Kingdom", "United Kingdom ");

    }};

    public static final Map<String, String> GIFTCARD = new HashMap<String, String>() {{
        put("£100", "1097761000001");
        put("£200", "1097762000001");
        put("£300", "1097763000001");
        put("£400", "1097764000001");
        put("£500", "1097766000001");
        put("£750", "1097766000001");

    }};

    /////Product IDs

    // instock, soldout, coming soon,sizes, recentally viewed

//sujana.idr@gmail.com
    // Username and password for specific browser


    public static final String CHROME_USERNAME = "mfauto45@gmail.com";
    public static final String CHROME_PASSWORD = "Matches123";
    //sujana.rtbf@gmail.com
    public static final String SAFARI_USERNAME = "mfauto45@gmail.com";
    public static final String SAFARI_PASSWORD = "Matches123";

    public static final String FIREFOX_USERNAME = "mfauto45@gmail.com";
    public static final String FIREFOX_PASSWORD = "Matches123";

	public static final String EDGE_USERNAME= "";
	public static final String EDGE_PASSWORD= "";

	public static final String IE_USERNAME= "";
	public static final String IE_PASSWORD= "";

	public static final String WISHLIST_EMAIL="mfautotest+3001@gmail.com";
	public static final String WISHLIST_NAME="Dilbag";
	public static final String WISHLIST_TEXTMESSAGE="Please buy this for me";

    //1266984
// INSTOCK PRODUCT IDS FOR SMALL, MEDIUM AND LARGE SIZES for 1009197
    public static final String INSTOCK = "1009197";
    public static final String INSTOCK_SECONDPRODUCT = "1004572";
    public static final String SOLDOUT = "1168680";
    public static final String INSTOCK_ONESIZE = "1206511";
    public static final String PRODUCT_ONEQTYONLY = "1220881";
    public static final String PRODUCT_MATCHITWITH = "1187611";
    public static String NO_LANGUAGE_FOUND = "No language found in header.";

    public static final String API_SUCCESS_MESSAGE = "Success";
    public static final String API_FAILED_MESSAGE = "failed";

    public static final String ORDER_PREFIX = "OMF";

    public static final ArrayList<String> MATCHESFASHIONLINKSFOOTER = new ArrayList<String>(){
        {
                add("/bio"); //About-us
                add("/bio/eco-age"); //Sustainability
                add("/careers");
                add("/affiliates");
        }
    };

    public static final ArrayList<String> HELPLINKSFOOTER = new ArrayList<String>(){
        {
            add("/contact-us");
            add("/faqs");
            add("/delivery");
            add("/returns");
        }
    };

    public static final ArrayList<String> SERVICELINKSFOOTER = new ArrayList<String>(){
        {
            add("/mystylist");
            add("/private-shopping");
            add("/loyalty");
        }
    };

    public static final ArrayList<String> LEGALLINKSFOOTER = new ArrayList<String>(){
        {
            add("/tandcs");
            add("/privacy-policy");
            add("/privacy-policy#cookie-policy");
            add("/bio/eco-age/our-responsibility");
        }
    };

    public static final String GIFTCARDLINKSFOOTER = "/gift-cards";

    public static final ArrayList<String> STORELINKSFOOTER = new ArrayList<String>(){
        {
            add("/stores/marylebone");
            add("/stores/notting-hill");
            add("/stores/wimbledon");
            add("/5carlosplace");
        }
    };

    // if this value is changed, please validate accordingly in designersdef class
    public static final String DESIGNERS_LINK = "/mens/designers/gucci";

    public static final String DESIGNER_NAME = "GUCCI";


    // gmail details

    public static final String HOST = "pop.gmail.com";
    public static final String MAILSTORETYPE = "pop3";
    public static final String USERNAME = "matches.whitelist@gmail.com";
    public static final String PASSWORD = "Matches123";

//public static void main(String[]args) throws IllegalAccessException, MalformedURLException {
//	  String username = "dilbagsingh"; // Your username
//	  String authkey = "120c2887-c52f-4b28-8ff6-fef0cc8d3d33";  // Your authkey
//	DesiredCapabilities caps1 = new DesiredCapabilities();
//	//capabilities.setCapability("username", "dilbag.singh@matchesfashion.com");
//	//capabilities.setCapability("accessKey", "120c2887-c52f-4b28-8ff6-fef0cc8d3d33");
//	caps1.setCapability("browserName", "Safari");
//	caps1.setCapability("platform", "macOS 10.12");
//	caps1.setCapability("version", "11.0");
//	//capabilities.setCapability("extendedDebugging: true");
//	WebDriver driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@ondemand.eu-central-1.saucelabs.com/wd/hub"), caps1);
//
//	driver.get("https://www.google.com");
//}

}
