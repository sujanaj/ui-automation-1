package config;

import java.util.HashMap;
import java.util.Map;

import commonfunctions.CommonFunctions;

public class CurrencySettings {
	CommonFunctions commonFunctions = new CommonFunctions();
	public String setCurrencyAlias(String currency) {
		Map<String, String> currencyAliases = new HashMap<String, String>();
		currencyAliases.put("USD", "US Dollar");
		currencyAliases.put("GBP", "Pound");
		currencyAliases.put("EUR", "Euro");
		currencyAliases.put("AUD", "Australian");
		currencyAliases.put("HKD", "Hong");
		currencyAliases.put("JPY", "Japanese Yen");
		currencyAliases.put("Korea", "Euro");
		String result = currency;
		for (Map.Entry<String, String> pair : currencyAliases.entrySet()) {
			if (pair.getKey().toLowerCase().contentEquals(currency.toLowerCase().trim())) {
				result = pair.getValue();
				break;
			}
		}

		return result;
	}


	public void selectCurrencyFromDropDownCSS(String cssLocator,String currency ) {
		currency =setCurrencyAlias(currency);
		commonFunctions.selectFromDropdownByCSS(cssLocator, currency);

	}

}
