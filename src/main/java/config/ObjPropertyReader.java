package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ObjPropertyReader {

	Properties properties = new Properties();
	InputStream inputStream = null;

	public ObjPropertyReader() {
		loadProperties();
	}

	private void loadProperties() {
		try {
			inputStream = new FileInputStream("src/main/resources/ObjectRepository.properties");
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String readProperty(String key) {

		return properties.getProperty(key);
	}



}
